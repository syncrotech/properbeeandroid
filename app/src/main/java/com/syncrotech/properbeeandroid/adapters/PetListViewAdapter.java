package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.Pet;

import java.util.List;

public class PetListViewAdapter extends ArrayAdapter<Pet> {

    private List<Pet> petList;
    private Context mContext;

    public PetListViewAdapter(Context mContext, int textViewResourceId, List<Pet> petList) {
        super(mContext, textViewResourceId, petList);
        this.petList = petList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = viewInflater.inflate(R.layout.pet_list_view_item, null);

        Pet pet = petList.get(position);

        if (pet != null) {
            View display = view.findViewById(R.id.display_occupant);
            View create = view.findViewById(R.id.create_view);

            if (pet.getAge() > -1) {
                display.setVisibility(View.VISIBLE);
                create.setVisibility(View.GONE);
                TextView type = (TextView) view.findViewById(R.id.pet_list_view_item_type);
                TextView age = (TextView) view.findViewById(R.id.pet_list_view_item_age);
                TextView breed = (TextView) view.findViewById(R.id.pet_list_view_item_breed);

                type.setText(pet.getType());
                age.setText(String.format(getContext().getString(R.string.display_int), pet.getAge()));
                breed.setText(pet.getBreed());
            } else {
                display.setVisibility(View.GONE);
                create.setVisibility(View.VISIBLE);
                TextView textView = (TextView) view.findViewById(R.id.create_new_item_text);
                textView.setText("Add New Pet");
            }
        }
        return view;
    }

    public List<Pet> getData() {
        return this.petList;
    }
}
