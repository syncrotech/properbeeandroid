package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.Occupant;

import java.util.List;

/**
 * Created by ScryJ_000 on 7/10/2016.
 */
public class OccupantListViewAdapter extends ArrayAdapter<Occupant> {

    private List<Occupant> occupantList;
    private Context mContext;

    public OccupantListViewAdapter(Context mContext, int textViewResourceId, List<Occupant> occupantList) {
        super(mContext, textViewResourceId, occupantList);
        this.occupantList = occupantList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = viewInflater.inflate(R.layout.occupant_list_view_item, null);

        Occupant occupant = occupantList.get(position);

        if (occupant != null) {
            View display = view.findViewById(R.id.display_occupant);
            View create = view.findViewById(R.id.create_view);

            if (occupant.getAge() > -1) {
                display.setVisibility(View.VISIBLE);
                create.setVisibility(View.GONE);
                TextView name = (TextView) view.findViewById(R.id.pet_list_view_item_type);
                TextView age = (TextView) view.findViewById(R.id.occupant_list_view_item_age);
                TextView relationship = (TextView) view.findViewById(R.id.occupant_list_view_item_relationship);
                name.setText(occupant.getName());
                age.setText(String.format(getContext().getString(R.string.display_int), occupant.getAge()));
                relationship.setText(occupant.getRelationship());
            } else {
                display.setVisibility(View.GONE);
                create.setVisibility(View.VISIBLE);
                TextView textView = (TextView) view.findViewById(R.id.create_new_item_text);
                textView.setText("Add New Occupant");
            }
        }
        return view;
    }

    public List<Occupant> getData() {
        return this.occupantList;
    }
}
