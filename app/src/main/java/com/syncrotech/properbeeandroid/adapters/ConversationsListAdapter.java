package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.dbModels.Conversation;

import java.util.List;

public class ConversationsListAdapter extends ArrayAdapter<Conversation> {

    private List<Conversation> conversationList;
    private Context context;

    public ConversationsListAdapter(Context context, int textViewResourceId, List<Conversation> conversations) {
        super(context, textViewResourceId, conversations);
        this.conversationList = conversations;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.message_list_item, null);

            Conversation conversation = conversationList.get(position);

            if (conversation != null) {
                TextView count = (TextView) view.findViewById(R.id.list_item_counter);
                TextView conversationName = (TextView) view.findViewById(R.id.list_item_text);

                count.setText(String.valueOf(conversation.unreadMessages));
                conversationName.setText(conversation.name);
                if (conversation.unreadMessages <= 0) {
                    count.setVisibility(View.INVISIBLE);
                }

            }
        }
        return view;
    }

    public List<Conversation> getData() {
        return this.conversationList;
    }
}
