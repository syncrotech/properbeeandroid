package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.dbModels.Notification;

import java.util.List;

public class NotificationListViewAdapter extends ArrayAdapter<Notification> {

    private List<Notification> notificationList;
    private Context mContext;

    public NotificationListViewAdapter(Context mContext, int textViewResourceId, List<Notification> notificationList) {
        super(mContext, textViewResourceId, notificationList);
        this.notificationList = notificationList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = viewInflater.inflate(R.layout.list_item_image_text, null);

        Notification notification = notificationList.get(position);

        if (notification != null) {
            TextView message = (TextView) view.findViewById(R.id.list_item_text);
            ImageView imageView = (ImageView) view.findViewById(R.id.list_item_icon);
            View itemView = view.findViewById(R.id.item_view);

            message.setText(notification.message);
            int iconId = 0;
            switch (notification.actionArea) {
                case 0:
                    iconId = notification.firstSeen ? R.drawable.ic_inspections_white_24px : R.drawable.ic_inspections_yellow_24px;
                    break;
                case 1:
                    iconId = notification.firstSeen ? R.drawable.ic_account_white_24dp : R.drawable.ic_account_yellow_24dp;
                    break;
                case 2:
                    iconId = notification.firstSeen ? R.drawable.ic_reports_white_24px : R.drawable.ic_reports_yellow_24px;
                    break;
                case 3:
                    iconId = notification.firstSeen ? R.drawable.ic_message_white_24px : R.drawable.ic_message_yellow_24px;
                    break;
                default:
                    iconId = notification.firstSeen ? R.drawable.ic_notification_white_24dp : R.drawable.ic_notification_yellow_24dp;
                    break;
            }
            imageView.setBackground(ContextCompat.getDrawable(mContext, iconId));

            if (notification.firstSeen) {
                view.setBackground(ContextCompat.getDrawable(mContext, R.color.colorPrimary));
            }

        }
        return view;
    }

    public List<Notification> getData() {
        return this.notificationList;
    }
}
