package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.Message;

import java.util.List;

public class MessagesAdapter extends ArrayAdapter<Message> {

    private List<Message> messageList;
    private Context context;

    public MessagesAdapter(Context context, int textViewResourceId, List<Message> messages) {
        super(context, textViewResourceId, messages);
        this.messageList = messages;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater viewInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Message message = getItem(position);


        if (message != null) {
            view = message.userMessage ? viewInflater.inflate(R.layout.list_item_user_message, null) : viewInflater.inflate(R.layout.list_item_other_message, null);
            message.userName = message.userMessage ? "You" : message.userName;
            view.findViewById(R.id.seenDot).setVisibility(message.seen ? View.INVISIBLE : View.VISIBLE);
            ((TextView) view.findViewById(R.id.userName)).setText(message.userName);
            ((TextView) view.findViewById(R.id.message)).setText(message.message);
        }
        return view;
    }

    public List<Message> getData() {
        return this.messageList;
    }
}