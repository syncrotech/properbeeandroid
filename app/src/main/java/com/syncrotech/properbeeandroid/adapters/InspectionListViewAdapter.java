package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionPartialViewModel;

import java.util.List;

public class InspectionListViewAdapter extends ArrayAdapter<InspectionPartialViewModel> {

    private List<InspectionPartialViewModel> inspectionList;
    private Context mContext;

    public InspectionListViewAdapter(Context mContext, int textViewResourceId, List<InspectionPartialViewModel> inspectionList) {
        super(mContext, textViewResourceId, inspectionList);
        this.inspectionList = inspectionList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.list_item_inspection, null);

            InspectionPartialViewModel inspection = inspectionList.get(position);

            if(inspection != null){
                TextView date = (TextView) view.findViewById(R.id.date);
                ImageView state = (ImageView) view.findViewById(R.id.state);

                date.setText("Inspection date: " + inspection.date);

                Drawable img;
                switch (inspection.state){
                    case 0:
                        img = ContextCompat.getDrawable(getContext(), R.drawable.circle);
                        break;
                    case 1:
                        img = ContextCompat.getDrawable(getContext(), R.drawable.tick);
                        break;
                    case 2:
                        img = ContextCompat.getDrawable(getContext(), R.drawable.cross);
                        break;
                    default:
                        img = ContextCompat.getDrawable(getContext(), R.drawable.circle);
                        break;
                }
                state.setBackground(img);
            }
        }
        return view;
    }

    public List<InspectionPartialViewModel> getData(){
        return this.inspectionList;
    }
}
