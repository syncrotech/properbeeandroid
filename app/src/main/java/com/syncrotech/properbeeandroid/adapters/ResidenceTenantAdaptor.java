package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.ResidenceTenantModel;

import java.util.List;

public class ResidenceTenantAdaptor extends ArrayAdapter<ResidenceTenantModel> {

    private List<ResidenceTenantModel> data;
    private Context context;

    public ResidenceTenantAdaptor(Context context, int textViewResourceId, List<ResidenceTenantModel> data) {
        super(context, textViewResourceId, data);
        this.data = data;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            ResidenceTenantModel model = data.get(position);

            if (model != null) {
                if (model.titleRow) {
                    view = viewInflater.inflate(R.layout.list_item_residence_tenant_title, null);
                    ((TextView) view.findViewById(R.id.title)).setText(model.titleText);
                } else {
                    view = viewInflater.inflate(R.layout.list_item_residence_tenant, null);

                    TextView name = (TextView) view.findViewById(R.id.name);
                    TextView contactNumber = (TextView) view.findViewById(R.id.contactNumber);
                    ImageView attentionRequired = (ImageView) view.findViewById(R.id.attentionRequired);

                    name.setText(model.getName());
                    contactNumber.setText(model.contactNumber);
                    if (model.state == ResidenceTenantModel.ApplicationStatus.Completed)
                        attentionRequired.setVisibility(View.VISIBLE);
                }
            }
        }
        return view;
    }

    public List<ResidenceTenantModel> getData() {
        return this.data;
    }


}
