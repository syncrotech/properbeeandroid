package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.SearchTenantModel;

import java.util.List;

public class SearchTenantsAdaptor extends ArrayAdapter<SearchTenantModel> {

    private List<SearchTenantModel> tenantsList;
    private Context context;

    public SearchTenantsAdaptor(Context context, int textViewResourceId, List<SearchTenantModel> tenantsList) {
        super(context, textViewResourceId, tenantsList);
        this.tenantsList = tenantsList;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.list_item_search_tenant, null);

            SearchTenantModel tenant = tenantsList.get(position);

            ((TextView) view.findViewById(R.id.name)).setText(tenant.name);
            ((TextView) view.findViewById(R.id.email)).setText(tenant.email);
        }
        view.setBackgroundColor(tenantsList.get(position).selected ? 0xFF212121 : 0xFF374045);
        return view;
    }


    public List<SearchTenantModel> getData() {
        return this.tenantsList;
    }
}