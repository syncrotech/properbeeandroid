package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.viewModels.ReportMessageIdViewModel;

import java.util.List;

public class ReportListViewAdapter extends ArrayAdapter<ReportMessageIdViewModel> {

    private List<ReportMessageIdViewModel> reportList;
    private Context mContext;

    public ReportListViewAdapter(Context mContext, int textViewResourceId, List<ReportMessageIdViewModel> reportList) {
        super(mContext, textViewResourceId, reportList);
        this.reportList = reportList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.list_item_image_text, null);

            ReportMessageIdViewModel report = reportList.get(position);

            if(report != null){
                TextView message = (TextView) view.findViewById(R.id.list_item_text);
                ImageView imageView = (ImageView) view.findViewById(R.id.list_item_icon);
                View itemView = view.findViewById(R.id.item_view);

                message.setText(report.message);
                imageView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_reports_yellow_24px));
            }
        }
        return view;
    }

    public List<ReportMessageIdViewModel> getData(){
        return this.reportList;
    }
}
