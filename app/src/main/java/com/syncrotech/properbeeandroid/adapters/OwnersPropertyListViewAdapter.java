package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

import java.util.List;

/**
 * Created by ScryJ_000 on 7/10/2016.
 */
public class OwnersPropertyListViewAdapter extends ArrayAdapter<Residence> {

    private List<Residence> residenceList;
    private Context mContext;

    public OwnersPropertyListViewAdapter(Context mContext, int textViewResourceId, List<Residence> residenceList) {
        super(mContext, textViewResourceId, residenceList);
        this.residenceList = residenceList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            view = vi.inflate(R.layout.list_item_owner_property, null);
        }

        Residence residence = residenceList.get(position);

        if (residence != null) {
            TextView address = (TextView) view.findViewById(R.id.address);
            address.setText(residence.address);
        }

        return view;
    }

    public List<Residence> getData() {
        return this.residenceList;
    }
}
