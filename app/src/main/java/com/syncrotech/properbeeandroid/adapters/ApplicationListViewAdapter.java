package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.TenancyApplication;

import java.util.List;

public class ApplicationListViewAdapter extends ArrayAdapter<TenancyApplication> {

    private List<TenancyApplication> applicationList;
    private Context mContext;

    public ApplicationListViewAdapter(Context mContext, int textViewResourceId, List<TenancyApplication> applicationList) {
        super(mContext, textViewResourceId, applicationList);
        this.applicationList = applicationList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.application_list_view_item, null);

            TenancyApplication application = applicationList.get(position);

            if(application != null){
                TextView address = (TextView) view.findViewById(R.id.application_list_view_item_address);
                TextView state = (TextView) view.findViewById(R.id.application_list_view_item_state);

                address.setText(application.address);
                if(application.state == 0){
                    state.setText(mContext.getString(R.string.application_list_view_item_state_awaiting_info));
                }
                else if (application.state == 1){
                    state.setText(mContext.getString(R.string.application_list_view_item_state_pending));
                }
            }
        }
        return view;
    }

    public List<TenancyApplication> getData(){
        return this.applicationList;
    }
}
