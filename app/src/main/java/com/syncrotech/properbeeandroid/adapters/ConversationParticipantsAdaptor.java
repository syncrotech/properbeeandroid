package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.viewModels.ConversationParticipantViewModel;

import java.util.List;

public class ConversationParticipantsAdaptor extends ArrayAdapter<ConversationParticipantViewModel> {

    private List<ConversationParticipantViewModel> tenants;
    private Context context;

    public ConversationParticipantsAdaptor(Context context, int textViewResourceId, List<ConversationParticipantViewModel> tenants) {
        super(context, textViewResourceId, tenants);
        this.tenants = tenants;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.list_item_conversation_participant, null);

            final ConversationParticipantViewModel tenant = getItem(position);

            if(tenant != null){
                CheckBox checkBox = (CheckBox)view.findViewById(R.id.addParticipant);

                checkBox.setChecked(tenant.selected);

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        tenant.selected = b;
                    }
                });

                ((TextView)view.findViewById(R.id.userName)).setText(tenant.name);
            }
        }
        return view;
    }

    public List<ConversationParticipantViewModel> getData(){
        return this.tenants;
    }
}
