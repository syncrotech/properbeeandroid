package com.syncrotech.properbeeandroid.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.IconTextModel;

import java.util.List;

/**
 * Created by ScryJ_000 on 16/10/2016.
 */

public class HomeListAdapter extends ArrayAdapter<IconTextModel> {

    private List<IconTextModel> recentActivityList;
    private Context mContext;

    public HomeListAdapter(Context mContext, int textViewResourceId, List<IconTextModel> recentActivityList) {
        super(mContext, textViewResourceId, recentActivityList);
        this.recentActivityList = recentActivityList;
        this.mContext = mContext;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater viewInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = viewInflater.inflate(R.layout.list_item_image_text, null);

            IconTextModel iconTextModel = recentActivityList.get(position);

            if(iconTextModel != null){
                ImageView icon = (ImageView) view.findViewById(R.id.list_item_icon);
                TextView message = (TextView) view.findViewById(R.id.list_item_text);

                icon.setBackground(ContextCompat.getDrawable(mContext, iconTextModel.iconId));
                message.setText(iconTextModel.mesage);

            }
        }
        return view;
    }

    public List<IconTextModel> getData(){
        return this.recentActivityList;
    }
}
