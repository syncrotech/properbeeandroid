package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.InspectionListViewAdapter;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.InspectionHelper;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionPartialViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.PostNewInspectionViewModel;

import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OwnerInspection extends Fragment implements InspectionHelper.NewInspectionInterface, InspectionHelper.GetInspectionsForResidenceInterface{

    BroadcastReceiver dataSyncedReciever;
    View display, loading;
    FloatingActionButton fab;
    ListView inspectionListView;


    public OwnerInspection() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_owner_inspection, container, false);

        display = rootView.findViewById(R.id.display_view);
        loading = rootView.findViewById(R.id.loading_panel);

        fab = (FloatingActionButton) rootView.findViewById(R.id.add_inspection);

        inspectionListView = (ListView) rootView.findViewById(R.id.owner_inspection_list_view);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_create_inspection, null, false);

                final DatePicker datePicker = (DatePicker) rootView.findViewById(R.id.inspection_date);

                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_MONTH, 2);

                datePicker.setMinDate(cal.getTimeInMillis()- 1000);


                alertDialog.setView(rootView)
                        .setTitle("Create Inspection")
                        .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String inspectionDate = datePicker.getDayOfMonth() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getYear();
                                PostNewInspectionViewModel model = new PostNewInspectionViewModel(PrefHelper.getDefaultResidenceId(getContext()), inspectionDate);
                                InspectionHelper.sendNewInspection(getContext(), OwnerInspection.this, model);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).create()
                        .show();
            }
        });




        setUpReciever();

        if(((GlobalClass)getActivity().getApplicationContext()).dataSynced)
            setUpView();


        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataSyncedReciever, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataSyncedReciever);
    }

    private void setUpView(){
        loading.setVisibility(View.VISIBLE);
        display.setVisibility(View.GONE);

        InspectionHelper.getInspectionsForResidence(getContext(), OwnerInspection.this);
    }




    @Override
    public void afterPostNewInspection() {
        setUpView();
    }

    @Override
    public void postInspectionFailed() {
        Toast.makeText(getContext(), "Failed to create inspection", Toast.LENGTH_LONG).show();

        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);
    }

    @Override
    public void getInspectionsForResidenceSuccess(List<InspectionPartialViewModel> inspections) {
        InspectionListViewAdapter adapter = new InspectionListViewAdapter(getContext(), R.layout.list_item_inspection, inspections);
        inspectionListView.setAdapter(adapter);

        inspectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                InspectionPartialViewModel inspection = (InspectionPartialViewModel) inspectionListView.getItemAtPosition(i);

                Fragment fragment = InspectionInProgress.newInstance(inspection.id);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).addToBackStack("inspections").commit();
            }
        });

        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);
    }

    @Override
    public void getInspectionsForResidenceFailed() {
        Toast.makeText(getContext(), "Failed to get inspections", Toast.LENGTH_LONG).show();


    }
}
