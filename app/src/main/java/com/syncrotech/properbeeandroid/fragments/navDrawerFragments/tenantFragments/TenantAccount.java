package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenantAccount extends Fragment {
    FloatingActionButton fabEdit;
    Button changePassword;
    BroadcastReceiver dataSyncedReciever;


    public TenantAccount() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_tenant_account, container, false);

        fabEdit = (FloatingActionButton) rootView.findViewById(R.id.edit_account);
        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;

                try{
                    fragment = new TenantAccountEdit();
                } catch (Exception e){
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("Tenant Account").commit();
            }
        });

        changePassword = (Button) rootView.findViewById(R.id.owner_account_change_password);

        setUpReciever();
        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataSyncedReciever, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataSyncedReciever);
    }

    private void setUpView(){

    }

}
