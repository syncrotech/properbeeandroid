package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.ReportListViewAdapter;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.sharedFragments.SharedSingleReport;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ReportHelper;
import com.syncrotech.properbeeandroid.models.viewModels.ReportCreateViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.ReportMessageIdViewModel;
import com.syncrotech.properbeeandroid.services.CustomFirebaseMessageService;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenantReport extends Fragment implements ReportHelper.GetReportsInterface, ReportHelper.PostReportInterface {


    private static final int CAMERA_REQUEST = 1888;

    View display, loading;
    ListView reportListView;
    BroadcastReceiver dataSyncedReciever, pushReceived;
    FloatingActionButton fab;
    AlertDialog dialog;
    ImageView photoView;
    Bitmap photo;


    public TenantReport() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_tenant_report, container, false);

        display = rootView.findViewById(R.id.display_view);
        loading = rootView.findViewById(R.id.loading_panel);
        reportListView = (ListView) rootView.findViewById(R.id.report_list_view);

        reportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ReportListViewAdapter adapter = (ReportListViewAdapter) reportListView.getAdapter();
                ReportMessageIdViewModel report = adapter.getData().get(i);

                Fragment fragment = SharedSingleReport.newInstance(report.id);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(PrefHelper.getTokenDetails(getContext()).getUserRole().equals("Landlord") ? R.id.owner_content : R.id.tenant_content, fragment).addToBackStack("Tenant Report").commit();
            }
        });

        fab = (FloatingActionButton) rootView.findViewById(R.id.add_report);
        if (PrefHelper.getTokenDetails(getContext()).getUserRole().equals("Landlord")) {
            fab.setVisibility(View.GONE);
        } else {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.dialog_add_report, null);
                    builder.setView(dialogView);

                    photoView = (ImageView) dialogView.findViewById(R.id.dialog_report_photo);
                    Button takePhoto = (Button) dialogView.findViewById(R.id.take_photo);
                    final TextInputLayout message = (TextInputLayout) dialogView.findViewById(R.id.message);

                    takePhoto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }
                    });

                    builder.setTitle("Create Report")
                            .setPositiveButton("Create", null)
                            .setNegativeButton("Cancel", null);

                    dialog = builder.create();

                    dialog.show();

                    final Button positive = dialog.getButton(DialogInterface.BUTTON_POSITIVE);

                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String reportMessage = message.getEditText().getText().toString();
                            if (reportMessage.isEmpty()) {
                                message.setError("You must enter a message for the report.");
                                return;
                            }

                            ReportCreateViewModel model = new ReportCreateViewModel(reportMessage, photo);
                            ReportHelper.postNewReport(getContext(), TenantReport.this, model);

                            positive.setEnabled(false);
                        }
                    });
                }
            });
        }


        photo = null;

        setUpReciever();
        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        manager.registerReceiver(dataSyncedReciever, filter);

        pushReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    if(extras.get("ActionArea") == CustomFirebaseMessageService.ActionAreas.Report)
                        ReportHelper.getReports(getContext(), TenantReport.this);
                }
            }
        };

        filter = new IntentFilter();
        filter.addAction(CustomFirebaseMessageService.MESSAGE_RECEIVED);

        manager.registerReceiver(pushReceived, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.unregisterReceiver(dataSyncedReciever);
        manager.unregisterReceiver(pushReceived);
    }

    private void setUpView() {
        loading.setVisibility(View.VISIBLE);
        display.setVisibility(View.GONE);

        ReportHelper.getReports(getContext(), TenantReport.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bm = (Bitmap) data.getExtras().get("data");
        photo = bm;
        photoView.setImageBitmap(bm);
    }

    @Override
    public void getReportsSuccess(List<ReportMessageIdViewModel> reportList) {
        ReportListViewAdapter adapter = new ReportListViewAdapter(getContext(), R.layout.list_item_image_text, reportList);
        reportListView.setAdapter(adapter);


        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);
    }

    @Override
    public void getReportsFailed() {

    }

    @Override
    public void postReportSuccess() {
        dialog.dismiss();
        setUpView();

    }

    @Override
    public void postReportFailed() {

    }
}
