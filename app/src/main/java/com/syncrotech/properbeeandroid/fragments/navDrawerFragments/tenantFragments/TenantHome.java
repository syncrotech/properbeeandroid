package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.activities.TennantActivity;
import com.syncrotech.properbeeandroid.adapters.ApplicationListViewAdapter;
import com.syncrotech.properbeeandroid.adapters.NotificationListViewAdapter;
import com.syncrotech.properbeeandroid.fragments.Application;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ApplicationHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.NotificationHelper;
import com.syncrotech.properbeeandroid.models.TenancyApplication;
import com.syncrotech.properbeeandroid.models.dbModels.Notification;
import com.syncrotech.properbeeandroid.services.CustomFirebaseMessageService;

import java.util.List;

public class TenantHome extends Fragment implements ApplicationHelper.ApplicationInterface, NotificationHelper.NotificationInterface, NotificationHelper.DismissNotificationInterface {
    View home, homeNoResidence, loading;
    ApplicationListViewAdapter applicationListViewAdapter;
    ListView applicationListView, recentActivityListView;
    List<TenancyApplication> applications;
    BroadcastReceiver dataSyncedReciever, pushReceived;
    TextView welcome;

    public TenantHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_tenant_home, container, false);

        home = rootView.findViewById(R.id.home_welcome);
        homeNoResidence = rootView.findViewById(R.id.tenant_home_no_residence);
        loading = rootView.findViewById(R.id.loading_panel);
        recentActivityListView = (ListView) rootView.findViewById(R.id.home_notifications);
        applicationListView = (ListView) rootView.findViewById(R.id.tenant_home_no_residence_application_list);
        welcome= (TextView) rootView.findViewById(R.id.home_welcome_title);

        home.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

        setUpReciever();

        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager manager =  LocalBroadcastManager.getInstance(getActivity());

        manager.registerReceiver(dataSyncedReciever, filter);

        pushReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    NotificationHelper.getNotifications(getContext(), TenantHome.this);
                }
            }
        };

        filter = new IntentFilter();
        filter.addAction(CustomFirebaseMessageService.MESSAGE_RECEIVED);
        manager.registerReceiver(pushReceived, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.unregisterReceiver(dataSyncedReciever);
        manager.unregisterReceiver(pushReceived);
    }

    private void setUpView() {
        if (((GlobalClass) getActivity().getApplicationContext()).selectedResidence == null) {
            ApplicationHelper.getApplications(this, getContext());
        } else {
            NotificationHelper.getNotifications(getContext(), TenantHome.this);
        }
    }

    @Override
    public void applicationFailed(String errorMessage) {

    }

    @Override
    public void applicationSuccess(final List<TenancyApplication> applicationList) {
        applications = applicationList;
        applicationListViewAdapter = new ApplicationListViewAdapter(getContext(), R.layout.application_list_view_item, applications);
        recentActivityListView.setAdapter(applicationListViewAdapter);
        recentActivityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TenancyApplication application = applications.get(i);
                if(application != null){
                    GlobalClass gc = (GlobalClass) getActivity().getApplicationContext();
                    gc.tenancyApplication = application;

                    Fragment fragment = null;

                    try{
                        fragment = new Application();
                    } catch (Exception e){
                        e.printStackTrace();
                    }

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("Tenant Home").commit();
                }

            }
        });

        welcome.setText("Applications");



        home.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void applicationUpdateSuccess() {

    }

    @Override
    public void notificationSuccess(final List<Notification> notificationList) {
        recentActivityListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Notification notification = notificationList.get(i);
                if (notification != null) {
                    Fragment fragment = notification.getActionArea(getContext());

                    TennantActivity activity = (TennantActivity) getActivity();
                    activity.setTitle(notification.getTitle());

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("Tenant Home").commit();
                }
            }
        });

        recentActivityListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Notification notification = notificationList.get(i);
                if (notification != null) {
                    NotificationHelper.dismissNotification(notification.id, getContext(), TenantHome.this);
                    NotificationListViewAdapter adapter = ((NotificationListViewAdapter)recentActivityListView.getAdapter());
                    List<Notification> data = adapter.getData();
                    data.remove(notification);
                    adapter.notifyDataSetChanged();

                }
                return true;
            }
        });


        NotificationListViewAdapter recentActivityAdapter = new NotificationListViewAdapter(getContext(), R.layout.list_item_image_text, notificationList);
        recentActivityListView.setAdapter(recentActivityAdapter);


        home.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);

    }

    @Override
    public void notificationFailed(String errorMessage) {

    }

    @Override
    public void dismissNotificationFailed(String errorMessage) {

    }
}
