package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.sharedFragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.ConversationParticipantsAdaptor;
import com.syncrotech.properbeeandroid.adapters.MessagesAdapter;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ConversationsHelper;
import com.syncrotech.properbeeandroid.models.Message;
import com.syncrotech.properbeeandroid.models.dbModels.Owner;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;
import com.syncrotech.properbeeandroid.models.dbModels.Tenant;
import com.syncrotech.properbeeandroid.models.viewModels.AddParticipantViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.ConversationParticipantViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.SendMessageViewModel;
import com.syncrotech.properbeeandroid.services.CustomFirebaseMessageService;

import java.util.ArrayList;
import java.util.List;


public class SharedSingleConversation extends Fragment implements ConversationsHelper.getMessagesInterface, ConversationsHelper.sendMessageInterface, ConversationsHelper.addParticipantInterface, ConversationsHelper.leaveConversationInterface {

    public String conversationId;
    BroadcastReceiver pushReceived;
    ListView listView;
    View loadingView, messagesView;
    EditText messageInput;
    ImageButton sendButton;
    AlertDialog dialog;

    public SharedSingleConversation() {

    }

    public static Fragment newInstance(String conversationId) {
        SharedSingleConversation fragment = new SharedSingleConversation();
        fragment.conversationId = conversationId;
        return fragment;
    }

    private void scrollToBottom(){
        listView.setSelection(listView.getAdapter().getCount() - 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shared_single_conversation, container, false);

        setHasOptionsMenu(true);

        listView = (ListView)rootView.findViewById(R.id.conversationListView);
        loadingView = rootView.findViewById(R.id.loading_panel);
        messagesView = rootView.findViewById(R.id.messagesView);
        messageInput = (EditText) rootView.findViewById(R.id.messageInput);
        sendButton = (ImageButton) rootView.findViewById(R.id.send);

        IntentFilter filter = new IntentFilter(CustomFirebaseMessageService.MESSAGE_RECEIVED);

        pushReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    if (extras.get("ActionArea") == CustomFirebaseMessageService.ActionAreas.Message && extras.get("ActionId").equals(conversationId))
                        ConversationsHelper.getMessages(getContext(), SharedSingleConversation.this, conversationId);
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(pushReceived, filter);

        ConversationsHelper.getMessages(getContext(), this, conversationId);

        loadingView.setVisibility(View.VISIBLE);
        messagesView.setVisibility(View.GONE);

        return rootView;
    }

    @Override
    public void afterAddParticipant() {
        dialog.dismiss();
    }

    @Override
    public void addParticipantFailed() {
        Toast.makeText(getContext(), "Could not add participants", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStop() {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getContext());
        broadcastManager.unregisterReceiver(pushReceived);
        super.onStop();
    }

    @Override
    public void afterGetMessages(List<Message> messages) {
        MessagesAdapter adapter = new MessagesAdapter(getContext(), R.layout.message_list_item, messages);
        listView.setAdapter(adapter);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = messageInput.getText().toString();
                if (message == null || message.isEmpty()) return;
                messageInput.setText("");
                SendMessageViewModel model = new SendMessageViewModel();
                model.conversationId = conversationId;
                model.message = message;
                ConversationsHelper.sendMessage(getContext(), SharedSingleConversation.this, model);
            }
        });

        scrollToBottom();

        messagesView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_single_conversation, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_add_participant:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_add_participants, null);
                builder.setView(dialogView);

                Residence residence = ((GlobalClass)getActivity().getApplicationContext()).selectedResidence;

                List<ConversationParticipantViewModel> participants = new ArrayList<>();

                for(Tenant tenant : residence.tenants){
                    ConversationParticipantViewModel model = new ConversationParticipantViewModel();
                    model.id = tenant.id;
                    model.selected = false;
                    model.name = tenant.firstName + " " + tenant.lastName;
                    participants.add(model);
                }

                for (Owner owner : residence.owners){
                    ConversationParticipantViewModel model = new ConversationParticipantViewModel();
                    model.id = owner.id;
                    model.selected = false;
                    model.name = owner.firstName + " " + owner.lastName;
                    participants.add(model);
                }

                final ListView participantsListView = (ListView) dialogView.findViewById(R.id.conversationParticipantsListView);

                ConversationParticipantsAdaptor adaptor = new ConversationParticipantsAdaptor(getContext(), R.layout.list_item_conversation_participant, participants);
                participantsListView.setAdapter(adaptor);

                builder.setTitle("Add participants to conversation")
                        .setPositiveButton("Add", null)
                        .setNegativeButton("Cancel", null);

                dialog =  builder.create();

                dialog.show();

                final Button positive = dialog.getButton(DialogInterface.BUTTON_POSITIVE);

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<ConversationParticipantViewModel> participants = ((ConversationParticipantsAdaptor)participantsListView.getAdapter()).getData();
                        AddParticipantViewModel model = new AddParticipantViewModel();
                        model.participants = new ArrayList<>();
                        for(ConversationParticipantViewModel participant : participants){
                            if(participant.selected) model.participants.add(participant.id);
                        }
                        model.conversationId = conversationId;
                        ConversationsHelper.addParticipantToConversation(getContext(), SharedSingleConversation.this, model);
                        positive.setEnabled(false);
                    }
                });
                return true;
            case R.id.menu_leave_conversation:
                ConversationsHelper.leaveConversation(getContext(), this, conversationId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void getMessagesFailed() {
        Toast.makeText(getContext(), "Could not retrieve messages", Toast.LENGTH_LONG).show();
    }

    @Override
    public void afterLeaveConversation() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void leaveConversationFailed() {
        Toast.makeText(getContext(), "Could not leave conversation", Toast.LENGTH_LONG).show();
    }

    @Override
    public void afterSendMessage(Message message) {
        MessagesAdapter adapter = ((MessagesAdapter)listView.getAdapter());
        adapter.getData().add(message);
        adapter.notifyDataSetChanged();

        scrollToBottom();
    }

    @Override
    public void sendMessageFailed() {
        Toast.makeText(getContext(), "Could not send message", Toast.LENGTH_LONG).show();
    }


}
