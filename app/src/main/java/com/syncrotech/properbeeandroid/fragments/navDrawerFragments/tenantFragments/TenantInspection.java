package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.InspectionListViewAdapter;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.InspectionInProgress;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.InspectionHelper;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionPartialViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenantInspection extends Fragment implements InspectionHelper.GetInspectionsForResidenceInterface{
    BroadcastReceiver dataSyncedReciever;
    View display, loading;
    ListView inspectionListView;


    public TenantInspection() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_owner_inspection, container, false);

        display = rootView.findViewById(R.id.display_view);
        loading = rootView.findViewById(R.id.loading_panel);

        inspectionListView = (ListView) rootView.findViewById(R.id.owner_inspection_list_view);

        setUpReciever();

        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced)
            setUpView();

        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataSyncedReciever, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataSyncedReciever);
    }

    private void setUpView() {
        loading.setVisibility(View.VISIBLE);
        display.setVisibility(View.GONE);

        InspectionHelper.getInspectionsForResidence(getContext(), TenantInspection.this);
    }


    @Override
    public void getInspectionsForResidenceSuccess(List<InspectionPartialViewModel> inspections) {
        InspectionListViewAdapter adapter = new InspectionListViewAdapter(getContext(), R.layout.list_item_inspection, inspections);
        inspectionListView.setAdapter(adapter);

        inspectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                InspectionPartialViewModel inspection = (InspectionPartialViewModel) inspectionListView.getItemAtPosition(i);

                Fragment fragment = InspectionInProgress.newInstance(inspection.id);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("inspections").commit();
            }
        });

        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);

    }

    @Override
    public void getInspectionsForResidenceFailed() {

    }
}

