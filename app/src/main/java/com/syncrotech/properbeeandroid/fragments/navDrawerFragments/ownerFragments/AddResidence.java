package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ResidenceHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

public class AddResidence extends Fragment implements ResidenceHelper.PostResidenceInterface {

    Spinner propertyType, parking, pets;
    EditText address, rentalPrice, bedrooms, bathrooms, tenants, details;
    Button done;

    public AddResidence() {
    }

    public static Fragment newInstance() {
        Fragment fragment = new AddResidence();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_residence, container, false);
        Resources res = getResources();
        String[] propertyTypes = res.getStringArray(R.array.residence_type);
        String[] parkingTypes = res.getStringArray(R.array.parking);
        String[] petTypes = res.getStringArray(R.array.pets);

        propertyType = (Spinner) rootView.findViewById(R.id.add_residence_type);
        parking = (Spinner) rootView.findViewById(R.id.add_residence_parking);
        pets = (Spinner) rootView.findViewById(R.id.add_residence_pets);
        address = (EditText) rootView.findViewById(R.id.add_residence_address);
        rentalPrice = (EditText) rootView.findViewById(R.id.add_residence_rental_price);
        bedrooms = (EditText) rootView.findViewById(R.id.add_residence_bedrooms);
        bathrooms = (EditText) rootView.findViewById(R.id.add_residence_bathrooms);
        tenants = (EditText) rootView.findViewById(R.id.add_residence_max_tenants);
        details = (EditText) rootView.findViewById(R.id.add_residence_details);
        done = (Button) rootView.findViewById(R.id.add_residence_done);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Residence residence = new Residence(
                        address.getText().toString(),
                        propertyType.getSelectedItemPosition(),
                        Double.parseDouble(rentalPrice.getText().toString()),
                        Integer.parseInt(bedrooms.getText().toString()),
                        Integer.parseInt(tenants.getText().toString()),
                        Integer.parseInt(bathrooms.getText().toString()),
                        parking.getSelectedItemPosition(),
                        pets.getSelectedItemPosition(),
                        details.getText().toString()
                );
                ResidenceHelper.postResidence(getContext(), AddResidence.this, residence);
            }
        });

        ArrayAdapter<String> typeAdaptor = new ArrayAdapter<>(getContext(), R.layout.spinner_selected_item, propertyTypes);
        typeAdaptor.setDropDownViewResource(R.layout.spinner_item);
        propertyType.setAdapter(typeAdaptor);

        ArrayAdapter<String> parkingAdaptor = new ArrayAdapter<>(getContext(), R.layout.spinner_selected_item, parkingTypes);
        parkingAdaptor.setDropDownViewResource(R.layout.spinner_item);
        parking.setAdapter(parkingAdaptor);

        ArrayAdapter<String> petAdaptor = new ArrayAdapter<>(getContext(), R.layout.spinner_selected_item, petTypes);
        petAdaptor.setDropDownViewResource(R.layout.spinner_item);
        pets.setAdapter(petAdaptor);

        rentalPrice.setText(getString(R.string.display_double, 0.00));
        bedrooms.setText(getString(R.string.display_int, 0));
        tenants.setText(getString(R.string.display_int, 0));
        bathrooms.setText(getString(R.string.display_int, 0));

        return rootView;
    }

    @Override
    public void postFailed(String message) {
        Fragment fragment = OwnerProperty.newInstance();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).commit();
    }

    @Override
    public void postSuccess() {
        Fragment fragment = OwnerProperty.newInstance();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).commit();
    }


}
