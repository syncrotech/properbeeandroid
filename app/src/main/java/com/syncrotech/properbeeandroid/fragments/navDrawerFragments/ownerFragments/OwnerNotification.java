package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.activities.OwnerActivity;
import com.syncrotech.properbeeandroid.adapters.NotificationListViewAdapter;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.NotificationHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Notification;
import com.syncrotech.properbeeandroid.services.CustomFirebaseMessageService;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OwnerNotification extends Fragment implements NotificationHelper.NotificationInterface, NotificationHelper.DismissNotificationInterface{

    ListView notificationListView;
    View notificationView, loadingView;
    BroadcastReceiver dataSyncedReciever, pushReceived;

    public OwnerNotification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_owner_notifation, container, false);

        notificationView = rootView.findViewById(R.id.owner_notification_view);
        loadingView = rootView.findViewById(R.id.loading_panel);
        notificationListView = (ListView) rootView.findViewById(R.id.owner_notification_notifications);


        notificationView.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);



        setUpReciever();
        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.registerReceiver(dataSyncedReciever, filter);

        pushReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    NotificationHelper.getNotifications(getContext(), OwnerNotification.this);
                }
            }
        };

        filter = new IntentFilter();
        filter.addAction(CustomFirebaseMessageService.MESSAGE_RECEIVED);
        manager.registerReceiver(pushReceived, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.unregisterReceiver(dataSyncedReciever);
        manager.unregisterReceiver(pushReceived);
    }

    private void setUpView(){
        NotificationHelper.getNotifications(getContext(), this);
    }


    @Override
    public void notificationSuccess(final List<Notification> notificationList) {
        notificationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Notification notification = notificationList.get(i);
                if (notification != null) {
                    Fragment fragment = notification.getActionArea(getContext());

                    OwnerActivity activity = (OwnerActivity) getActivity();
                    activity.setTitle(notification.getTitle());

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).addToBackStack("Owner Notification").commit();
                }
            }
        });

        notificationListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Notification notification = notificationList.get(i);
                if (notification != null) {
                    NotificationHelper.dismissNotification(notification.id, getContext(), OwnerNotification.this);
                    NotificationListViewAdapter adapter = ((NotificationListViewAdapter)notificationListView.getAdapter());
                    List<Notification> data = adapter.getData();
                    data.remove(notification);
                    adapter.notifyDataSetChanged();

                }
                return false;
            }
        });


        NotificationListViewAdapter recentActivityAdapter = new NotificationListViewAdapter(getContext(), R.layout.list_item_image_text, notificationList);
        notificationListView.setAdapter(recentActivityAdapter);


        notificationView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);

    }

    @Override
    public void notificationFailed(String errorMessage) {

    }

    @Override
    public void dismissNotificationFailed(String errorMessage) {

    }
}
