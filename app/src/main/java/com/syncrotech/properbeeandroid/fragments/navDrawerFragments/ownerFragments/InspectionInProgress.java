package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.InspectionHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Inspection;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionNoteWithId;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionPhotoWithId;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionStateIdViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectionInProgress extends Fragment implements InspectionHelper.SendPhotoInterface, InspectionHelper.GetInspectionInterface, InspectionHelper.SendNoteInterface, InspectionHelper.SubmitInspectionInterface {

    private static final int CAMERA_REQUEST = 1888;

    String inspectionId;
    ListView noteListView;
    ArrayList<String> notes;
    Button takePhoto, addNote, submit;
    ArrayList<Bitmap> photoList;
    LinearLayout photoLayout;
    View display, loading;
    TextView title;
    boolean isLandlord;




    public InspectionInProgress() {
        // Required empty public constructor
    }


    public static Fragment newInstance(String inspectionId){
        InspectionInProgress fragment = new InspectionInProgress();
        fragment.inspectionId = inspectionId;
        return  fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_inspection_in_progress, container, false);

        display = rootView.findViewById(R.id.display_view);
        loading = rootView.findViewById(R.id.loading_panel);

        noteListView = (ListView) rootView.findViewById(R.id.inspection_in_progress_notes);
        notes = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, notes);
        noteListView.setAdapter(adapter);

        title = (TextView) rootView.findViewById(R.id.title_text);
        title.setText("Inspection for " + PrefHelper.getDefaultResidenceName(getContext()));

        addNote = (Button) rootView.findViewById(R.id.inspection_in_progress_add_note);
        addNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_edit_text, null, false);

                final TextInputLayout note = (TextInputLayout) rootView.findViewById(R.id.text_input_layout);
                note.getEditText().setHint("Note");

                alertDialog.setView(rootView)
                        .setTitle("Add Note")
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(!note.getEditText().getText().toString().isEmpty())
                                {
                                    notes.add(note.getEditText().getText().toString());
                                    ArrayAdapter adapter = ((ArrayAdapter) noteListView.getAdapter());
                                    adapter.notifyDataSetChanged();

                                    InspectionNoteWithId noteWithId = new InspectionNoteWithId(inspectionId, note.getEditText().getText().toString());
                                    InspectionHelper.sendNoteViewModel(getContext(), InspectionInProgress.this, noteWithId);
                                }

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).create()
                        .show();
            }
        });

        photoLayout = (LinearLayout) rootView.findViewById(R.id.inspection_in_progress_photo_display);

        photoList = new ArrayList<>();

        takePhoto = (Button) rootView.findViewById(R.id.inspection_in_progress_take_photo);
        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        submit = (Button) rootView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_application_response, null, false);

                final Spinner response = (Spinner) rootView.findViewById(R.id.dialog_response);

                final ArrayAdapter<CharSequence> responseAdapter = ArrayAdapter.createFromResource(getContext(),
                        R.array.inspection, android.R.layout.simple_spinner_item);
                responseAdapter.setDropDownViewResource(R.layout.spinner_item);
                response.setAdapter(responseAdapter);

                alertDialog.setView(rootView)
                        .setTitle("Submit")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                InspectionHelper.submitInspection(getContext(), InspectionInProgress.this,
                                        new InspectionStateIdViewModel(inspectionId, response.getSelectedItemPosition() + 1));
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).create()
                        .show();
            }
        });

        isLandlord = PrefHelper.getTokenDetails(getContext()).getUserRole().equals("Landlord");

        loading.setVisibility(View.VISIBLE);
        display.setVisibility(View.GONE);

        InspectionHelper.getInspection(getContext(), InspectionInProgress.this, inspectionId);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK){
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photoList.add(photo);
            displayPhotos();
            InspectionPhotoWithId photoWithId = new InspectionPhotoWithId(inspectionId, photo);
            InspectionHelper.sendPhotoViewModel(getContext(), InspectionInProgress.this, photoWithId);
        }
    }

    private void displayPhotos(){
        createImageView(photoList.size() - 1);
    }

    @Override
    public void sendPhotoSuccess() {

    }

    @Override
    public void sendPhotoFailed() {

    }

    @Override
    public void getInspectionSucces(Inspection inspection) {

        notes.addAll(inspection.notes);
        ArrayAdapter adapter = ((ArrayAdapter) noteListView.getAdapter());
        adapter.notifyDataSetChanged();

        photoList = inspection.photos;
        for(int i = 0; i < photoList.size(); i++) {
            createImageView(i);
        }

        if(inspection.state != 0 || !isLandlord){
            addNote.setVisibility(View.GONE);
            takePhoto.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
        }


        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);
    }

    private void createImageView(int id){
        final ImageView imageView = new ImageView(getContext());
        imageView.setId(id);
        imageView.setPadding(2, 2, 2, 2);
        imageView.setImageBitmap(photoList.get(id));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_display_photo, null, false);

                ImageView photoDisplay = (ImageView) rootView.findViewById(R.id.display_photo);
                photoDisplay.setImageBitmap(getResizedBitmap(photoList.get(imageView.getId()), 400));

                alertDialog.setView(rootView)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }).create()
                        .show();

            }
        });

        photoLayout.addView(imageView);

    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scale = ((float) newWidth) / width;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scale, scale);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    @Override
    public void getInspectionFailed() {

    }

    @Override
    public void sendNoteSuccess() {

    }

    @Override
    public void sendNoteFailed() {

    }

    @Override
    public void submitInspecctionSuccess() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void submitInspectionFailed() {

    }
}
