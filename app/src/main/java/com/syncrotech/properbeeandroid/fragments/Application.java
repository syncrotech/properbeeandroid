package com.syncrotech.properbeeandroid.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.OccupantListViewAdapter;
import com.syncrotech.properbeeandroid.adapters.PetListViewAdapter;
import com.syncrotech.properbeeandroid.helpers.DateHelper;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ApplicationHelper;
import com.syncrotech.properbeeandroid.models.Occupant;
import com.syncrotech.properbeeandroid.models.Pet;
import com.syncrotech.properbeeandroid.models.TenancyApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 */
public class Application extends Fragment implements ApplicationHelper.ApplicationInterface {
    private static final int NUM_PAGES = 13;

    TenancyApplication tenancyApplication;

    ScrollView scrollView;
    Button prev, next;
    ViewFlipper applicationFlipper;
    Integer page;
    TextView pageIndicator, applicationTitle;
    TextInputLayout currentAddress, currentLength, currentLeaveReason, currentLandlordName, currentLandlordPhone,
            previousAddress, previousLength, previousLeaveReason, previousLandlordName, previousLandlordPhone,
            newRentalPeriod, newBorderAmount, newCommercialDescription,
            tribunalDescription,
            crimeDescription,
            employedOccupation, employedLength, employedName, employedAddress, employedSupervisorName, employedSupervisorContact,
            studyingWhere, studyingWhat, studyingAdminName, studyingAdminContact, studyingIncome, studyingIncomeAmount,
            winzAmount, winzCategory, winzAccSupp,
            bondSavings, bondWINZ, bondTransfer,
            houseAddress,
            vehicleMake, vehicleModel, vehicleColour, vehicleRego,
            referenceName, referenceAddress, referencePhoneA, referencePhoneB,
            referenceRelativeName, referenceRelativeAddress, referenceRelativePhoneA, referenceRelativePhoneB;
    Spinner currentLivingArrangements,
            employedType,
            winzDay;
    DatePicker studyingCourseEnd,
            winzEnd;
    ListView newOccupants, newPets;
    CheckBox newBorders, newCommercial,
            tribunal,
            crime, crimeBail, crimeBailBorder, crimeMonitoring, crimeMonitoringBorder, crimeRestriction, crimeRestrictionBorder, crimeBankrupt,
            employed, employedSelf,
            studying,
            winz, winzRedirect,
            house,
            vehicle;
    OccupantListViewAdapter occupantListViewAdapter;
    PetListViewAdapter petListViewAdapter;
    ArrayList<Occupant> occupants;
    ArrayList<Pet> pets;


    public Application() {
        // Required empty public constructor
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AppBarLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_application, container, false);

        tenancyApplication = PrefHelper.getTenancyApplication(((GlobalClass) getActivity().getApplicationContext()).userName, getContext());

        scrollView = (ScrollView) rootView.findViewById(R.id.scroll_view);

        applicationFlipper = (ViewFlipper) rootView.findViewById(R.id.application_view_flipper);

        prev = (Button) rootView.findViewById(R.id.application_prev_page);
        next = (Button) rootView.findViewById(R.id.application_next_page);

        page = 0;

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page > 0) {
                    validate();
                    if (page == 1)
                        prev.setEnabled(false);
                    else if (!next.isEnabled())
                        next.setEnabled(true);
                    saveApplication();
                    page--;
                    changeApplicationPage();
                }
            }
        });
        prev.setEnabled(false);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page < NUM_PAGES - 1) {
                    Boolean valid = validate();
                    if (valid) {
                        if (!prev.isEnabled())
                            prev.setEnabled(true);
                        else if (page == NUM_PAGES - 2)
                            next.setText("Submit");
                        tenancyApplication.state = 0;
                        saveApplication();
                        page++;
                        changeApplicationPage();
                    }
                } else {
                    Boolean valid = validate();
                    if (valid) {
                        tenancyApplication.state = 1;
                        saveApplication();
                    }

                }
            }
        });

        pageIndicator = (TextView) rootView.findViewById(R.id.view_page_page_number);
        applicationTitle = (TextView) rootView.findViewById(R.id.application_title);


        currentAddress = (TextInputLayout) rootView.findViewById(R.id.application_current_address);
        currentLivingArrangements = (Spinner) rootView.findViewById(R.id.application_current_address_living_arrangement);
        currentLength = (TextInputLayout) rootView.findViewById(R.id.application_current_address_length);
        currentLeaveReason = (TextInputLayout) rootView.findViewById(R.id.application_current_address_leave_reason);
        currentLandlordName = (TextInputLayout) rootView.findViewById(R.id.application_current_address_landlord_name);
        currentLandlordPhone = (TextInputLayout) rootView.findViewById(R.id.application_current_address_landlord_phone);

        final ArrayAdapter<CharSequence> livingArangementsAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.living_arrangements, android.R.layout.simple_spinner_item);
        livingArangementsAdapter.setDropDownViewResource(R.layout.spinner_item);
//        livingArangementsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currentLivingArrangements.setAdapter(livingArangementsAdapter);

        currentLivingArrangements.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 4) {
                    currentLandlordName.getEditText().setText("");
                    currentLandlordPhone.getEditText().setText("");
                    currentLandlordName.getEditText().setEnabled(false);
                    currentLandlordPhone.getEditText().setEnabled(false);
                } else {
                    currentLandlordName.getEditText().setEnabled(true);
                    currentLandlordPhone.getEditText().setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        previousAddress = (TextInputLayout) rootView.findViewById(R.id.application_previous_address);
        previousLength = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_length);
        previousLeaveReason = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_leave_reason);
        previousLandlordName = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_landlord_name);
        previousLandlordPhone = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_landlord_phone);


        newOccupants = (ListView) rootView.findViewById(R.id.application_new_address_occupants);
        newPets = (ListView) rootView.findViewById(R.id.application_new_address_pets);
        newRentalPeriod = (TextInputLayout) rootView.findViewById(R.id.application_new_address_rental_period);
        newBorders = (CheckBox) rootView.findViewById(R.id.application_new_address_borders);
        newBorderAmount = (TextInputLayout) rootView.findViewById(R.id.application_new_address_border_amount);
        newCommercial = (CheckBox) rootView.findViewById(R.id.application_new_address_commercial);
        newCommercialDescription = (TextInputLayout) rootView.findViewById(R.id.application_new_address_commercial_description);

        occupants = new ArrayList<>();

//        setListViewHeightBasedOnChildren(newOccupants);
        newOccupants.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        newOccupants.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectOccupant(position);
            }
        });
        newOccupants.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deleteOccupant(position);
                return true;
            }
        });

        pets = new ArrayList<>();

        newPets.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        newPets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectPet(position);
            }
        });
        newPets.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deletePet(position);
                return true;
            }
        });

        newBorders.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newBorderAmount.getEditText().setText("");
                newBorderAmount.getEditText().setEnabled(newBorders.isChecked());

                crimeBailBorder.setChecked(false);
                crimeMonitoringBorder.setChecked(false);
                crimeRestrictionBorder.setChecked(false);

                crimeBailBorder.setEnabled(newBorders.isChecked());
                crimeMonitoringBorder.setEnabled(newBorders.isChecked());
                crimeRestrictionBorder.setEnabled(newBorders.isChecked());
            }
        });

        newCommercial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newCommercialDescription.getEditText().setText("");
                newCommercialDescription.getEditText().setEnabled(newCommercial.isChecked());
            }
        });

        tribunal = (CheckBox) rootView.findViewById(R.id.application_tribunal);
        tribunalDescription = (TextInputLayout) rootView.findViewById(R.id.application_tribunal_description);

        tribunal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                tribunalDescription.getEditText().setText("");
                tribunalDescription.getEditText().setEnabled(tribunal.isChecked());
            }
        });

        crime = (CheckBox) rootView.findViewById(R.id.application_criminal_conviction);
        crimeDescription = (TextInputLayout) rootView.findViewById(R.id.application_criminal_conviction_reason);
        crimeBail = (CheckBox) rootView.findViewById(R.id.application_criminal_bail);
        crimeBailBorder = (CheckBox) rootView.findViewById(R.id.application_criminal_bail_border);
        crimeMonitoring = (CheckBox) rootView.findViewById(R.id.application_criminal_monitoring);
        crimeMonitoringBorder = (CheckBox) rootView.findViewById(R.id.application_criminal_monitoring_border);
        crimeRestriction = (CheckBox) rootView.findViewById(R.id.application_criminal_restrictions);
        crimeRestrictionBorder = (CheckBox) rootView.findViewById(R.id.application_criminal_restrictions_border);
        crimeBankrupt = (CheckBox) rootView.findViewById(R.id.application_criminal_bankrupt);

        crime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                crimeDescription.getEditText().setText("");
                crimeDescription.getEditText().setEnabled(tribunal.isChecked());
            }
        });

        employed = (CheckBox) rootView.findViewById(R.id.application_income_employment_employed);
        employedSelf = (CheckBox) rootView.findViewById(R.id.application_income_employment_self_employed);
        employedOccupation = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_occupation);
        employedType = (Spinner) rootView.findViewById(R.id.application_income_employment_occupation_hours);
        employedLength = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_occupation_length);
        employedName = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_employer_name);
        employedAddress = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_employer_address);
        employedSupervisorName = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_supervisor_name);
        employedSupervisorContact = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_supervisor_contact);

        employed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (employed.isChecked()) {
                    employedSelf.setChecked(false);
                    employedSupervisorName.getEditText().setEnabled(true);
                    employedSupervisorContact.getEditText().setEnabled(true);
                }
            }
        });

        employedSelf.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (employedSelf.isChecked()) {
                    employed.setChecked(false);
                    employedSupervisorName.getEditText().setEnabled(false);
                    employedSupervisorContact.getEditText().setEnabled(false);
                }
            }
        });

        final ArrayAdapter<CharSequence> employedTypeAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.occupation_hours, android.R.layout.simple_spinner_item);
        employedTypeAdapter.setDropDownViewResource(R.layout.spinner_item);
//        employedTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        employedType.setAdapter(employedTypeAdapter);

        studying = (CheckBox) rootView.findViewById(R.id.application_income_studying);
        studyingWhere = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_institution);
        studyingWhat = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_course);
        studyingCourseEnd = (DatePicker) rootView.findViewById(R.id.application_income_studying_course_end);
        studyingAdminName = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_course_admin_name);
        studyingAdminContact = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_course_admin_contact);
        studyingIncome = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_income);
        studyingIncomeAmount = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_income_amount);

        studying.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                studyingWhere.getEditText().setText("");
                studyingWhat.getEditText().setText("");
                studyingAdminName.getEditText().setText("");
                studyingAdminContact.getEditText().setText("");
                studyingIncome.getEditText().setText("");
                studyingIncomeAmount.getEditText().setText("0");

                studyingWhere.getEditText().setEnabled(studying.isChecked());
                studyingWhat.getEditText().setEnabled(studying.isChecked());
                studyingCourseEnd.setEnabled(studying.isChecked());
                studyingAdminName.getEditText().setEnabled(studying.isChecked());
                studyingAdminContact.getEditText().setEnabled(studying.isChecked());
                studyingIncome.getEditText().setEnabled(studying.isChecked());
                studyingIncomeAmount.getEditText().setEnabled(studying.isChecked());
            }
        });

        winz = (CheckBox) rootView.findViewById(R.id.application_income_winz);
        winzAmount = (TextInputLayout) rootView.findViewById(R.id.application_income_winz_income_amount);
        winzCategory = (TextInputLayout) rootView.findViewById(R.id.application_income_winz_category);
        winzEnd = (DatePicker) rootView.findViewById(R.id.application_income_winz_end);
        winzRedirect = (CheckBox) rootView.findViewById(R.id.application_income_winz_redirect);
        winzDay = (Spinner) rootView.findViewById(R.id.application_income_winz_day);
        winzAccSupp = (TextInputLayout) rootView.findViewById(R.id.application_income_winz_accommodation_support);

        winz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                winzAmount.getEditText().setText("");
                winzCategory.getEditText().setText("");
                winzAccSupp.getEditText().setText("");

                winzAmount.getEditText().setEnabled(winz.isChecked());
                winzCategory.getEditText().setEnabled(winz.isChecked());
                winzEnd.setEnabled(winz.isChecked());
                winzRedirect.setEnabled(winz.isChecked());
                winzDay.setEnabled(winz.isChecked());
                winzAccSupp.getEditText().setEnabled(winz.isChecked());
            }
        });

        final ArrayAdapter<CharSequence> winzDayAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.day, android.R.layout.simple_spinner_item);
        winzDayAdapter.setDropDownViewResource(R.layout.spinner_item);
//        winzDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        winzDay.setAdapter(winzDayAdapter);

        bondSavings = (TextInputLayout) rootView.findViewById(R.id.application_income_bond_savings);
        bondWINZ = (TextInputLayout) rootView.findViewById(R.id.application_income_bond_winz);
        bondTransfer = (TextInputLayout) rootView.findViewById(R.id.application_income_bond_transfer);

        house = (CheckBox) rootView.findViewById(R.id.application_asset_house);
        houseAddress = (TextInputLayout) rootView.findViewById(R.id.application_asset_house_address);

        house.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                houseAddress.getEditText().setText("");
                houseAddress.getEditText().setEnabled(house.isChecked());
            }
        });

        vehicle = (CheckBox) rootView.findViewById(R.id.application_asset_vehicle);
        vehicleMake = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_make);
        vehicleModel = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_model);
        vehicleColour = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_colour);
        vehicleRego = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_rego);

        vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                vehicleMake.getEditText().setText("");
                vehicleMake.getEditText().setEnabled(vehicle.isChecked());
                vehicleModel.getEditText().setText("");
                vehicleModel.getEditText().setEnabled(vehicle.isChecked());
                vehicleColour.getEditText().setText("");
                vehicleColour.getEditText().setEnabled(vehicle.isChecked());
                vehicleRego.getEditText().setText("");
                vehicleRego.getEditText().setEnabled(vehicle.isChecked());
            }
        });

        referenceName = (TextInputLayout) rootView.findViewById(R.id.application_reference_name);
        referenceAddress = (TextInputLayout) rootView.findViewById(R.id.application_reference_address);
        referencePhoneA = (TextInputLayout) rootView.findViewById(R.id.application_reference_phone_a);
        referencePhoneB = (TextInputLayout) rootView.findViewById(R.id.application_reference_phone_b);

        referenceRelativeName = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_name);
        referenceRelativeAddress = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_address);
        referenceRelativePhoneA = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_phone_a);
        referenceRelativePhoneB = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_phone_b);

        showApplication();

        occupants.add(new Occupant("", "", -1, ""));
        occupantListViewAdapter = new OccupantListViewAdapter(getContext(), R.layout.occupant_list_view_item, occupants);
        newOccupants.setAdapter(occupantListViewAdapter);

        pets.add(new Pet("", "", "", -1, ""));
        petListViewAdapter = new PetListViewAdapter(getContext(), R.layout.pet_list_view_item, pets);
        newPets.setAdapter(petListViewAdapter);

        tenancyApplication = ((GlobalClass) getActivity().getApplicationContext()).tenancyApplication;

        changeApplicationPage();
        return rootView;
    }

    public void changeApplicationPage() {
        pageIndicator.setText(getString(R.string.display_page, page + 1, NUM_PAGES));
        String[] titles = getResources().getStringArray(R.array.application_titles);
        applicationTitle.setText(titles[page]);
        applicationFlipper.setDisplayedChild(page);
        scrollView.scrollTo(0, 0);
    }

    public boolean validate() {
        boolean valid;
        switch (page) {
            case 0:
//                valid = true;
                valid = validateCurrentAddress();
                break;
            case 1:
//                valid = true;
                valid = validatePreviousAddress();
                break;
            case 2:
//                valid = true;
                valid = validateNewAddress();
                break;
            case 3:
//                valid = true;
                valid = validateTribunal();
                break;
            case 4:
//                valid = true;
                valid = validateCrime();
                break;
            case 5:
//                valid = true;
                valid = validateEmployment();
                break;
            case 6:
//                valid = true;
                valid = validateStudying();
                break;
            case 7:
//                valid = true;
                valid = validateWINZ();
                break;
            case 8:
//                valid = true;
                valid = validateBond();
                break;
            case 11:
                valid = true;
                valid = validateReference();
                break;
            case 12:
                valid = true;
                valid = validateReferenceRelative();
                break;
            default:
                valid = true;
                break;
        }
        return valid;
    }

    public boolean validateCurrentAddress() {
        boolean valid = true;

        if (currentAddress.getEditText().getText().toString().isEmpty()) {
            currentAddress.setError(getString(R.string.application_current_address_empty));
            valid = false;
        } else {
            currentAddress.setError(null);
        }

        if (currentLeaveReason.getEditText().getText().toString().isEmpty()) {
            currentLeaveReason.setError(getString(R.string.application_current_address_leave_reason_empty));
            valid = false;
        } else {
            currentLeaveReason.setError(null);
        }

        if (currentLivingArrangements.getSelectedItemId() != 4) {
            if (currentLandlordName.getEditText().getText().toString().isEmpty()) {
                currentLandlordName.setError(getString(R.string.application_current_address_landlord_name_empty));
                valid = false;
            } else {
                currentLandlordName.setError(null);
            }
            if (currentLandlordPhone.getEditText().getText().toString().isEmpty()) {
                currentLandlordPhone.setError(getString(R.string.application_current_address_landlord_phone_empty));
                valid = false;
            } else {
                currentLandlordPhone.setError(null);
            }
        } else {
            currentLandlordPhone.setError(null);
            currentLandlordName.setError(null);
        }

        return valid;
    }

    public boolean validatePreviousAddress() {
        boolean valid = true;

        if (previousAddress.getEditText().getText().toString().isEmpty()) {
            previousAddress.setError(getString(R.string.application_current_address_empty));
            valid = false;
        } else {
            previousAddress.setError(null);
        }

        if (previousLeaveReason.getEditText().getText().toString().isEmpty()) {
            previousLeaveReason.setError(getString(R.string.application_current_address_leave_reason_empty));
            valid = false;
        } else {
            previousLeaveReason.setError(null);
        }

        if (previousLandlordName.getEditText().getText().toString().isEmpty()) {
            previousLandlordName.setError(getString(R.string.application_current_address_landlord_name_empty));
            valid = false;
        } else {
            previousLandlordName.setError(null);
        }
        if (previousLandlordPhone.getEditText().getText().toString().isEmpty()) {
            previousLandlordPhone.setError(getString(R.string.application_current_address_landlord_phone_empty));
            valid = false;
        } else {
            previousLandlordPhone.setError(null);
        }

        return valid;
    }

    public boolean validateNewAddress() {
        boolean valid = true;

        if (newRentalPeriod.getEditText().getText().toString().isEmpty()) {
            newRentalPeriod.setError(getString(R.string.application_new_address_rental_period_empty));
            valid = false;
        } else {
            newRentalPeriod.setError(null);
        }

        if (newBorders.isChecked()) {
            if (newBorderAmount.getEditText().getText().toString().isEmpty()) {
                newBorderAmount.setError(getString(R.string.application_new_address_borders_amount_empty));
                valid = false;
            } else {
                newBorderAmount.setError(null);
            }
        } else {
            newBorderAmount.setError(null);
        }

        if (newCommercial.isChecked()) {
            if (newCommercialDescription.getEditText().getText().toString().isEmpty()) {
                newCommercialDescription.setError(getString(R.string.application_new_address_commercial_description_empty));
                valid = false;
            } else {
                newCommercialDescription.setError(null);
            }
        } else {
            newCommercialDescription.setError(null);
        }


        return valid;
    }

    public boolean validateTribunal() {
        boolean valid = true;

        if (tribunal.isChecked()) {
            if (tribunalDescription.getEditText().getText().toString().isEmpty()) {
                tribunalDescription.setError(getString(R.string.application_tribunal_description_empty));
                valid = false;
            } else {
                tribunalDescription.setError(null);
            }
        } else {
            tribunalDescription.setError(null);
        }

        return valid;
    }

    public boolean validateCrime() {
        boolean valid = true;

        if (crime.isChecked()) {
            if (crimeDescription.getEditText().getText().toString().isEmpty()) {
                crimeDescription.setError(getString(R.string.application_tribunal_description_empty));
                valid = false;
            } else {
                crimeDescription.setError(null);
            }
        } else {
            crimeDescription.setError(null);
        }

        return valid;
    }

    public boolean validateEmployment() {
        boolean valid = true;

        if (employed.isChecked()) {
            if (employedLength.getEditText().getText().toString().isEmpty()) {
                employedLength.setError(getString(R.string.application_income_employment_occupation_length_empty));
                valid = false;
            } else {
                employedLength.setError(null);
            }

            if (employedName.getEditText().getText().toString().isEmpty()) {
                employedName.setError(getString(R.string.application_income_employment_employer_name_empty));
                valid = false;
            } else {
                employedName.setError(null);
            }

            if (employedAddress.getEditText().getText().toString().isEmpty()) {
                employedAddress.setError(getString(R.string.application_income_employment_employer_address_empty));
                valid = false;
            } else {
                employedAddress.setError(null);
            }

            if (employedSupervisorName.getEditText().getText().toString().isEmpty()) {
                employedSupervisorName.setError(getString(R.string.application_income_employment_supervisor_name_empty));
                valid = false;
            } else {
                employedSupervisorName.setError(null);
            }

            if (employedSupervisorContact.getEditText().getText().toString().isEmpty()) {
                employedSupervisorContact.setError(getString(R.string.application_income_employment_supervisor_phone_empty));
                valid = false;
            } else {
                employedSupervisorContact.setError(null);
            }
        } else {
            employedSupervisorContact.setError(null);
            employedSupervisorName.setError(null);
            employedAddress.setError(null);
            employedName.setError(null);
            employedLength.setError(null);
        }

        if (employedOccupation.getEditText().getText().toString().isEmpty()) {
            employedOccupation.setError(getString(R.string.application_income_employment_occupation_empty));
            valid = false;
        } else {
            employedOccupation.setError(null);
        }

        return valid;
    }

    public boolean validateStudying() {
        boolean valid = true;

        if (studying.isChecked()) {
            if (studyingWhat.getEditText().getText().toString().isEmpty()) {
                studyingWhat.setError(getString(R.string.application_income_studying_course_empty));
                valid = false;
            } else {
                studyingWhat.setError(null);
            }

            if (studyingWhere.getEditText().getText().toString().isEmpty()) {
                studyingWhere.setError(getString(R.string.application_income_studying_institution_empty));
                valid = false;
            } else {
                studyingWhere.setError(null);
            }
        } else {
            studyingWhere.setError(null);
            studyingWhat.setError(null);
        }


        return valid;
    }

    public boolean validateWINZ() {
        boolean valid = true;

        if (winz.isChecked()) {
            if (winzAmount.getEditText().getText().toString().isEmpty()) {
                winzAmount.setError(getString(R.string.application_income_winz_amount));
                valid = false;
            } else {
                winzAmount.setError(null);
            }

            if (winzAccSupp.getEditText().getText().toString().isEmpty()) {
                winzAccSupp.setError(getString(R.string.application_income_winz_accommodation_support_empty));
                valid = false;
            } else {
                winzAccSupp.setError(null);
            }
        } else {
            winzAmount.setError(null);
            winzAccSupp.setError(null);
        }

        return valid;
    }

    public boolean validateBond() {
        boolean valid = true;

        if (bondSavings.getEditText().getText().toString().isEmpty()) {
            bondSavings.setError(getString(R.string.application_income_bond_savings_empty));
            valid = false;
        } else {
            bondSavings.setError(null);
        }

        if (bondWINZ.getEditText().getText().toString().isEmpty()) {
            bondWINZ.setError(getString(R.string.application_income_bond_winz_empty));
            valid = false;
        } else {
            bondWINZ.setError(null);
        }

        if (bondTransfer.getEditText().getText().toString().isEmpty()) {
            bondTransfer.setError(getString(R.string.application_income_bond_transfer_empty));
            valid = false;
        } else {
            bondTransfer.setError(null);
        }

        return valid;
    }

    public boolean validateReference() {
        boolean valid = true;

        if (referenceName.getEditText().getText().toString().isEmpty()) {
            referenceName.setError(getString(R.string.application_reference_name_empty));
            valid = false;
        } else {
            referenceName.setError(null);
        }

        if (referenceAddress.getEditText().getText().toString().isEmpty()) {
            referenceAddress.setError(getString(R.string.application_reference_address_empty));
            valid = false;
        } else {
            referenceAddress.setError(null);
        }

        if (referencePhoneA.getEditText().getText().toString().isEmpty()) {
            referencePhoneA.setError(getString(R.string.application_reference_phone_a_empty));
            valid = false;
        } else {
            referencePhoneA.setError(null);
        }

        return valid;
    }

    public boolean validateReferenceRelative() {
        boolean valid = true;

        if (referenceRelativeName.getEditText().getText().toString().isEmpty()) {
            referenceRelativeName.setError(getString(R.string.application_reference_name_empty));
            valid = false;
        } else {
            referenceRelativeName.setError(null);
        }

        if (referenceRelativeAddress.getEditText().getText().toString().isEmpty()) {
            referenceRelativeAddress.setError(getString(R.string.application_reference_address_empty));
            valid = false;
        } else {
            referenceRelativeAddress.setError(null);
        }

        if (referenceRelativePhoneA.getEditText().getText().toString().isEmpty()) {
            referenceRelativePhoneA.setError(getString(R.string.application_reference_phone_a_empty));
            valid = false;
        } else {
            referenceRelativePhoneA.setError(null);
        }

        return valid;
    }

    public void updateOccupantListView(Occupant occupant) {
        occupants.remove(occupants.size() - 1);
        occupants.add(occupant);
        occupants.add(new Occupant("", "", -1, ""));

//        newOccupants.setAdapter(occupantListViewAdapter);
        occupantListViewAdapter.notifyDataSetChanged();
    }

    public void selectOccupant(int position) {
        final Occupant occupant = occupants.get(position);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_create_occupant, null, false);

        final TextInputLayout name = (TextInputLayout) rootView.findViewById(R.id.dialog_create_occupant_name);
        final TextInputLayout age = (TextInputLayout) rootView.findViewById(R.id.dialog_create_occupant_age);
        final TextInputLayout relationship = (TextInputLayout) rootView.findViewById(R.id.dialog_create_occupant_relationship);

        if (occupant.getAge() < 0) {

            age.getEditText().setText(String.format(getString(R.string.display_int), 0));

            alertDialog.setView(rootView);
            alertDialog.setPositiveButton("Create", null)
                    .setNegativeButton(android.R.string.cancel, null);

            alertDialog.setTitle("Add New Occupant");

            final AlertDialog alert = alertDialog.create();

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Boolean valid = true;
                            if (name.getEditText().getText().toString().isEmpty()) {
                                name.setError(getString(R.string.dialog_validate_occupant_name));
                                valid = false;
                            }
                            if (Integer.parseInt(age.getEditText().getText().toString()) < 0 ||
                                    Integer.parseInt(age.getEditText().getText().toString()) > 100) {
                                age.setError(getString(R.string.dialog_validate_occupant_age));
                                valid = false;
                            }
                            if (relationship.getEditText().getText().toString().isEmpty()) {
                                relationship.setError(getString(R.string.dialog_validate_occupant_relationship));
                                valid = false;
                            }


                            if (valid) {
                                Occupant o = new Occupant(
                                        UUID.randomUUID().toString(),
                                        name.getEditText().getText().toString(),
                                        Integer.parseInt(age.getEditText().getText().toString()),
                                        relationship.getEditText().getText().toString()
                                );

                                updateOccupantListView(o);
                                alert.dismiss();
                            }
                        }
                    });
                }
            });

            relationship.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        alert.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    }
                    return false;
                }
            });

            alert.show();
        } else {

            name.getEditText().setText(occupant.getName());
            age.getEditText().setText(String.format(getString(R.string.display_int), occupant.getAge()));
            relationship.getEditText().setText(occupant.getRelationship());

            alertDialog.setView(rootView);
            alertDialog.setPositiveButton("Edit", null)
                    .setNegativeButton(android.R.string.cancel, null);

            alertDialog.setTitle("Edit Occupant");

            final AlertDialog alert = alertDialog.create();

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Boolean valid = true;
                            if (name.getEditText().getText().toString().isEmpty()) {
                                name.setError(getString(R.string.dialog_validate_occupant_name));
                                valid = false;
                            }
                            if (Integer.parseInt(age.getEditText().getText().toString()) < 0 ||
                                    Integer.parseInt(age.getEditText().getText().toString()) > 100) {
                                age.setError(getString(R.string.dialog_validate_occupant_age));
                                valid = false;
                            }
                            if (relationship.getEditText().getText().toString().isEmpty()) {
                                relationship.setError(getString(R.string.dialog_validate_occupant_relationship));
                                valid = false;
                            }

                            if (valid) {
                                occupant.setName(name.getEditText().getText().toString());
                                occupant.setAge(Integer.parseInt(age.getEditText().getText().toString()));
                                occupant.setRelationship(relationship.getEditText().getText().toString());

                                newOccupants.setAdapter(occupantListViewAdapter);

                                alert.dismiss();
                            }
                        }
                    });
                }
            });

            relationship.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        alert.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    }
                    return false;
                }
            });
            alert.show();
        }
    }

    public void deleteOccupant(int position) {
        final Occupant occupant = occupants.get(position);
        if (occupant.getAge() >= 0) {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
            alertDialog.setMessage("Do you wish to delete this occupant?");
            alertDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    occupants.remove(occupant);
                    occupantListViewAdapter.notifyDataSetChanged();
                }
            });

            alertDialog.setNegativeButton(android.R.string.cancel, null);
            alertDialog.setTitle("Delete Occupant");
            alertDialog.show();
        }
    }

    public void updatePetListView(Pet pet) {
        pets.remove(pets.size() - 1);
        pets.add(pet);
        pets.add(new Pet("", "", "", -1, ""));

        newPets.setAdapter(petListViewAdapter);
    }

    public void selectPet(int position) {
        final Pet pet = pets.get(position);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_create_pet, null, false);

        final TextInputLayout type = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_type);
        final TextInputLayout age = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_age);
        final TextInputLayout breed = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_breed);
        final TextInputLayout description = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_description);

        if (pet.getAge() < 0) {

            age.getEditText().setText(String.format(getString(R.string.display_int), 0));

            alertDialog.setView(rootView);
            alertDialog.setPositiveButton("Create", null)
                    .setNegativeButton(android.R.string.cancel, null);

            alertDialog.setTitle("Add New Pet");

            final AlertDialog alert = alertDialog.create();

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Boolean valid = true;
                            if (type.getEditText().getText().toString().isEmpty()) {
                                type.setError(getString(R.string.dialog_validate_pet_type));
                                valid = false;
                            }
                            if (Integer.parseInt(age.getEditText().getText().toString()) < 0 ||
                                    Integer.parseInt(age.getEditText().getText().toString()) > 100) {
                                age.setError(getString(R.string.dialog_validate_pet_age));
                                valid = false;
                            }


                            if (valid) {
                                Pet p = new Pet(
                                        UUID.randomUUID().toString(),
                                        type.getEditText().getText().toString(),
                                        breed.getEditText().getText().toString(),
                                        Integer.parseInt(age.getEditText().getText().toString()),
                                        description.getEditText().getText().toString()
                                );

                                updatePetListView(p);
                                alert.dismiss();
                            }
                        }
                    });
                }
            });

            description.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        alert.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    }
                    return false;
                }
            });

            alert.show();
        } else {

            type.getEditText().setText(pet.getType());
            age.getEditText().setText(String.format(getString(R.string.display_int), pet.getAge()));
            description.getEditText().setText(pet.getDescription());
            breed.getEditText().setText(pet.getBreed());

            alertDialog.setView(rootView);
            alertDialog.setPositiveButton("Edit", null)
                    .setNegativeButton(android.R.string.cancel, null);

            alertDialog.setTitle("Edit Pet");

            final AlertDialog alert = alertDialog.create();

            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Boolean valid = true;
                            if (type.getEditText().getText().toString().isEmpty()) {
                                type.setError(getString(R.string.dialog_validate_pet_type));
                                valid = false;
                            }
                            if (Integer.parseInt(age.getEditText().getText().toString()) < 0 ||
                                    Integer.parseInt(age.getEditText().getText().toString()) > 100) {
                                age.setError(getString(R.string.dialog_validate_pet_age));
                                valid = false;
                            }

                            if (valid) {
                                pet.setType(type.getEditText().getText().toString());
                                pet.setAge(Integer.parseInt(age.getEditText().getText().toString()));
                                pet.setBreed(breed.getEditText().getText().toString());
                                pet.setDescription(description.getEditText().getText().toString());

                                newPets.setAdapter(petListViewAdapter);

                                alert.dismiss();
                            }
                        }
                    });
                }
            });

            description.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        alert.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    }
                    return false;
                }
            });
            alert.show();
        }
    }

    public void deletePet(int position) {
        final Pet pet = pets.get(position);
        if (pet.getAge() >= 0) {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
            alertDialog.setMessage("Do you wish to delete this pet?");
            alertDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pets.remove(pet);
                    newPets.setAdapter(petListViewAdapter);
                }
            });

            alertDialog.setNegativeButton(android.R.string.cancel, null);
            alertDialog.setTitle("Delete Pet");
            alertDialog.show();
        }
    }

    public void saveApplication() {

        //Current Address
        tenancyApplication.currentAddress.address = currentAddress.getEditText().getText().toString();
        tenancyApplication.currentAddress.livingArrangements = currentLivingArrangements.getSelectedItemPosition();
        tenancyApplication.currentAddress.leaveReason = currentLeaveReason.getEditText().getText().toString();
        tenancyApplication.currentAddress.length = currentLength.getEditText().getText().toString();
        tenancyApplication.currentAddress.contactName = currentLandlordName.getEditText().getText().toString();
        tenancyApplication.currentAddress.contactPhone = currentLandlordPhone.getEditText().getText().toString();
        //Previous Address
        tenancyApplication.previousAddress.address = previousAddress.getEditText().getText().toString();
        tenancyApplication.previousAddress.leaveReason = previousLeaveReason.getEditText().getText().toString();
        tenancyApplication.previousAddress.length = previousLength.getEditText().getText().toString();
        tenancyApplication.previousAddress.contactName = previousLandlordName.getEditText().getText().toString();
        tenancyApplication.previousAddress.contactPhone = previousLandlordPhone.getEditText().getText().toString();
        //New Address
        tenancyApplication.otherOccupants = new ArrayList<>();
        tenancyApplication.otherOccupants.addAll(occupants);
        tenancyApplication.otherOccupants.remove(occupants.size() - 1);

        tenancyApplication.intentRentalPeriod = newRentalPeriod.getEditText().getText().toString();
        tenancyApplication.intentBorders = newBorders.isChecked();
        tenancyApplication.intentBordersAmount = Integer.parseInt(newBorderAmount.getEditText().getText().toString());
        tenancyApplication.intentCommercial = newCommercial.isChecked();
        tenancyApplication.intentCommercialDescription = newCommercialDescription.getEditText().getText().toString();

        tenancyApplication.pets = new ArrayList<>();
        tenancyApplication.pets.addAll(pets);
        tenancyApplication.pets.remove(pets.size() - 1);
        //Tenancy Tribunal
        tenancyApplication.tenancyTribunal = tribunal.isChecked();
        tenancyApplication.tenancyTribunalReason = tribunalDescription.getEditText().getText().toString();
        //Criminal
        tenancyApplication.convicted = crime.isChecked();
        tenancyApplication.convictedReason = crimeDescription.getEditText().getText().toString();
        tenancyApplication.bailParole = crimeBail.isChecked();
        tenancyApplication.bailParoleBorder = crimeBailBorder.isChecked();
        tenancyApplication.monitoringDevice = crimeMonitoring.isChecked();
        tenancyApplication.monitoringDeviceBorder = crimeMonitoringBorder.isChecked();
        tenancyApplication.residenceRestrictions = crimeRestriction.isChecked();
        tenancyApplication.residenceRestrictionsBorder = crimeRestrictionBorder.isChecked();
        tenancyApplication.bankrupt = crimeBankrupt.isChecked();
        //Employment
        tenancyApplication.employed = employed.isChecked();
        tenancyApplication.selfEmployed = employedSelf.isChecked();
        tenancyApplication.occupation = employedOccupation.getEditText().getText().toString();
        tenancyApplication.occupationHours = employedType.getSelectedItemPosition();
        tenancyApplication.occupationTime = employedLength.getEditText().getText().toString();
        tenancyApplication.employerName = employedName.getEditText().getText().toString();
        tenancyApplication.employerAddress = employedAddress.getEditText().getText().toString();
        tenancyApplication.employerContactName = employedSupervisorName.getEditText().getText().toString();
        tenancyApplication.employerContactPhone = employedSupervisorContact.getEditText().getText().toString();
        //Studying
        tenancyApplication.student = studying.isChecked();
        tenancyApplication.studentInstitution = studyingWhere.getEditText().getText().toString();
        tenancyApplication.studentCourse = studyingWhat.getEditText().getText().toString();
        tenancyApplication.studentCourseEnd = DateHelper.getDateFromString(studyingCourseEnd.getYear() + " " + studyingCourseEnd.getMonth() + " " + studyingCourseEnd.getDayOfMonth());
        tenancyApplication.studentCourseAdminName = studyingAdminName.getEditText().getText().toString();
        tenancyApplication.studentCourseAdminContact = studyingAdminContact.getEditText().getText().toString();
        tenancyApplication.studentIncome = studyingIncome.getEditText().getText().toString();
        tenancyApplication.studentIncomeAmount = Double.parseDouble(studyingIncomeAmount.getEditText().getText().toString());
        //WINZ
        tenancyApplication.winz = winz.isChecked();
        tenancyApplication.winzAmount = Double.parseDouble(winzAmount.getEditText().getText().toString());
        tenancyApplication.winzCategory = currentAddress.getEditText().getText().toString();
        tenancyApplication.winzEnd = DateHelper.getDateFromString(winzEnd.getYear() + " " + winzEnd.getMonth() + " " + winzEnd.getDayOfMonth());
        tenancyApplication.winzRedirect = winzRedirect.isChecked();
        tenancyApplication.winzDay = winzDay.getSelectedItemPosition();
        tenancyApplication.winzAccSupp = Double.parseDouble(winzAccSupp.getEditText().getText().toString());
        //Advance and Bond
        tenancyApplication.advanceSavingsLoan = Double.parseDouble(bondSavings.getEditText().getText().toString());
        tenancyApplication.advanceWINZ = Double.parseDouble(bondWINZ.getEditText().getText().toString());
        tenancyApplication.advanceBondTransfer = Double.parseDouble(bondTransfer.getEditText().getText().toString());
        //House
        tenancyApplication.houseOwner = house.isChecked();
        tenancyApplication.houseAddress = houseAddress.getEditText().getText().toString();
        //Vehicle
        tenancyApplication.vehicleOwner = vehicle.isChecked();
        tenancyApplication.vehicleMake = vehicleMake.getEditText().getText().toString();
        tenancyApplication.vehicleModel = vehicleModel.getEditText().getText().toString();
        tenancyApplication.vehicleColour = vehicleColour.getEditText().getText().toString();
        tenancyApplication.vehicleRego = vehicleRego.getEditText().getText().toString();
        //Reference
        tenancyApplication.reference.name = referenceName.getEditText().getText().toString();
        tenancyApplication.reference.address = referenceAddress.getEditText().getText().toString();
        tenancyApplication.reference.phoneA = referencePhoneA.getEditText().getText().toString();
        tenancyApplication.reference.phoneB = referencePhoneB.getEditText().getText().toString();
        //Relative
        tenancyApplication.reference.name = referenceRelativeName.getEditText().getText().toString();
        tenancyApplication.reference.address = referenceRelativeAddress.getEditText().getText().toString();
        tenancyApplication.reference.phoneA = referenceRelativePhoneA.getEditText().getText().toString();
        tenancyApplication.reference.phoneB = referenceRelativePhoneB.getEditText().getText().toString();

        PrefHelper.saveTenancyApplication(((GlobalClass) getActivity().getApplicationContext()).userName, tenancyApplication, getContext());
        ApplicationHelper.updateApplication(tenancyApplication, getContext(), this);

    }

    public void showApplication() {

        //Current Address
        currentAddress.getEditText().setText(tenancyApplication.currentAddress.address);
        currentLivingArrangements.setSelection(tenancyApplication.currentAddress.livingArrangements);
        currentLeaveReason.getEditText().setText(tenancyApplication.currentAddress.leaveReason);
        currentLength.getEditText().setText(tenancyApplication.currentAddress.length);
        currentLandlordName.getEditText().setText(tenancyApplication.currentAddress.contactName);
        currentLandlordPhone.getEditText().setText(tenancyApplication.currentAddress.contactPhone);
        //Previous Address
        previousAddress.getEditText().setText(tenancyApplication.previousAddress.address);
        previousLeaveReason.getEditText().setText(tenancyApplication.previousAddress.leaveReason);
        previousLength.getEditText().setText(tenancyApplication.previousAddress.length);
        previousLandlordName.getEditText().setText(tenancyApplication.previousAddress.contactName);
        previousLandlordPhone.getEditText().setText(tenancyApplication.previousAddress.contactPhone);
        //New Address
        occupants = tenancyApplication.otherOccupants;
        newRentalPeriod.getEditText().setText(tenancyApplication.intentRentalPeriod);
        newBorders.setChecked(tenancyApplication.intentBorders);
        newBorderAmount.getEditText().setText(getString(R.string.display_int, tenancyApplication.intentBordersAmount));
        newCommercial.setChecked(tenancyApplication.intentCommercial);
        newCommercialDescription.getEditText().setText(tenancyApplication.intentCommercialDescription);
        pets = tenancyApplication.pets;
        //Tenancy Tribunal
        tribunal.setChecked(tenancyApplication.tenancyTribunal);
        tribunalDescription.getEditText().setText(tenancyApplication.tenancyTribunalReason);
        //Criminal
        crime.setChecked(tenancyApplication.convicted);
        crimeDescription.getEditText().setText(tenancyApplication.convictedReason);
        crimeBail.setChecked(tenancyApplication.bailParole);
        crimeBailBorder.setChecked(tenancyApplication.bailParoleBorder);
        crimeMonitoring.setChecked(tenancyApplication.monitoringDevice);
        crimeMonitoringBorder.setChecked(tenancyApplication.monitoringDeviceBorder);
        crimeRestriction.setChecked(tenancyApplication.residenceRestrictions);
        crimeRestrictionBorder.setChecked(tenancyApplication.residenceRestrictionsBorder);
        crimeBankrupt.setChecked(tenancyApplication.bankrupt);
        //Employment
        employed.setChecked(tenancyApplication.employed);
        employedSelf.setChecked(tenancyApplication.selfEmployed);
        employedOccupation.getEditText().setText(tenancyApplication.occupation);
        employedType.setSelection(tenancyApplication.occupationHours);
        employedLength.getEditText().setText(tenancyApplication.occupationTime);
        employedName.getEditText().setText(tenancyApplication.employerName);
        employedAddress.getEditText().setText(tenancyApplication.employerAddress);
        employedSupervisorName.getEditText().setText(tenancyApplication.employerContactName);
        employedSupervisorContact.getEditText().setText(tenancyApplication.employerContactPhone);
        //Studying
        studying.setChecked(tenancyApplication.student);
        studyingWhere.getEditText().setText(tenancyApplication.studentInstitution);
        studyingWhat.getEditText().setText(tenancyApplication.studentCourse);
        studyingAdminName.getEditText().setText(tenancyApplication.studentCourseAdminName);
        studyingAdminContact.getEditText().setText(tenancyApplication.studentCourseAdminContact);
        studyingIncome.getEditText().setText(tenancyApplication.studentIncome);
        studyingIncomeAmount.getEditText().setText(getString(R.string.display_double, tenancyApplication.studentIncomeAmount));
        //WINZ
        winz.setChecked(tenancyApplication.winz);
        winzAmount.getEditText().setText(getString(R.string.display_double, tenancyApplication.winzAmount));
        currentAddress.getEditText().setText(tenancyApplication.winzCategory);
        winzRedirect.setChecked(tenancyApplication.winzRedirect);
        winzDay.setSelection(tenancyApplication.winzDay);
        winzAccSupp.getEditText().setText(getString(R.string.display_double, tenancyApplication.winzAccSupp));
        //Advance and Bond
        bondSavings.getEditText().setText(getString(R.string.display_double, tenancyApplication.advanceSavingsLoan));
        bondWINZ.getEditText().setText(getString(R.string.display_double, tenancyApplication.advanceWINZ));
        bondTransfer.getEditText().setText(getString(R.string.display_double, tenancyApplication.advanceBondTransfer));
        //House
        house.setChecked(tenancyApplication.houseOwner);
        houseAddress.getEditText().setText(tenancyApplication.houseAddress);
        //Vehicle
        vehicle.setChecked(tenancyApplication.vehicleOwner);
        vehicleMake.getEditText().setText(tenancyApplication.vehicleMake);
        vehicleModel.getEditText().setText(tenancyApplication.vehicleModel);
        vehicleColour.getEditText().setText(tenancyApplication.vehicleColour);
        vehicleRego.getEditText().setText(tenancyApplication.vehicleRego);
        //Reference
        referenceName.getEditText().setText(tenancyApplication.reference.name);
        referenceAddress.getEditText().setText(tenancyApplication.reference.address);
        referencePhoneA.getEditText().setText(tenancyApplication.reference.phoneA);
        referencePhoneB.getEditText().setText(tenancyApplication.reference.phoneB);
        //Relative
        referenceRelativeName.getEditText().setText(tenancyApplication.reference.name);
        referenceRelativeAddress.getEditText().setText(tenancyApplication.reference.address);
        referenceRelativePhoneA.getEditText().setText(tenancyApplication.reference.phoneA);
        referenceRelativePhoneB.getEditText().setText(tenancyApplication.reference.phoneB);

    }

    @Override
    public void applicationFailed(String errorMessage) {

    }

    @Override
    public void applicationSuccess(List<TenancyApplication> applicationList) {

    }

    @Override
    public void applicationUpdateSuccess() {
        if (tenancyApplication.state == 1) {
            getFragmentManager().popBackStack();
        }
    }


}
