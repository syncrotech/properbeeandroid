package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.ResidenceTenantAdaptor;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.TenantsHelper;
import com.syncrotech.properbeeandroid.models.ResidenceTenantModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenantTenant extends Fragment implements TenantsHelper.GetTenantsResponseInterface{

    ListView tenantListView;
    TextView noTenants;

    View tenantView, loadingView;
    BroadcastReceiver dataSyncedReciever;


    public TenantTenant() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_tenant_tenant, container, false);

        loadingView = rootView.findViewById(R.id.loading_panel);
        tenantView = rootView.findViewById(R.id.tenant_tenant_view);
        noTenants = (TextView) rootView.findViewById(R.id.noTenantsText);
        tenantListView = (ListView) rootView.findViewById(R.id.tenantsList);

        tenantView.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);


        setUpReciever();
        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataSyncedReciever, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataSyncedReciever);
    }

    private void setUpView() {
        TenantsHelper.getTenantsForResidence(getContext(), this);
    }


    @Override
    public void tenantsSuccess(List<ResidenceTenantModel> data) {
        if (data.isEmpty()) {
            loadingView.setVisibility(View.GONE);
            tenantView.setVisibility(View.VISIBLE);
            noTenants.setVisibility(View.VISIBLE);
        } else {
            ResidenceTenantAdaptor adaptor = new ResidenceTenantAdaptor(getContext(), R.layout.list_item_residence_tenant, data);
            tenantListView.setAdapter(adaptor);

            tenantListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    ResidenceTenantModel model = (ResidenceTenantModel) adapterView.getItemAtPosition(i);
                }
            });

            loadingView.setVisibility(View.GONE);
            tenantView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void tenantsFailed() {

    }
}
