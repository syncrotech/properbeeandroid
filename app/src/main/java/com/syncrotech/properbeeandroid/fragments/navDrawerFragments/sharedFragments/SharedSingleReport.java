package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.sharedFragments;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ReportHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Report;

public class SharedSingleReport extends Fragment implements ReportHelper.GetReportWithIdInterface, ReportHelper.PostReportInterface{

    public String reportId;

    View display, loading;
    TextView message, state;
    ImageView photo;
    Button resolve;



    public SharedSingleReport() {
        // Required empty public constructor
    }

    public static Fragment newInstance(String reportId) {
        SharedSingleReport fragment = new SharedSingleReport();
        fragment.reportId = reportId;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shared_single_report, container, false);

        display = rootView.findViewById(R.id.report_display);
        loading = rootView.findViewById(R.id.loading_panel);

        message = (TextView) rootView.findViewById(R.id.message);
        state = (TextView) rootView.findViewById(R.id.state);

        photo = (ImageView) rootView.findViewById(R.id.photo_display);

        resolve = (Button) rootView.findViewById(R.id.resolved);
        resolve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                alertDialog.setMessage("Do you wish to mark this report as resolved?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ReportHelper.postReportResolved(getContext(), SharedSingleReport.this, reportId);
                    }
                });

                alertDialog.setNegativeButton(android.R.string.no, null);
                alertDialog.setTitle("Resolve Report");
                alertDialog.show();
            }
        });

        loading.setVisibility(View.VISIBLE);
        display.setVisibility(View.GONE);

        ReportHelper.getReportWithId(getContext(), this, reportId);

        return rootView;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scale = ((float) newWidth) / width;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scale, scale);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }


    @Override
    public void getReportWithIdSuccess(Report report) {
        message.setText(report.message);
        photo.setImageBitmap(getResizedBitmap(report.photo, 400));
        state.setText(report.resolved? "Resolved":"Unresolved");
        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);
    }

    @Override
    public void getReportWithIdFailed() {

    }

    @Override
    public void postReportSuccess() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void postReportFailed() {

    }
}
