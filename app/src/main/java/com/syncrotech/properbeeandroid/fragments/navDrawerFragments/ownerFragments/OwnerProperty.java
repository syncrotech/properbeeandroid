package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.activities.OwnerActivity;
import com.syncrotech.properbeeandroid.adapters.OwnersPropertyListViewAdapter;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ResidenceHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

import java.util.List;

public class OwnerProperty extends Fragment implements ResidenceHelper.ResidenceInterface{

    View residencesV, loadingV, noPropertyDescription;
    ListView propertyList;
    FloatingActionButton fab;
    BroadcastReceiver dataSyncedReciever;

    public OwnerProperty() {
        // Required empty public constructor
    }

    public static Fragment newInstance(){
        Fragment fragment = new OwnerProperty();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_nav_owner_property, container, false);

        residencesV = rootView.findViewById(R.id.owner_residences);
        loadingV = rootView.findViewById(R.id.loading_panel);
        noPropertyDescription = rootView.findViewById(R.id.noPropertyText);
        residencesV.setVisibility(View.GONE);
        loadingV.setVisibility(View.VISIBLE);

        rootView.findViewById(R.id.help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.alertDialogStyle);
                builder.setMessage(R.string.residences_description)
                        .setPositiveButton("OK", null);
                builder.create().show();
            }
        });

        propertyList = (ListView)rootView.findViewById(R.id.propertyList);
        fab = (FloatingActionButton)rootView.findViewById(R.id.addProperty);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = null;

                try{
                    fragment = AddResidence.newInstance();
                } catch (Exception e){
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).addToBackStack("Add Residence").commit();
            }
        });

        setUpReciever();

        if(((GlobalClass)getActivity().getApplicationContext()).dataSynced)
            setUpView();


        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataSyncedReciever, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataSyncedReciever);
    }

    private void setUpView(){
        ResidenceHelper.getResidences(getContext(), this);
    }

    @Override
    public void residenceFailed(String errorMessage) {
        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void residenceSuccessNoResidence() {
        residencesV.setVisibility(View.VISIBLE);
        loadingV.setVisibility(View.GONE);
        propertyList.setVisibility(View.GONE);
        noPropertyDescription.setVisibility(View.VISIBLE);
    }

    @Override
    public void residenceSuccess(List<Residence> residences) {
        residencesV.setVisibility(View.VISIBLE);
        loadingV.setVisibility(View.GONE);

        OwnersPropertyListViewAdapter adapter = new OwnersPropertyListViewAdapter(getContext(), R.layout.list_item_owner_property, residences);
        propertyList.setAdapter(adapter);

        propertyList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Residence residence = (Residence) propertyList.getItemAtPosition(i);
                ((GlobalClass)getActivity().getApplicationContext()).selectedResidence = residence;
                PrefHelper.saveDefaultResidence(getActivity(), residence);

                // Go back to the home screen
                ((OwnerActivity)getActivity()).enableButtons(true);
                ((OwnerActivity)getActivity()).setPropertyTitle(residence.address);
                Fragment fragment = OwnerHome.newInstance();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).commit();
            }
        });

        propertyList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Fragment fragment = EditResidence.newInstance((Residence)propertyList.getItemAtPosition(i));
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).addToBackStack("Edit Residence").commit();
                return true;
            }
        });
    }
}
