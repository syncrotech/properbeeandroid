package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.syncrotech.properbeeandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OwnerAccountEdit extends Fragment {


    public OwnerAccountEdit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_owner_account_edit, container, false);


        return rootView;
    }

}
