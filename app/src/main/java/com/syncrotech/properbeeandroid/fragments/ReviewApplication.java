package com.syncrotech.properbeeandroid.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.OccupantListViewAdapter;
import com.syncrotech.properbeeandroid.adapters.PetListViewAdapter;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.OwnerTenant;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ApplicationHelper;
import com.syncrotech.properbeeandroid.models.Occupant;
import com.syncrotech.properbeeandroid.models.Pet;
import com.syncrotech.properbeeandroid.models.TenancyApplication;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewApplication extends Fragment implements ApplicationHelper.ApplicationResponseInterface{
    private static final int NUM_PAGES = 13;

    TenancyApplication tenancyApplication;

    View loadingView, applicationView;
    Button prev, next;
    ViewFlipper applicationFlipper;
    Integer page;
    TextView pageIndicator, applicationTitle;
    TextInputLayout currentAddress, currentLength, currentLeaveReason, currentLandlordName, currentLandlordPhone,
            previousAddress, previousLength, previousLeaveReason, previousLandlordName, previousLandlordPhone,
            newRentalPeriod, newBorderAmount, newCommercialDescription,
            tribunalDescription,
            crimeDescription,
            employedOccupation, employedLength, employedName, employedAddress, employedSupervisorName, employedSupervisorContact,
            studyingWhere, studyingWhat, studyingAdminName, studyingAdminContact, studyingIncome, studyingIncomeAmount,
            winzAmount, winzCategory, winzAccSupp,
            bondSavings, bondWINZ, bondTransfer,
            houseAddress,
            vehicleMake, vehicleModel, vehicleColour, vehicleRego,
            referenceName, referenceAddress, referencePhoneA, referencePhoneB,
            referenceRelativeName, referenceRelativeAddress, referenceRelativePhoneA, referenceRelativePhoneB;
    Spinner currentLivingArrangements,
            employedType,
            winzDay;
    DatePicker studyingCourseEnd,
            winzEnd;
    ListView newOccupants, newPets;
    CheckBox newBorders, newCommercial,
            tribunal,
            crime, crimeBail, crimeBailBorder, crimeMonitoring, crimeMonitoringBorder, crimeRestriction, crimeRestrictionBorder, crimeBankrupt,
            employed, employedSelf,
            studying,
            winz, winzRedirect,
            house,
            vehicle;
    OccupantListViewAdapter occupantListViewAdapter;
    PetListViewAdapter petListViewAdapter;
    ArrayList<Occupant> occupants;
    ArrayList<Pet> pets;


    public ReviewApplication() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_application, container, false);



        tenancyApplication = ((GlobalClass) getActivity().getApplicationContext()).tenancyApplication;

        applicationView = rootView.findViewById(R.id.application_view);
        loadingView = rootView.findViewById(R.id.loading_panel);

        applicationFlipper = (ViewFlipper) rootView.findViewById(R.id.application_view_flipper);

        prev = (Button) rootView.findViewById(R.id.application_prev_page);
        next = (Button) rootView.findViewById(R.id.application_next_page);

        page = 0;

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page > 0) {
                    if (page == 1)
                        prev.setEnabled(false);
                    else if (!next.isEnabled())
                        next.setEnabled(true);
                    page--;
                    changeApplicationPage();
                }
            }
        });
        prev.setEnabled(false);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page < NUM_PAGES - 1) {
                    {
                        if (!prev.isEnabled())
                            prev.setEnabled(true);
                        else if (page == NUM_PAGES - 2)
                            next.setText("Submit");
                        page++;
                        changeApplicationPage();
                    }
                } else {
                    chooseResponse();
                }
            }
        });

        pageIndicator = (TextView) rootView.findViewById(R.id.view_page_page_number);
        applicationTitle = (TextView) rootView.findViewById(R.id.application_title);


        currentAddress = (TextInputLayout) rootView.findViewById(R.id.application_current_address);
        currentLivingArrangements = (Spinner) rootView.findViewById(R.id.application_current_address_living_arrangement);
        currentLength = (TextInputLayout) rootView.findViewById(R.id.application_current_address_length);
        currentLeaveReason = (TextInputLayout) rootView.findViewById(R.id.application_current_address_leave_reason);
        currentLandlordName = (TextInputLayout) rootView.findViewById(R.id.application_current_address_landlord_name);
        currentLandlordPhone = (TextInputLayout) rootView.findViewById(R.id.application_current_address_landlord_phone);

        currentAddress.getEditText().setEnabled(false);
        currentLivingArrangements.setEnabled(false);
        currentLength.getEditText().setEnabled(false);
        currentLandlordName.getEditText().setEnabled(false);
        currentLeaveReason.getEditText().setEnabled(false);
        currentLandlordPhone.getEditText().setEnabled(false);

        final ArrayAdapter<CharSequence> livingArangementsAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.living_arrangements, android.R.layout.simple_spinner_item);
        livingArangementsAdapter.setDropDownViewResource(R.layout.spinner_item);
        currentLivingArrangements.setAdapter(livingArangementsAdapter);

        previousAddress = (TextInputLayout) rootView.findViewById(R.id.application_previous_address);
        previousLength = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_length);
        previousLeaveReason = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_leave_reason);
        previousLandlordName = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_landlord_name);
        previousLandlordPhone = (TextInputLayout) rootView.findViewById(R.id.application_previous_address_landlord_phone);

        previousAddress.getEditText().setEnabled(false);
        previousLength.getEditText().setEnabled(false);
        previousLeaveReason.getEditText().setEnabled(false);
        previousLandlordName.getEditText().setEnabled(false);
        previousLandlordPhone.getEditText().setEnabled(false);


        newOccupants = (ListView) rootView.findViewById(R.id.application_new_address_occupants);
        newPets = (ListView) rootView.findViewById(R.id.application_new_address_pets);
        newRentalPeriod = (TextInputLayout) rootView.findViewById(R.id.application_new_address_rental_period);
        newBorders = (CheckBox) rootView.findViewById(R.id.application_new_address_borders);
        newBorderAmount = (TextInputLayout) rootView.findViewById(R.id.application_new_address_border_amount);
        newCommercial = (CheckBox) rootView.findViewById(R.id.application_new_address_commercial);
        newCommercialDescription = (TextInputLayout) rootView.findViewById(R.id.application_new_address_commercial_description);

        newRentalPeriod.getEditText().setEnabled(false);
        newBorderAmount.getEditText().setEnabled(false);
        newCommercialDescription.getEditText().setEnabled(false);
        newBorders.setEnabled(false);
        newCommercial.setEnabled(false);

        occupants = new ArrayList<>();

        newOccupants.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectOccupant(position);
            }
        });

        pets = new ArrayList<>();

        newPets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectPet(position);
            }
        });

        tribunal = (CheckBox) rootView.findViewById(R.id.application_tribunal);
        tribunalDescription = (TextInputLayout) rootView.findViewById(R.id.application_tribunal_description);

        tribunal.setEnabled(false);
        tribunalDescription.getEditText().setEnabled(false);

        crime = (CheckBox) rootView.findViewById(R.id.application_criminal_conviction);
        crimeDescription = (TextInputLayout) rootView.findViewById(R.id.application_criminal_conviction_reason);
        crimeBail = (CheckBox) rootView.findViewById(R.id.application_criminal_bail);
        crimeBailBorder = (CheckBox) rootView.findViewById(R.id.application_criminal_bail_border);
        crimeMonitoring = (CheckBox) rootView.findViewById(R.id.application_criminal_monitoring);
        crimeMonitoringBorder = (CheckBox) rootView.findViewById(R.id.application_criminal_monitoring_border);
        crimeRestriction = (CheckBox) rootView.findViewById(R.id.application_criminal_restrictions);
        crimeRestrictionBorder = (CheckBox) rootView.findViewById(R.id.application_criminal_restrictions_border);
        crimeBankrupt = (CheckBox) rootView.findViewById(R.id.application_criminal_bankrupt);

        crimeDescription.getEditText().setEnabled(false);
        crime.setEnabled(false);
        crimeBail.setEnabled(false);
        crimeBailBorder.setEnabled(false);
        crimeMonitoring.setEnabled(false);
        crimeMonitoringBorder.setEnabled(false);
        crimeRestriction.setEnabled(false);
        crimeRestrictionBorder.setEnabled(false);
        crimeBankrupt.setEnabled(false);

        employed = (CheckBox) rootView.findViewById(R.id.application_income_employment_employed);
        employedSelf = (CheckBox) rootView.findViewById(R.id.application_income_employment_self_employed);
        employedOccupation = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_occupation);
        employedType = (Spinner) rootView.findViewById(R.id.application_income_employment_occupation_hours);
        employedLength = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_occupation_length);
        employedName = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_employer_name);
        employedAddress = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_employer_address);
        employedSupervisorName = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_supervisor_name);
        employedSupervisorContact = (TextInputLayout) rootView.findViewById(R.id.application_income_employment_supervisor_contact);

        employed.setEnabled(false);
        employedSelf.setEnabled(false);
        employedOccupation.getEditText().setEnabled(false);
        employedType.setFocusable(false);
        employedName.getEditText().setEnabled(false);
        employedLength.getEditText().setEnabled(false);
        employedAddress.getEditText().setEnabled(false);
        employedSupervisorName.getEditText().setEnabled(false);
        employedSupervisorContact.getEditText().setEnabled(false);

        final ArrayAdapter<CharSequence> employedTypeAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.occupation_hours, android.R.layout.simple_spinner_item);
        employedTypeAdapter.setDropDownViewResource(R.layout.spinner_item);
        employedType.setAdapter(employedTypeAdapter);

        studying = (CheckBox) rootView.findViewById(R.id.application_income_studying);
        studyingWhere = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_institution);
        studyingWhat = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_course);
        studyingCourseEnd = (DatePicker) rootView.findViewById(R.id.application_income_studying_course_end);
        studyingAdminName = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_course_admin_name);
        studyingAdminContact = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_course_admin_contact);
        studyingIncome = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_income);
        studyingIncomeAmount = (TextInputLayout) rootView.findViewById(R.id.application_income_studying_income_amount);

        studying.setEnabled(false);
        studyingWhere.getEditText().setEnabled(false);
        studyingWhat.getEditText().setEnabled(false);
        studyingCourseEnd.setEnabled(false);
        studyingAdminName.getEditText().setEnabled(false);
        studyingAdminContact.getEditText().setEnabled(false);
        studyingIncome.getEditText().setEnabled(false);
        studyingIncomeAmount.getEditText().setEnabled(false);

        winz = (CheckBox) rootView.findViewById(R.id.application_income_winz);
        winzAmount = (TextInputLayout) rootView.findViewById(R.id.application_income_winz_income_amount);
        winzCategory = (TextInputLayout) rootView.findViewById(R.id.application_income_winz_category);
        winzEnd = (DatePicker) rootView.findViewById(R.id.application_income_winz_end);
        winzRedirect = (CheckBox) rootView.findViewById(R.id.application_income_winz_redirect);
        winzDay = (Spinner) rootView.findViewById(R.id.application_income_winz_day);
        winzAccSupp = (TextInputLayout) rootView.findViewById(R.id.application_income_winz_accommodation_support);

        winz.setEnabled(false);
        winzRedirect.setEnabled(false);
        winzAmount.getEditText().setEnabled(false);
        winzCategory.getEditText().setEnabled(false);
        winzEnd.setEnabled(false);
        winzDay.setEnabled(false);
        winzAccSupp.getEditText().setEnabled(false);

        final ArrayAdapter<CharSequence> winzDayAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.day, android.R.layout.simple_spinner_item);
        winzDayAdapter.setDropDownViewResource(R.layout.spinner_item);
        winzDay.setAdapter(winzDayAdapter);

        bondSavings = (TextInputLayout) rootView.findViewById(R.id.application_income_bond_savings);
        bondWINZ = (TextInputLayout) rootView.findViewById(R.id.application_income_bond_winz);
        bondTransfer = (TextInputLayout) rootView.findViewById(R.id.application_income_bond_transfer);

        bondSavings.getEditText().setEnabled(false);
        bondWINZ.getEditText().setEnabled(false);
        bondTransfer.getEditText().setEnabled(false);

        house = (CheckBox) rootView.findViewById(R.id.application_asset_house);
        houseAddress = (TextInputLayout) rootView.findViewById(R.id.application_asset_house_address);

        house.setEnabled(false);
        houseAddress.getEditText().setEnabled(false);

        vehicle = (CheckBox) rootView.findViewById(R.id.application_asset_vehicle);
        vehicleMake = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_make);
        vehicleModel = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_model);
        vehicleColour = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_colour);
        vehicleRego = (TextInputLayout) rootView.findViewById(R.id.application_asset_vehicle_rego);

        vehicle.setEnabled(false);
        vehicleMake.getEditText().setEnabled(false);
        vehicleModel.getEditText().setEnabled(false);
        vehicleColour.getEditText().setEnabled(false);
        vehicleRego.getEditText().setEnabled(false);

        referenceName = (TextInputLayout) rootView.findViewById(R.id.application_reference_name);
        referenceAddress = (TextInputLayout) rootView.findViewById(R.id.application_reference_address);
        referencePhoneA = (TextInputLayout) rootView.findViewById(R.id.application_reference_phone_a);
        referencePhoneB = (TextInputLayout) rootView.findViewById(R.id.application_reference_phone_b);

        referenceName.getEditText().setEnabled(false);
        referenceAddress.getEditText().setEnabled(false);
        referencePhoneA.getEditText().setEnabled(false);
        referencePhoneB.getEditText().setEnabled(false);

        referenceRelativeName = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_name);
        referenceRelativeAddress = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_address);
        referenceRelativePhoneA = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_phone_a);
        referenceRelativePhoneB = (TextInputLayout) rootView.findViewById(R.id.application_reference_relative_phone_b);

        referenceRelativeName.getEditText().setEnabled(false);
        referenceRelativeAddress.getEditText().setEnabled(false);
        referenceRelativePhoneA.getEditText().setEnabled(false);
        referenceRelativePhoneB.getEditText().setEnabled(false);



        showApplication();

        occupantListViewAdapter = new OccupantListViewAdapter(getContext(), R.layout.occupant_list_view_item, occupants);
        newOccupants.setAdapter(occupantListViewAdapter);

        petListViewAdapter = new PetListViewAdapter(getContext(), R.layout.pet_list_view_item, pets);
        newPets.setAdapter(petListViewAdapter);

        changeApplicationPage();
        return rootView;
    }


    public void changeApplicationPage() {
        pageIndicator.setText(getString(R.string.display_page, page + 1, NUM_PAGES));
        String[] titles = getResources().getStringArray(R.array.application_titles);
        applicationTitle.setText(titles[page]);
        applicationFlipper.setDisplayedChild(page);
    }


    public void selectOccupant(int position) {
        final Occupant occupant = occupants.get(position);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_create_occupant, null, false);

        final TextInputLayout name = (TextInputLayout) rootView.findViewById(R.id.dialog_create_occupant_name);
        final TextInputLayout age = (TextInputLayout) rootView.findViewById(R.id.dialog_create_occupant_age);
        final TextInputLayout relationship = (TextInputLayout) rootView.findViewById(R.id.dialog_create_occupant_relationship);

        name.getEditText().setFocusable(false);
        age.getEditText().setFocusable(false);
        relationship.getEditText().setFocusable(false);

        name.getEditText().setText(occupant.getName());
        age.getEditText().setText(String.format(getString(R.string.display_int), occupant.getAge()));
        relationship.getEditText().setText(occupant.getRelationship());

        alertDialog.setView(rootView);
        alertDialog.setPositiveButton("Done", null)
                .setNegativeButton(android.R.string.cancel, null);

        alertDialog.setTitle("Occupant");

        final AlertDialog alert = alertDialog.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
            }
        });

        relationship.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    alert.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                }
                return false;
            }
        });
        alert.show();
    }

    public void selectPet(int position) {
        final Pet pet = pets.get(position);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_create_pet, null, false);

        final TextInputLayout type = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_type);
        final TextInputLayout age = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_age);
        final TextInputLayout breed = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_breed);
        final TextInputLayout description = (TextInputLayout) rootView.findViewById(R.id.dialog_create_pet_description);

        type.getEditText().setFocusable(false);
        age.getEditText().setFocusable(false);
        breed.getEditText().setFocusable(false);
        description.getEditText().setFocusable(false);

        type.getEditText().setText(pet.getType());
        age.getEditText().setText(String.format(getString(R.string.display_int), pet.getAge()));
        description.getEditText().setText(pet.getDescription());
        breed.getEditText().setText(pet.getBreed());

        alertDialog.setView(rootView);
        alertDialog.setPositiveButton("Edit", null)
                .setNegativeButton(android.R.string.cancel, null);

        alertDialog.setTitle("Edit Pet");

        final AlertDialog alert = alertDialog.create();

        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
            }
        });

        description.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    alert.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                }
                return false;
            }
        });
        alert.show();
    }


    public void showApplication() {

        //Current Address
        currentAddress.getEditText().setText(tenancyApplication.currentAddress.address);
        currentLivingArrangements.setSelection(tenancyApplication.currentAddress.livingArrangements);
        currentLeaveReason.getEditText().setText(tenancyApplication.currentAddress.leaveReason);
        currentLength.getEditText().setText(tenancyApplication.currentAddress.length);
        currentLandlordName.getEditText().setText(tenancyApplication.currentAddress.contactName);
        currentLandlordPhone.getEditText().setText(tenancyApplication.currentAddress.contactPhone);
        //Previous Address
        previousAddress.getEditText().setText(tenancyApplication.previousAddress.address);
        previousLeaveReason.getEditText().setText(tenancyApplication.previousAddress.leaveReason);
        previousLength.getEditText().setText(tenancyApplication.previousAddress.length);
        previousLandlordName.getEditText().setText(tenancyApplication.previousAddress.contactName);
        previousLandlordPhone.getEditText().setText(tenancyApplication.previousAddress.contactPhone);
        //New Address
        occupants = tenancyApplication.otherOccupants;
        newRentalPeriod.getEditText().setText(tenancyApplication.intentRentalPeriod);
        newBorders.setChecked(tenancyApplication.intentBorders);
        newBorderAmount.getEditText().setText(getString(R.string.display_int, tenancyApplication.intentBordersAmount));
        newCommercial.setChecked(tenancyApplication.intentCommercial);
        newCommercialDescription.getEditText().setText(tenancyApplication.intentCommercialDescription);
        pets = tenancyApplication.pets;
        //Tenancy Tribunal
        tribunal.setChecked(tenancyApplication.tenancyTribunal);
        tribunalDescription.getEditText().setText(tenancyApplication.tenancyTribunalReason);
        //Criminal
        crime.setChecked(tenancyApplication.convicted);
        crimeDescription.getEditText().setText(tenancyApplication.convictedReason);
        crimeBail.setChecked(tenancyApplication.bailParole);
        crimeBailBorder.setChecked(tenancyApplication.bailParoleBorder);
        crimeMonitoring.setChecked(tenancyApplication.monitoringDevice);
        crimeMonitoringBorder.setChecked(tenancyApplication.monitoringDeviceBorder);
        crimeRestriction.setChecked(tenancyApplication.residenceRestrictions);
        crimeRestrictionBorder.setChecked(tenancyApplication.residenceRestrictionsBorder);
        crimeBankrupt.setChecked(tenancyApplication.bankrupt);
        //Employment
        employed.setChecked(tenancyApplication.employed);
        employedSelf.setChecked(tenancyApplication.selfEmployed);
        employedOccupation.getEditText().setText(tenancyApplication.occupation);
        employedType.setSelection(tenancyApplication.occupationHours);
        employedLength.getEditText().setText(tenancyApplication.occupationTime);
        employedName.getEditText().setText(tenancyApplication.employerName);
        employedAddress.getEditText().setText(tenancyApplication.employerAddress);
        employedSupervisorName.getEditText().setText(tenancyApplication.employerContactName);
        employedSupervisorContact.getEditText().setText(tenancyApplication.employerContactPhone);
        //Studying
        studying.setChecked(tenancyApplication.student);
        studyingWhere.getEditText().setText(tenancyApplication.studentInstitution);
        studyingWhat.getEditText().setText(tenancyApplication.studentCourse);
        studyingAdminName.getEditText().setText(tenancyApplication.studentCourseAdminName);
        studyingAdminContact.getEditText().setText(tenancyApplication.studentCourseAdminContact);
        studyingIncome.getEditText().setText(tenancyApplication.studentIncome);
        studyingIncomeAmount.getEditText().setText(getString(R.string.display_double, tenancyApplication.studentIncomeAmount));
        //WINZ
        winz.setChecked(tenancyApplication.winz);
        winzAmount.getEditText().setText(getString(R.string.display_double, tenancyApplication.winzAmount));
        currentAddress.getEditText().setText(tenancyApplication.winzCategory);
        winzRedirect.setChecked(tenancyApplication.winzRedirect);
        winzDay.setSelection(tenancyApplication.winzDay);
        winzAccSupp.getEditText().setText(getString(R.string.display_double, tenancyApplication.winzAccSupp));
        //Advance and Bond
        bondSavings.getEditText().setText(getString(R.string.display_double, tenancyApplication.advanceSavingsLoan));
        bondWINZ.getEditText().setText(getString(R.string.display_double, tenancyApplication.advanceWINZ));
        bondTransfer.getEditText().setText(getString(R.string.display_double, tenancyApplication.advanceBondTransfer));
        //House
        house.setChecked(tenancyApplication.houseOwner);
        houseAddress.getEditText().setText(tenancyApplication.houseAddress);
        //Vehicle
        vehicle.setChecked(tenancyApplication.vehicleOwner);
        vehicleMake.getEditText().setText(tenancyApplication.vehicleMake);
        vehicleModel.getEditText().setText(tenancyApplication.vehicleModel);
        vehicleColour.getEditText().setText(tenancyApplication.vehicleColour);
        vehicleRego.getEditText().setText(tenancyApplication.vehicleRego);
        //Reference
        referenceName.getEditText().setText(tenancyApplication.reference.name);
        referenceAddress.getEditText().setText(tenancyApplication.reference.address);
        referencePhoneA.getEditText().setText(tenancyApplication.reference.phoneA);
        referencePhoneB.getEditText().setText(tenancyApplication.reference.phoneB);
        //Relative
        referenceRelativeName.getEditText().setText(tenancyApplication.reference.name);
        referenceRelativeAddress.getEditText().setText(tenancyApplication.reference.address);
        referenceRelativePhoneA.getEditText().setText(tenancyApplication.reference.phoneA);
        referenceRelativePhoneB.getEditText().setText(tenancyApplication.reference.phoneB);

    }

    public void chooseResponse(){

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_application_response, null, false);

        final Spinner response = (Spinner) rootView.findViewById(R.id.dialog_response);

        final ArrayAdapter<CharSequence> responseAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.responses, android.R.layout.simple_spinner_item);
        responseAdapter.setDropDownViewResource(R.layout.spinner_item);
        response.setAdapter(responseAdapter);

        alertDialog.setView(rootView)
                .setTitle("Response")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int responseNum = -1;
                        if(response.getSelectedItemPosition() == 0) {
                            responseNum = 3;
                        }else if (response.getSelectedItemPosition() == 1){
                            responseNum = 2;
                        }
                        else if(response.getSelectedItemPosition() == 2){
                            //TODO change to correct num
                            responseNum = 5;
                        }

                        ApplicationHelper.respondToApllication(responseNum, tenancyApplication.id, getContext(), ReviewApplication.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create()
                .show();
    }


    @Override
    public void applicationResponseFailed(String errorMessage) {
        Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void applicationResponseSuccess() {

        GlobalClass gc = (GlobalClass) getActivity().getApplicationContext();
        gc.tenancyApplication = null;

        Fragment fragment = null;

        try{
            fragment = new OwnerTenant();
        } catch (Exception e){
            e.printStackTrace();
        }

        getFragmentManager().popBackStack();

    }
}
