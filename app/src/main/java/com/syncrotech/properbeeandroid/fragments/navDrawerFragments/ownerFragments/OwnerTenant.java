package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.ResidenceTenantAdaptor;
import com.syncrotech.properbeeandroid.adapters.SearchTenantsAdaptor;
import com.syncrotech.properbeeandroid.fragments.ReviewApplication;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ApplicationHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.TenantsHelper;
import com.syncrotech.properbeeandroid.models.ResidenceTenantModel;
import com.syncrotech.properbeeandroid.models.SearchTenantModel;
import com.syncrotech.properbeeandroid.models.TenancyApplication;

import java.util.ArrayList;
import java.util.List;

public class OwnerTenant extends Fragment implements TenantsHelper.GetTenantsResponseInterface, TenantsHelper.PostTenantsResponseInterface, TenantsHelper.SearchTenantsInterface, ApplicationHelper.ApplicationInterface {

    View listView, loading, noTenantsDescription, help;
    BroadcastReceiver dataSyncedReciever;
    FloatingActionButton fab;
    ListView tenantsList, searchTenantsList;
    TextView title;
    ProgressBar searchLoader;

    String selectedTenantId;

    public OwnerTenant() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        Fragment fragment = new OwnerTenant();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_nav_owner_tenant, container, false);

        listView = rootView.findViewById(R.id.owner_tenant_view);
        loading = rootView.findViewById(R.id.loading_panel);
        noTenantsDescription = rootView.findViewById(R.id.noTenantsText);
        tenantsList = (ListView) rootView.findViewById(R.id.tenantsList);
        title = (TextView) rootView.findViewById(R.id.title);
        help = rootView.findViewById(R.id.help);

        listView.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

        title.setText(String.format("Tenants for %s", ((GlobalClass) getActivity().getApplicationContext()).selectedResidence.address));

        setUpReciever();

        fab = (FloatingActionButton) rootView.findViewById(R.id.addTenant);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_add_tenant, null);
                final EditText email = (EditText) dialogView.findViewById(R.id.tenantSearch);
                final Button searchButton = (Button) dialogView.findViewById(R.id.search_button);
                searchTenantsList = (ListView) dialogView.findViewById(R.id.tenantsList);
                searchLoader = (ProgressBar) dialogView.findViewById(R.id.tenantLoader);
                builder.setView(dialogView);

                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        searchLoader.setVisibility(View.VISIBLE);
                        searchTenantsList.setVisibility(View.GONE);
                        TenantsHelper.searchForTenants(getContext(), OwnerTenant.this, email.getText().toString());
                    }
                });

                builder.setTitle("Add new tenant");
                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (selectedTenantId != null && !selectedTenantId.isEmpty()) {
                            TenantsHelper.addTenantToProperty(getContext(), OwnerTenant.this, selectedTenantId);
                            loading.setVisibility(View.VISIBLE);
                            listView.setVisibility(View.GONE);
                        }
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.create().show();
            }
        });

        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(dataSyncedReciever, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(dataSyncedReciever);
    }

    private void setUpView() {
        TenantsHelper.getTenantsForResidence(getContext(), this);
    }

    @Override
    public void tenantsSuccess(List<ResidenceTenantModel> data) {
        if (data.isEmpty()) {
            loading.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            noTenantsDescription.setVisibility(View.VISIBLE);
        } else {

            data = organizeList(data);
            ResidenceTenantAdaptor adaptor = new ResidenceTenantAdaptor(getContext(), R.layout.list_item_residence_tenant, data);
            tenantsList.setAdapter(adaptor);

            tenantsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    ResidenceTenantModel model = (ResidenceTenantModel) adapterView.getItemAtPosition(i);
                    ApplicationHelper.getApplication(model.applicationId, OwnerTenant.this, getContext());
                    loading.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                }
            });

            loading.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            noTenantsDescription.setVisibility(View.GONE);
        }
    }

    private List<ResidenceTenantModel> organizeList(List<ResidenceTenantModel> data) {

        List<ResidenceTenantModel> tenants = new ArrayList<>();
        List<ResidenceTenantModel> applicaitons = new ArrayList<>();

        for (ResidenceTenantModel model : data) {
            if (model.state == ResidenceTenantModel.ApplicationStatus.Accpeted)
                tenants.add(model);
            else applicaitons.add(model);
        }

        List<ResidenceTenantModel> sortedList = new ArrayList<>();

        if (tenants.size() > 0) {
            ResidenceTenantModel title = new ResidenceTenantModel();
            title.titleRow = true;
            title.titleText = "Tenants";
            sortedList.add(title);
        }
        sortedList.addAll(tenants);

        if (applicaitons.size() > 0) {
            ResidenceTenantModel title = new ResidenceTenantModel();
            title.titleRow = true;
            title.titleText = "Applications";
            sortedList.add(title);
        }
        sortedList.addAll(applicaitons);

        return sortedList;
    }

    @Override
    public void tenantsFailed() {
        Toast.makeText(getContext(), "Could not get tenants", Toast.LENGTH_LONG).show();
    }

    @Override
    public void tenantsPostedSuccess() {
        TenantsHelper.getTenantsForResidence(getContext(), this);
    }

    @Override
    public void tenantsPostedFailed() {
        Toast.makeText(getContext(), "Could not post tenant", Toast.LENGTH_LONG).show();
    }

    @Override
    public void searchTenantsSuccess(final List<SearchTenantModel> tenants) {
        final SearchTenantsAdaptor adaptor = new SearchTenantsAdaptor(getContext(), R.layout.list_item_search_tenant, tenants);
        searchTenantsList.setAdapter(adaptor);

        searchTenantsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SearchTenantModel model = (SearchTenantModel) searchTenantsList.getItemAtPosition(i);
                selectedTenantId = model.id;
                for (SearchTenantModel item : tenants) {
                    item.selected = item.id.equals(selectedTenantId);
                }
                ((SearchTenantsAdaptor) searchTenantsList.getAdapter()).notifyDataSetChanged();
            }
        });

        searchLoader.setVisibility(View.GONE);
        searchTenantsList.setVisibility(View.VISIBLE);

    }

    @Override
    public void searchTenantsFailed() {
        Toast.makeText(getContext(), "Could not get for search", Toast.LENGTH_LONG).show();
    }

    @Override
    public void applicationFailed(String errorMessage) {
        Toast.makeText(getContext(), "Could not start application", Toast.LENGTH_LONG).show();
    }

    @Override
    public void applicationSuccess(List<TenancyApplication> applicationList) {

        noTenantsDescription.setVisibility(View.GONE);

        GlobalClass gc = (GlobalClass) getActivity().getApplicationContext();
        gc.tenancyApplication = applicationList.get(0);

        Fragment fragment = null;

        try {
            fragment = new ReviewApplication();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).addToBackStack("Owner Tenant").commit();

    }

    @Override
    public void applicationUpdateSuccess() {
        //Strangely lonely here
    }
}
