package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.syncrotech.properbeeandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenantAccountEdit extends Fragment {


    public TenantAccountEdit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_tenant_account_edit, container, false);


        return rootView;
    }

}
