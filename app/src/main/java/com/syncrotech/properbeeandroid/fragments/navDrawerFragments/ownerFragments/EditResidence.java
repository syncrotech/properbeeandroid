package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ResidenceHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

public class EditResidence extends Fragment implements ResidenceHelper.PostResidenceInterface {

    Spinner propertyType, parking, pets;
    EditText address, rentalPrice, bedrooms, bathrooms, tenants, details;
    Button done;
    Residence residence;

    public EditResidence() {
    }

    public static Fragment newInstance(Residence residence) {
        EditResidence fragment = new EditResidence();
        fragment.residence = residence;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_residence, container, false);
        Resources res = getResources();
        final String[] propertyTypes = res.getStringArray(R.array.residence_type);
        String[] parkingTypes = res.getStringArray(R.array.parking);
        String[] petTypes = res.getStringArray(R.array.pets);

        propertyType = (Spinner) rootView.findViewById(R.id.add_residence_type);
        parking = (Spinner) rootView.findViewById(R.id.add_residence_parking);
        pets = (Spinner) rootView.findViewById(R.id.add_residence_pets);
        address = (EditText) rootView.findViewById(R.id.add_residence_address);
        rentalPrice = (EditText) rootView.findViewById(R.id.add_residence_rental_price);
        bedrooms = (EditText) rootView.findViewById(R.id.add_residence_bedrooms);
        bathrooms = (EditText) rootView.findViewById(R.id.add_residence_bathrooms);
        tenants = (EditText) rootView.findViewById(R.id.add_residence_max_tenants);
        details = (EditText) rootView.findViewById(R.id.add_residence_details);
        done = (Button) rootView.findViewById(R.id.add_residence_done);

        if (residence == null) {
            Toast.makeText(getActivity(), "Could not find residence, was class instantiated with new instance?", Toast.LENGTH_LONG).show();
            getActivity().onBackPressed();
        }

        propertyType.setSelection(residence.residenceType);
        parking.setSelection(residence.parking);
        pets.setSelection(residence.pets);
        address.setText(residence.address);
        rentalPrice.setText(String.valueOf(residence.rentalPrice));
        bedrooms.setText(String.valueOf(residence.bedrooms));
        bathrooms.setText(String.valueOf(residence.bathrooms));
        tenants.setText(String.valueOf(residence.maxTenants));
        details.setText(residence.details);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                residence.residenceType = propertyType.getSelectedItemPosition();
                residence.address = address.getText().toString();
                residence.rentalPrice = Double.parseDouble(rentalPrice.getText().toString());
                residence.bedrooms = Integer.parseInt(bedrooms.getText().toString());
                residence.maxTenants = Integer.parseInt(tenants.getText().toString());
                residence.bathrooms = Integer.parseInt(bathrooms.getText().toString());
                residence.parking = parking.getSelectedItemPosition();
                residence.pets = pets.getSelectedItemPosition();
                residence.details = details.getText().toString();
                ResidenceHelper.updateResidence(getContext(), EditResidence.this, residence);
            }
        });

        ArrayAdapter<String> typeAdaptor = new ArrayAdapter<>(getContext(), R.layout.spinner_selected_item, propertyTypes);
        typeAdaptor.setDropDownViewResource(R.layout.spinner_item);
        propertyType.setAdapter(typeAdaptor);

        ArrayAdapter<String> parkingAdaptor = new ArrayAdapter<>(getContext(), R.layout.spinner_selected_item, parkingTypes);
        parkingAdaptor.setDropDownViewResource(R.layout.spinner_item);
        parking.setAdapter(parkingAdaptor);

        ArrayAdapter<String> petAdaptor = new ArrayAdapter<>(getContext(), R.layout.spinner_selected_item, petTypes);
        petAdaptor.setDropDownViewResource(R.layout.spinner_item);
        pets.setAdapter(petAdaptor);

        return rootView;
    }

    @Override
    public void postFailed(String message) {
        Fragment fragment = OwnerProperty.newInstance();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).commit();
    }

    @Override
    public void postSuccess() {
        Fragment fragment = OwnerProperty.newInstance();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.owner_content, fragment).commit();
    }


}
