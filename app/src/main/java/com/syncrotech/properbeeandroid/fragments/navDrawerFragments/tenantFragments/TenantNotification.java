package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.activities.TennantActivity;
import com.syncrotech.properbeeandroid.adapters.NotificationListViewAdapter;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.NotificationHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Notification;
import com.syncrotech.properbeeandroid.services.CustomFirebaseMessageService;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenantNotification extends Fragment implements NotificationHelper.NotificationInterface, NotificationHelper.DismissNotificationInterface{

    View notificationView, loadingView;
    ListView notificationListView;
    BroadcastReceiver dataSyncedReciever, pushReceived;


    public TenantNotification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_tenant_notification, container, false);

        notificationView = rootView.findViewById(R.id.tenant_notification_view);
        loadingView = rootView.findViewById(R.id.loading_panel);
        notificationListView = (ListView) rootView.findViewById(R.id.tenant_notification_notifications);


        setUpReciever();
        if (((GlobalClass) getActivity().getApplicationContext()).dataSynced) {
            setUpView();
        }
        return rootView;
    }

    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.registerReceiver(dataSyncedReciever, filter);

        pushReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    NotificationHelper.getNotifications(getContext(), TenantNotification.this);
                }
            }
        };

        filter = new IntentFilter();
        filter.addAction(CustomFirebaseMessageService.MESSAGE_RECEIVED);
        manager.registerReceiver(pushReceived, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.unregisterReceiver(dataSyncedReciever);
        manager.unregisterReceiver(pushReceived);
    }

    private void setUpView(){
        loadingView.setVisibility(View.VISIBLE);
        notificationView.setVisibility(View.GONE);
        NotificationHelper.getNotifications(getContext(), this);
    }

    @Override
    public void notificationSuccess(final List<Notification> notificationList) {
        notificationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Notification notification = notificationList.get(i);
                if (notification != null) {
                    Fragment fragment = notification.getActionArea(getContext());

                    TennantActivity activity = (TennantActivity) getActivity();
                    activity.setTitle(notification.getTitle());

                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("Tenant Notification").commit();
                }
            }
        });

        notificationListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Notification notification = notificationList.get(i);
                if (notification != null) {
                    NotificationHelper.dismissNotification(notification.id, getContext(), TenantNotification.this);
                    NotificationListViewAdapter adapter = ((NotificationListViewAdapter)notificationListView.getAdapter());
                    List<Notification> data = adapter.getData();
                    data.remove(notification);
                    adapter.notifyDataSetChanged();

                }
                return false;
            }
        });


        NotificationListViewAdapter recentActivityAdapter = new NotificationListViewAdapter(getContext(), R.layout.list_item_image_text, notificationList);
        notificationListView.setAdapter(recentActivityAdapter);


        notificationView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);

    }

    @Override
    public void notificationFailed(String errorMessage) {

    }

    @Override
    public void dismissNotificationFailed(String errorMessage) {

    }
}
