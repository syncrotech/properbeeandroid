package com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.activities.GlobalClass;
import com.syncrotech.properbeeandroid.adapters.ConversationParticipantsAdaptor;
import com.syncrotech.properbeeandroid.adapters.ConversationsListAdapter;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.sharedFragments.SharedSingleConversation;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ConversationsHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Conversation;
import com.syncrotech.properbeeandroid.models.dbModels.Owner;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;
import com.syncrotech.properbeeandroid.models.dbModels.Tenant;
import com.syncrotech.properbeeandroid.models.viewModels.ConversationParticipantViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.PostConversationViewModel;
import com.syncrotech.properbeeandroid.services.CustomFirebaseMessageService;

import java.util.ArrayList;
import java.util.List;

public class TenantMessage extends Fragment implements ConversationsHelper.getConversationsInterface, ConversationsHelper.postConversationInterface {

    ListView conversationListView;
    AlertDialog dialog;
    BroadcastReceiver dataSyncedReciever, pushReceived;
    View loading, display;

    public TenantMessage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_nav_shared_message, container, false);

        conversationListView = (ListView) rootView.findViewById(R.id.conversationsListView);
        final FloatingActionButton fab = (FloatingActionButton)rootView.findViewById(R.id.addConversation);

        display = rootView.findViewById(R.id.display_view);
        loading = rootView.findViewById(R.id.loading_panel);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.alertDialogStyle);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_add_conversation, null);
                builder.setView(dialogView);

                final Residence residence = ((GlobalClass)getActivity().getApplicationContext()).selectedResidence;

                List<ConversationParticipantViewModel> participants = new ArrayList<>();

                for(Tenant tenant : residence.tenants){
                    ConversationParticipantViewModel model = new ConversationParticipantViewModel();
                    model.id = tenant.id;
                    model.selected = false;
                    model.name = tenant.firstName + " " + tenant.lastName;
                    participants.add(model);
                }

                for (Owner owner : residence.owners){
                    ConversationParticipantViewModel model = new ConversationParticipantViewModel();
                    model.id = owner.id;
                    model.selected = false;
                    model.name = owner.firstName + " " + owner.lastName;
                    participants.add(model);
                }

                final ListView participantsListView = (ListView) dialogView.findViewById(R.id.conversationParticipantsListView);

                ConversationParticipantsAdaptor adaptor = new ConversationParticipantsAdaptor(getContext(), R.layout.list_item_conversation_participant, participants);
                participantsListView.setAdapter(adaptor);

                builder.setTitle("Add new conversation")
                        .setPositiveButton("Add", null)
                        .setNegativeButton("Cancel", null);

                dialog =  builder.create();

                dialog.show();

                final Button positive = dialog.getButton(DialogInterface.BUTTON_POSITIVE);

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String conversationTitle = ((EditText)dialogView.findViewById(R.id.conversationTitleInput)).getText().toString();
                        if (conversationTitle.isEmpty()){
                            ((TextInputLayout)dialogView.findViewById(R.id.conversationTitleLayout)).setError("You must enter a name for the conversation.");
                            return;
                        }
                        List<ConversationParticipantViewModel> participants = ((ConversationParticipantsAdaptor)participantsListView.getAdapter()).getData();
                        PostConversationViewModel model = new PostConversationViewModel();
                        model.name = conversationTitle;
                        model.participants = new ArrayList<>();
                        for(ConversationParticipantViewModel participant : participants){
                            if(participant.selected) model.participants.add(participant.id);
                        }
                        model.residenceId = residence.id;
                        ConversationsHelper.startNewConversation(getContext(), TenantMessage.this, model);
                        positive.setEnabled(false);
                    }
                });
            }
        });


        setUpReciever();

        if(((GlobalClass)getActivity().getApplicationContext()).dataSynced)
            setUpView();


        return rootView;
    }



    private void setUpReciever() {
        IntentFilter filter = new IntentFilter(GlobalClass.DATA_SYNCED_FILTER);

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());

        dataSyncedReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setUpView();
            }
        };

        manager.registerReceiver(dataSyncedReciever, filter);

        pushReceived = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    if(extras.get("ActionArea") == CustomFirebaseMessageService.ActionAreas.Conversation)
                        ConversationsHelper.getConversations(getContext(), TenantMessage.this);
                }
            }
        };

        filter = new IntentFilter();
        filter.addAction(CustomFirebaseMessageService.MESSAGE_RECEIVED);

        manager.registerReceiver(pushReceived, filter);
    }

    private void setUpView(){
        loading.setVisibility(View.VISIBLE);
        display.setVisibility(View.GONE);

        ConversationsHelper.getConversations(getContext(), this);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.unregisterReceiver(dataSyncedReciever);
        manager.unregisterReceiver(pushReceived);
    }

    @Override
    public void afterGetConversations(List<Conversation> conversations) {

        final ConversationsListAdapter adapter = new ConversationsListAdapter(getContext(), R.layout.message_list_item, conversations);
        conversationListView.setAdapter(adapter);

        conversationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Conversation conversation = (Conversation)conversationListView.getItemAtPosition(i);

                Fragment fragment = SharedSingleConversation.newInstance(conversation.id);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("messages").commit();
            }
        });

        loading.setVisibility(View.GONE);
        display.setVisibility(View.VISIBLE);
    }

    @Override
    public void getConversationsFailed() {
        Toast.makeText(getContext(), "Could not load conversations", Toast.LENGTH_LONG).show();
    }

    @Override
    public void afterPostConversation(String converstaionId) {
        dialog.dismiss();
        Fragment fragment = SharedSingleConversation.newInstance(converstaionId);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).addToBackStack("messages").commit();
    }

    @Override
    public void postConversationFailed() {
        Toast.makeText(getContext(), "Could not create conversation", Toast.LENGTH_LONG).show();
    }
}
