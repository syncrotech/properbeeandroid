package com.syncrotech.properbeeandroid.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class CustomFirebaseMessageService extends FirebaseMessagingService {
    public static String MESSAGE_RECEIVED = "MessageReceived";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Send a broadcast saying we have received a message
        Log.e("Notification", "notification received");
        Intent i = new Intent(MESSAGE_RECEIVED);
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            i.putExtra("MessageBody", remoteMessage.getNotification().getBody());
        }
        if (remoteMessage.getData() != null) {
            Map<String, String> data = remoteMessage.getData();
            i.putExtra("ActionArea", ActionAreas.valueOf(data.get("ActionArea")));
            String actionId = data.get("ActionId");
            if(actionId != null && !actionId.isEmpty())
                i.putExtra("ActionId",  actionId);
        }
        // Send the broadcast
        LocalBroadcastManager.getInstance(CustomFirebaseMessageService.this).sendBroadcast(i);
    }

    public enum ActionAreas {Inspection, Application, Report, Conversation, Message}
}
