package com.syncrotech.properbeeandroid.services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.syncrotech.properbeeandroid.helpers.PushNotificationHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.FcmTokenHelper;

public class CustomFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        SharedPreferences.Editor editor = getSharedPreferences(PushNotificationHelper.FCM_PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putBoolean("registrationPending", true);
        editor.apply();

        String token = FirebaseInstanceId.getInstance().getToken();
        if (token != null && !token.isEmpty())
            FcmTokenHelper.UpdateToken(token, getApplicationContext());

        super.onTokenRefresh();

    }
}
