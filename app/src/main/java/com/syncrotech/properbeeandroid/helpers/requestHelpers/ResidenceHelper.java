package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ResidenceHelper {
    public static final String TAG = "ResidenceHelper";

    public static void getResidences(final Context context, final ResidenceInterface callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .addHeader("Authorization", "Bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Residences")
                            .get()
                            .build();
                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        if (PrefHelper.getTokenDetails(context).getUserRole().equals("Landlord")) {
                            JSONArray residences = new JSONArray(response.body().string());
                            final List<Residence> residenceList = new ArrayList<Residence>();
                            for (int i = 0; i < residences.length(); i++) {
                                residenceList.add(new Residence(residences.getJSONObject(i)));
                            }
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.residenceSuccess(residenceList);
                                }
                            });
                        } else {
                            String body = response.body().string();
                            if (body.equals("null")) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.residenceSuccessNoResidence();
                                    }
                                });
                            } else {
                                JSONObject residence = new JSONObject(body);
                                final List<Residence> residenceList = new ArrayList<Residence>();
                                residenceList.add(new Residence(residence));
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.residenceSuccess(residenceList);
                                    }
                                });
                            }
                        }
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Could not get residences (" + response.code() + ") - " + response.message());
                                callback.residenceFailed(response.message());
                            }
                        });
                    }

                } catch (final IOException e) {
                    Log.e(TAG, "IO Exception while getting residences " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.residenceFailed(e.getMessage());
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not unwrap json (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.residenceFailed("Server error");
                        }
                    });
                }

            }
        }).start();
    }

    public static void getResidence(final Context context, final ResidenceInterface callback, final String residenceId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .addHeader("Authorization", "Bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Residences/" + residenceId)
                            .get()
                            .build();
                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        if (PrefHelper.getTokenDetails(context).getUserRole().equals("Landlord")) {
                            JSONObject residence = new JSONObject(response.body().string());
                            final ArrayList<Residence> residences = new ArrayList<>();
                            residences.add(new Residence(residence));
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.residenceSuccess(residences);
                                }
                            });
                        } else {
                            String body = response.body().string();
                            if (body.equals("null")) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.residenceSuccessNoResidence();
                                    }
                                });
                            } else {
                                JSONObject residence = new JSONObject(body);
                                final List<Residence> residenceList = new ArrayList<Residence>();
                                residenceList.add(new Residence(residence));
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        callback.residenceSuccess(residenceList);
                                    }
                                });
                            }
                        }
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Could not get residences (" + response.code() + ") - " + response.message());
                                callback.residenceFailed(response.message());
                            }
                        });
                    }

                } catch (final IOException e) {
                    Log.e(TAG, "IO Exception while getting residences " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.residenceFailed(e.getMessage());
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not unwrap json (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.residenceFailed("Server error");
                        }
                    });
                }

            }
        }).start();
    }

    public static void postResidence(final Context context, final PostResidenceInterface callback, final Residence residence) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody formBody = RequestBody.create(mediaType, residence.getJSON().toString());

                    Request request = new Request.Builder()
                            .addHeader("DeviceID", PrefHelper.getDeviceId(context))
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Residences")
                            .post(formBody)
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postSuccess();
                            }
                        });
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "Error wrapping json (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postFailed("Sever error");
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, "IO error (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postFailed("No internet");
                        }
                    });
                }
            }
        }).start();
    }

    public static void updateResidence(final Context context, final PostResidenceInterface callback, final Residence residence) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody formBody = RequestBody.create(mediaType, residence.getJSON().put("Id", residence.id).toString());

                    Request request = new Request.Builder()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Residences/Edit")
                            .post(formBody)
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postSuccess();
                            }
                        });
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "Error wrapping json (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postFailed("Sever error");
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, "IO error (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postFailed("No internet");
                        }
                    });
                }
            }
        }).start();
    }

    public interface ResidenceInterface {
        void residenceFailed(String errorMessage);

        void residenceSuccessNoResidence();

        void residenceSuccess(List<Residence> residences);
    }

    public interface PostResidenceInterface {
        void postFailed(String message);

        void postSuccess();
    }

}
