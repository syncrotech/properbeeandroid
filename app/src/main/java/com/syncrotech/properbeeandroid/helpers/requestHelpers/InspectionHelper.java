package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Inspection;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionNoteWithId;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionPartialViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionPhotoWithId;
import com.syncrotech.properbeeandroid.models.viewModels.InspectionStateIdViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.PostNewInspectionViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class InspectionHelper {
    private static final String TAG = "InspectionHelper";

    public static void getInspectionsForResidence(final Context context, final GetInspectionsForResidenceInterface callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(baseUrl + "Inspections/" + PrefHelper.getDefaultResidenceId(context))
                            .get()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        JSONArray array = new JSONArray(response.body().string());
                        final List<InspectionPartialViewModel> inspections = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            inspections.add(new InspectionPartialViewModel(array.getJSONObject(i)));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getInspectionsForResidenceSuccess(inspections);
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getInspectionsForResidenceFailed();
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Could not get inspections - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getInspectionsForResidenceFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getInspectionsForResidenceFailed();
                        }
                    });
                }
            }
        }).start();

    }

    public static void sendNewInspection(final Context context, final NewInspectionInterface callback, final PostNewInspectionViewModel model){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.getJSONObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Inspections")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.afterPostNewInspection();
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postInspectionFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not post inspection - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postInspectionFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postInspectionFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void sendPhotoViewModel(final Context context, final SendPhotoInterface callback, final InspectionPhotoWithId model){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.toJSONObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Inspections/AddPhotoToInspection")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.sendPhotoSuccess();
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.sendPhotoFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not post photo - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.sendPhotoFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.sendPhotoFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void sendNoteViewModel(final Context context, final SendNoteInterface callback, final InspectionNoteWithId model){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.toJSONObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Inspections/AddNoteToInspection")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.sendNoteSuccess();
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.sendNoteFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not post note - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.sendNoteFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.sendNoteFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void getInspection(final Context context, final GetInspectionInterface callback, final String inspectionId){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(baseUrl + "Inspections/GetInspection/" + inspectionId)
                            .get()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        JSONObject object = new JSONObject(response.body().string());
                        final Inspection inspection = new Inspection(object);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getInspectionSucces(inspection);
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getInspectionFailed();
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Could not get inspection - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getInspectionFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getInspectionFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void submitInspection(final Context context, final SubmitInspectionInterface callback, final InspectionStateIdViewModel model){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.toJSONObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Inspections/FinishInspection")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.submitInspecctionSuccess();
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.submitInspectionFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not send inspection state - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.submitInspectionFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.submitInspectionFailed();
                        }
                    });
                }
            }
        }).start();
    }




    public interface GetInspectionsForResidenceInterface{
        void getInspectionsForResidenceSuccess(List<InspectionPartialViewModel> inspections);
        void getInspectionsForResidenceFailed();
    }

    public interface NewInspectionInterface{
        void afterPostNewInspection();
        void postInspectionFailed();
    }

    public interface SendPhotoInterface{
        void sendPhotoSuccess();
        void sendPhotoFailed();
    }

    public interface SendNoteInterface{
        void sendNoteSuccess();
        void sendNoteFailed();
    }

    public interface GetInspectionInterface{
        void getInspectionSucces(Inspection inspection);
        void getInspectionFailed();
    }

    public interface SubmitInspectionInterface{
        void submitInspecctionSuccess();
        void submitInspectionFailed();
    }

}
