package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.PushNotificationHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FcmTokenHelper {

    public static final String TAG = "FcmHelper";

    public static void UpdateToken(final String token, final Context context){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    String metaData = "Debug-infos:";
                    metaData += "\n OS Version: " + System.getProperty("os.version") + "(" + Build.VERSION.INCREMENTAL + ")";
                    metaData += "\n OS API Level: " + Build.VERSION.SDK_INT;
                    metaData += "\n Device: " + Build.DEVICE;
                    metaData += "\n Model (and Product): " + Build.MODEL + "(" + Build.PRODUCT + ")";

                    JSONObject pushBody = new JSONObject();
                    pushBody.put("Id", PrefHelper.getDeviceId(context));
                    pushBody.put("DeviceType", 0);
                    pushBody.put("PushKey", token);
                    pushBody.put("HardwareId", Build.MODEL);
                    pushBody.put("MetaData", metaData);

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody requestBody = RequestBody.create(mediaType, pushBody.toString());
                    Request post = new Request.Builder()
                            .addHeader("Content-Type", "x-www-form-urlencoded")
                            .url(context.getString(R.string.base_url) + "Devices")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(post).execute();

                    if(response.code() == 200){
                        SharedPreferences.Editor editor = context.getSharedPreferences(PushNotificationHelper.FCM_PREFERENCES, Context.MODE_PRIVATE).edit();
                        editor.putBoolean("registrationPending", false);
                        editor.apply();
                    }else {
                        Log.e(TAG, "Token was not synced (" + response.code() + ") - " + response.message());
                    }

                }catch (JSONException error){
                    Log.e(TAG, "Json model could not be parsed -" + error.getMessage());
                }catch (IOException error){
                    Log.e(TAG, "Web request failed -" + error.getMessage());
                }
            }
        }).start();
    }
}
