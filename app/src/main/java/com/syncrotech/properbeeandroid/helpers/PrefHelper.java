package com.syncrotech.properbeeandroid.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.syncrotech.properbeeandroid.models.Occupant;
import com.syncrotech.properbeeandroid.models.Pet;
import com.syncrotech.properbeeandroid.models.TenancyApplication;
import com.syncrotech.properbeeandroid.models.TokenDetails;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

import java.util.UUID;

public class PrefHelper {
    public static final String ACCOUNT_PREFERENCES = "ACCOUNT";
    public static final String RESIDENCE_PREFERENCES = "RESIDENCE";

    public static String getOldUserName(Context context) {
        final SharedPreferences accountPreferences = context.getSharedPreferences(ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        return accountPreferences.getString("userName", "");
    }

    public static void saveUserName(String userName, Context context) {
        final SharedPreferences accountPreferences = context.getSharedPreferences(ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = accountPreferences.edit();
        editor.putString("userName", userName);
        editor.apply();
    }

    public static void saveTokenDetails(TokenDetails details, Context context) {
        final SharedPreferences accountPreferences = context.getSharedPreferences(ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = accountPreferences.edit();
        editor.putString("userName", details.getUserName());
        editor.putString("accessToken", details.getAccessToken());
        editor.putString("refreshToken", details.getRefreshToken());
        editor.putString("expires", DateHelper.getStringFromDate(details.getExpires()));
        editor.putString("userRole", details.getUserRole());
        editor.apply();
    }

    public static void saveDefaultResidence(Context context, Residence residence){
        final SharedPreferences preferences = context.getSharedPreferences(RESIDENCE_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("DefaultResidenceId", residence.id);
        editor.putString("DefaultResidenceAddress", residence.address);
        editor.apply();
    }

    public static String getDefaultResidenceId(Context context){
        final SharedPreferences preferences = context.getSharedPreferences(RESIDENCE_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString("DefaultResidenceId", null);
    }

    public static String getDefaultResidenceName(Context context){
        final SharedPreferences preferences = context.getSharedPreferences(RESIDENCE_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString("DefaultResidenceAddress", null);
    }

    public static TokenDetails getTokenDetails(Context context) {
        final SharedPreferences accountPreferences = context.getSharedPreferences(ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);

        if (!accountPreferences.getString("accessToken", "").isEmpty()) {
            return new TokenDetails(
                    accountPreferences.getString("accessToken", ""),
                    accountPreferences.getString("refreshToken", ""),
                    accountPreferences.getString("userName", ""),
                    DateHelper.getDateFromString(accountPreferences.getString("expires", "")),
                    accountPreferences.getString("userRole", "")
            );
        } else return null;
    }

    public static void logUserOff(Context context) {
        final SharedPreferences accountPreferences = context.getSharedPreferences(ACCOUNT_PREFERENCES, Context.MODE_PRIVATE);
        context.getSharedPreferences(RESIDENCE_PREFERENCES, Context.MODE_PRIVATE).edit().clear().apply();

        //Save the email before clearing the preferences
        String userName = accountPreferences.getString("userName", "");
        SharedPreferences.Editor editor = accountPreferences.edit();
        editor.clear().apply();
        //put the email back into the prefs
        editor.putString("userName", userName).apply();
    }

    public static String getDeviceId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Device", Context.MODE_PRIVATE);

        String deviceId = sharedPreferences.getString("DeviceID", null);
        if (deviceId == null || deviceId.isEmpty()) {
            deviceId = UUID.randomUUID().toString();
            sharedPreferences.edit().putString("DeviceID", deviceId).apply();
        }
        return deviceId;
    }

    public static void saveTenancyApplication(String userName, TenancyApplication tenancyApplication, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Application", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("userName", userName);

        //Current Address
        editor.putString("currentAddress", tenancyApplication.currentAddress.address);
        editor.putInt("currentLivingArrangments", tenancyApplication.currentAddress.livingArrangements);
        editor.putString("currentLeavingReason", tenancyApplication.currentAddress.leaveReason);
        editor.putString("currentStartDate", tenancyApplication.currentAddress.length);
        editor.putString("currentContactName", tenancyApplication.currentAddress.contactName);
        editor.putString("currentContactNumber", tenancyApplication.currentAddress.contactPhone);
        //Previous Address
        editor.putString("previousAddress", tenancyApplication.previousAddress.address);
        editor.putInt("previousLivingArrangments", tenancyApplication.previousAddress.livingArrangements);
        editor.putString("previousLeavingReason", tenancyApplication.previousAddress.leaveReason);
        editor.putString("previousStartDate", tenancyApplication.previousAddress.length);
        editor.putString("previousContactName", tenancyApplication.previousAddress.contactName);
        editor.putString("previousContactNumber", tenancyApplication.previousAddress.contactPhone);
        //New Address
        int occupantAmount = tenancyApplication.otherOccupants.size();
        editor.putInt("numOccupants",occupantAmount);
        for (int i = 0; i < occupantAmount; i++){
            editor.putString("occupantName" + i, tenancyApplication.otherOccupants.get(i).getName());
            editor.putInt("occupantAge" + i, tenancyApplication.otherOccupants.get(i).getAge());
            editor.putString("occupantRelationship" + i, tenancyApplication.otherOccupants.get(i).getRelationship());
            editor.putString("occupantId" + i, tenancyApplication.otherOccupants.get(i).getId());
        }

        editor.putString("rentalPeriod", tenancyApplication.intentRentalPeriod);
        editor.putBoolean("borders", tenancyApplication.intentBorders);
        editor.putInt("bordersAmount", tenancyApplication.intentBordersAmount);
        editor.putBoolean("commercial", tenancyApplication.intentCommercial);
        editor.putString("commercialDescription", tenancyApplication.intentCommercialDescription);

        int petAmount = tenancyApplication.pets.size();
        editor.putInt("numPets",petAmount);
        for (int i = 0; i < petAmount; i++){
            editor.putString("petBreed" + i, tenancyApplication.pets.get(i).getBreed());
            editor.putString("petType" + i, tenancyApplication.pets.get(i).getType());
            editor.putInt("petAge" + i, tenancyApplication.pets.get(i).getAge());
            editor.putString("petDescription" + i, tenancyApplication.pets.get(i).getDescription());
            editor.putString("petId" + i, tenancyApplication.pets.get(i).getId());
        }
        //Tenancy Tribunal
        editor.putBoolean("tenancyTribunal", tenancyApplication.tenancyTribunal);
        editor.putString("tenancyTribunalReason", tenancyApplication.tenancyTribunalReason);
        //Criminal
        editor.putBoolean("convicted", tenancyApplication.convicted);
        editor.putString("convictionDetails", tenancyApplication.convictedReason);
        editor.putBoolean("bailParole", tenancyApplication.bailParole);
        editor.putBoolean("boarderOnBailParole", tenancyApplication.bailParoleBorder);
        editor.putBoolean("monitoringDevice", tenancyApplication.monitoringDevice);
        editor.putBoolean("boarderMonitoringDevice", tenancyApplication.monitoringDeviceBorder);
        editor.putBoolean("residenceRestrictions", tenancyApplication.residenceRestrictions);
        editor.putBoolean("boarderResidenceRestrictions", tenancyApplication.residenceRestrictionsBorder);
        editor.putBoolean("bankrupt", tenancyApplication.bankrupt);
        //Employment
        editor.putBoolean("employed", tenancyApplication.employed);
        editor.putBoolean("selfEmployed", tenancyApplication.selfEmployed);
        editor.putString("occupation", tenancyApplication.occupation);
        editor.putInt("occupationHours", tenancyApplication.occupationHours);
        editor.putString("occupationTime", tenancyApplication.occupationTime);
        editor.putString("employerName", tenancyApplication.employerName);
        editor.putString("employerAddress", tenancyApplication.employerAddress);
        editor.putString("employerContactName", tenancyApplication.employerContactName);
        editor.putString("employerContactPhone", tenancyApplication.employerContactPhone);
        //Studying
        editor.putBoolean("student", tenancyApplication.student);
        editor.putString("institution", tenancyApplication.studentInstitution);
        editor.putString("course", tenancyApplication.studentCourse);
        editor.putString("courseEnd", DateHelper.getStringFromDate(tenancyApplication.studentCourseEnd));
        editor.putString("courseContactName", tenancyApplication.studentCourseAdminName);
        editor.putString("courseContactPhone", tenancyApplication.studentCourseAdminContact);
        editor.putString("studentSourceIncome", tenancyApplication.studentIncome);
        //WINZ
        editor.putBoolean("winz", tenancyApplication.winz);
        editor.putString("winzCategory", tenancyApplication.winzCategory);
        editor.putString("winzEnd", DateHelper.getStringFromDate(tenancyApplication.winzEnd));
        editor.putBoolean("winzRedirect", tenancyApplication.winzRedirect);
        editor.putInt("winzDay", tenancyApplication.winzDay);
        //House
        editor.putBoolean("houseOwner", tenancyApplication.houseOwner);
        editor.putString("houseAddress", tenancyApplication.houseAddress);
        //Vehicle
        editor.putBoolean("vehicleOwner", tenancyApplication.vehicleOwner);
        editor.putString("vehicleMake", tenancyApplication.vehicleMake);
        editor.putString("vehicleModel", tenancyApplication.vehicleModel);
        editor.putString("vehicleColour", tenancyApplication.vehicleColour);
        editor.putString("vehicleRego", tenancyApplication.vehicleRego);
        //Reference
        editor.putString("referenceRefernceName", tenancyApplication.reference.name);
        editor.putString("referenceRefernceAddress", tenancyApplication.reference.address);
        editor.putString("referenceReferncePhoneA", tenancyApplication.reference.phoneA);
        editor.putString("referenceReferncePhoneB", tenancyApplication.reference.phoneB);
        //Relative
        editor.putString("relativeRefernceName", tenancyApplication.reference.name);
        editor.putString("relativeRefernceAddress", tenancyApplication.reference.address);
        editor.putString("relativeReferncePhoneA", tenancyApplication.reference.phoneA);
        editor.putString("relativeReferncePhoneB", tenancyApplication.reference.phoneB);

        editor.apply();
    }

    public static TenancyApplication getTenancyApplication(String userName, Context context) {
        TenancyApplication tenancyApplication = new TenancyApplication();

        SharedPreferences sharedPreferences = context.getSharedPreferences("Application", Context.MODE_PRIVATE);
        String storedUser = sharedPreferences.getString("userName", "");
        if (userName.equals(storedUser)) {

            //Current Address
            tenancyApplication.currentAddress.address = sharedPreferences.getString("currentAddress", "");
            tenancyApplication.currentAddress.livingArrangements = sharedPreferences.getInt("currentLivingArrangments", 0);
            tenancyApplication.currentAddress.leaveReason = sharedPreferences.getString("currentLeavingReason", "");
            tenancyApplication.currentAddress.length = sharedPreferences.getString("currentStartDate", "");
            tenancyApplication.currentAddress.contactName = sharedPreferences.getString("currentContactName", "");
            tenancyApplication.currentAddress.contactPhone = sharedPreferences.getString("currentContactNumber", "");
            //Previous Address
            tenancyApplication.previousAddress.address = sharedPreferences.getString("previousAddress", "");
            tenancyApplication.previousAddress.livingArrangements = sharedPreferences.getInt("previousLivingArrangments", 0);
            tenancyApplication.previousAddress.leaveReason = sharedPreferences.getString("previousLeavingReason", "");
            tenancyApplication.previousAddress.length = sharedPreferences.getString("previousStartDate", "");
            tenancyApplication.previousAddress.contactName = sharedPreferences.getString("previousContactName", "");
            tenancyApplication.previousAddress.contactPhone = sharedPreferences.getString("previousContactNumber", "");
            //New Address
            int occupantAmount = sharedPreferences.getInt("numOccupants",0);
            for (int i = 0; i < occupantAmount; i++){
                Occupant occupant = new Occupant(sharedPreferences.getString("occupantId" + i, ""),
                        sharedPreferences.getString("occupantName" + i, ""),
                        sharedPreferences.getInt("occupantAge" + i, 0),
                        sharedPreferences.getString("occupantRelationship" + i, ""));
                tenancyApplication.otherOccupants.add(occupant);
            }

            tenancyApplication.intentRentalPeriod = sharedPreferences.getString("rentalPeriod", "");
            tenancyApplication.intentBorders = sharedPreferences.getBoolean("borders", false);
            tenancyApplication.intentBordersAmount = sharedPreferences.getInt("bordersAmount", 0);
            tenancyApplication.intentCommercial = sharedPreferences.getBoolean("commercial", false);
            tenancyApplication.intentCommercialDescription = sharedPreferences.getString("commercialDescription", "");

            int petAmount = sharedPreferences.getInt("numPets",0);
            for (int i = 0; i < petAmount; i++){
                Pet pet = new Pet(sharedPreferences.getString("petId" + i, ""),
                        sharedPreferences.getString("petType" + i, ""),
                        sharedPreferences.getString("petBreed" + i, ""),
                        sharedPreferences.getInt("petAge" + i, 0),
                        sharedPreferences.getString("petDescription" + i, ""));
                tenancyApplication.pets.add(pet);

            }
            //Tenancy Tribunal
            tenancyApplication.tenancyTribunal = sharedPreferences.getBoolean("tenancyTribunal", false);
            tenancyApplication.tenancyTribunalReason = sharedPreferences.getString("tenancyTribunalReason", "");
            //Criminal
            tenancyApplication.convicted = sharedPreferences.getBoolean("convicted", false);
            tenancyApplication.convictedReason = sharedPreferences.getString("convictionDetails", "");
            tenancyApplication.bailParole = sharedPreferences.getBoolean("bailParole", false);
            tenancyApplication.bailParoleBorder = sharedPreferences.getBoolean("boarderOnBailParole", false);
            tenancyApplication.monitoringDevice = sharedPreferences.getBoolean("monitoringDevice", false);
            tenancyApplication.monitoringDeviceBorder = sharedPreferences.getBoolean("boarderMonitoringDevice", false);
            tenancyApplication.residenceRestrictions = sharedPreferences.getBoolean("residenceRestrictions", false);
            tenancyApplication.residenceRestrictionsBorder = sharedPreferences.getBoolean("boarderResidenceRestrictions", false);
            tenancyApplication.bankrupt = sharedPreferences.getBoolean("bankrupt", false);
            //Employment
            tenancyApplication.employed = sharedPreferences.getBoolean("employed", false);
            tenancyApplication.selfEmployed = sharedPreferences.getBoolean("selfEmployed", false);
            tenancyApplication.occupation = sharedPreferences.getString("occupation", "");
            tenancyApplication.occupationHours = sharedPreferences.getInt("occupationHours", 0);
            tenancyApplication.occupationTime = sharedPreferences.getString("occupationTime", "");
            tenancyApplication.employerName = sharedPreferences.getString("employerName", "");
            tenancyApplication.employerAddress = sharedPreferences.getString("employerAddress", "");
            tenancyApplication.employerContactName = sharedPreferences.getString("employerContactName", "");
            tenancyApplication.employerContactPhone = sharedPreferences.getString("employerContactPhone", "");
            //Studying
            tenancyApplication.student = sharedPreferences.getBoolean("student", false);
            tenancyApplication.studentInstitution = sharedPreferences.getString("institution", "");
            tenancyApplication.studentCourse = sharedPreferences.getString("course", "");
            tenancyApplication.studentCourseEnd = DateHelper.getDateFromString(sharedPreferences.getString("courseEnd", "1900 04 04"));
            tenancyApplication.studentCourseAdminName = sharedPreferences.getString("courseContactName", "");
            tenancyApplication.studentCourseAdminContact = sharedPreferences.getString("courseContactPhone", "");
            tenancyApplication.studentIncome = sharedPreferences.getString("studentSourceIncome", "");
            //WINZ
            tenancyApplication.winz = sharedPreferences.getBoolean("winz", false);
            tenancyApplication.winzCategory = sharedPreferences.getString("winzCategory", "");
            tenancyApplication.winzEnd = DateHelper.getDateFromString(sharedPreferences.getString("winzEnd", "1900 04 04"));
            tenancyApplication.winzRedirect = sharedPreferences.getBoolean("winzRedirect", false);
            tenancyApplication.winzDay = sharedPreferences.getInt("winzDay", 0);
            //House
            tenancyApplication.houseOwner = sharedPreferences.getBoolean("houseOwner", false);
            tenancyApplication.houseAddress = sharedPreferences.getString("houseAddress", "");
            //Vehicle
            tenancyApplication.vehicleOwner = sharedPreferences.getBoolean("vehicleOwner", false);
            tenancyApplication.vehicleMake = sharedPreferences.getString("vehicleMake", "");
            tenancyApplication.vehicleModel = sharedPreferences.getString("vehicleModel", "");
            tenancyApplication.vehicleColour = sharedPreferences.getString("vehicleColour", "");
            tenancyApplication.vehicleRego = sharedPreferences.getString("vehicleRego", "");
            //Reference
            tenancyApplication.reference.name = sharedPreferences.getString("referenceRefernceName", "");
            tenancyApplication.reference.address = sharedPreferences.getString("referenceRefernceAddress", "");
            tenancyApplication.reference.phoneA = sharedPreferences.getString("referenceReferncePhoneA", "");
            tenancyApplication.reference.phoneB = sharedPreferences.getString("referenceReferncePhoneB", "");
            //Relative
            tenancyApplication.reference.name = sharedPreferences.getString("relativeRefernceName", "");
            tenancyApplication.reference.address = sharedPreferences.getString("relativeRefernceAddress", "");
            tenancyApplication.reference.phoneA = sharedPreferences.getString("relativeReferncePhoneA", "");
            tenancyApplication.reference.phoneB = sharedPreferences.getString("relativeReferncePhoneB", "");

        }
        return tenancyApplication;
    }

}
