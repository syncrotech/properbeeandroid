package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.Message;
import com.syncrotech.properbeeandroid.models.dbModels.Conversation;
import com.syncrotech.properbeeandroid.models.viewModels.AddParticipantViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.PostConversationViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.SendMessageViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ConversationsHelper {

    private static final String TAG = "ConversationsHelper";

    public static void getConversations(final Context context, final getConversationsInterface callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(baseUrl + "Conversations")
                            .get()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        JSONArray array = new JSONArray(response.body().string());
                        final List<Conversation> conversations = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            conversations.add(new Conversation(array.getJSONObject(i)));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.afterGetConversations(conversations);
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Could not get conversations - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getConversationsFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getConversationsFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void getMessages(final Context context, final getMessagesInterface callback, final String conversationId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(baseUrl + "Conversations/" + conversationId)
                            .get()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        JSONArray array = new JSONArray(response.body().string());
                        final List<Message> messages = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            messages.add(new Message(array.getJSONObject(i)));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.afterGetMessages(messages);
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getMessagesFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not get messages - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getMessagesFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getMessagesFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void startNewConversation(final Context context, final postConversationInterface callback, final PostConversationViewModel model) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.getJsonObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Conversations")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        String conversationId = response.body().string();
                        final String formatedId = conversationId.substring(1, conversationId.length() - 1);
                        callback.afterPostConversation(formatedId);
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postConversationFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not make conversation - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postConversationFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postConversationFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void addParticipantToConversation(final Context context, final addParticipantInterface callback, final AddParticipantViewModel model) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.getJsonObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Conversations/AddParticipantToConversation")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.afterAddParticipant();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not add participants - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.addParticipantFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.addParticipantFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void leaveConversation(final Context context, final leaveConversationInterface callback, final String conversationId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");

                    JSONObject obj = new JSONObject();
                    obj.put("ConversationId", conversationId);
                    RequestBody postBody = RequestBody.create(mediaType, obj.toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Conversations/LeaveConversation")
                            .post(postBody)
                            .addHeader("Authorization", "Bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.afterLeaveConversation();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not leave conversation - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.leaveConversationFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.leaveConversationFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void sendMessage(final Context context, final sendMessageInterface callback, final SendMessageViewModel model) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.getJsonObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Conversations/SendMessage")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        final Message message = new Message();
                        message.seen = true;
                        message.message = model.message;
                        message.userMessage = true;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.afterSendMessage(message);
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not send message - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.sendMessageFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.sendMessageFailed();
                        }
                    });
                }
            }
        }).start();
    }


    public interface getConversationsInterface {
        void afterGetConversations(List<Conversation> conversations);

        void getConversationsFailed();
    }

    public interface getMessagesInterface {
        void afterGetMessages(List<Message> messages);

        void getMessagesFailed();
    }

    public interface postConversationInterface {
        void afterPostConversation(String conversationId);

        void postConversationFailed();
    }

    public interface addParticipantInterface {
        void afterAddParticipant();

        void addParticipantFailed();
    }

    public interface leaveConversationInterface {
        void afterLeaveConversation();

        void leaveConversationFailed();
    }

    public interface sendMessageInterface {
        void afterSendMessage(Message message);

        void sendMessageFailed();
    }
}
