package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Notification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ScryJ_000 on 31/10/2016.
 */

public class NotificationHelper {
    public static final String TAG = "NotificationHelper";

    public static void getNotifications(final Context context, final NotificationInterface callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();


                    Request request = new Request.Builder()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Notifications")
                            .get()
                            .build();

                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){
                        JSONArray notifications = new JSONArray(response.body().string());
                        final List<Notification> notificationList = new ArrayList<Notification>();
                        for (int i = 0; i < notifications.length(); i++) {
                            notificationList.add(new Notification(notifications.getJSONObject(i)));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.notificationSuccess(notificationList);
                            }
                        });
                    }else{
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Could not get notifications (" + response.code() + ") - " + response.message());
                            }
                        });
                    }

                }catch (final IOException e){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG, "IOEception while getting notifications " + e.getMessage());
                            callback.notificationFailed(e.getMessage());
                        }
                    });

                }catch(final JSONException e){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG, "Could not unwrap JSON " + e.getMessage());
                            callback.notificationFailed(e.getMessage());
                        }
                    });
                }

            }
        }).start();
    }

    public static void dismissNotification(final String notificationId, final Context context, final DismissNotificationInterface callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("Status", 2);
                    jsonObject.put("NotificationId", notificationId);
                    RequestBody requestBody = RequestBody.create(mediaType, jsonObject.toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Notifications")
                            .post(requestBody)
                            .addHeader("Content-Type", "x-www-form-urlencoded")
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();
                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){

                    }
                    else{
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.dismissNotificationFailed("Server error");
                            }
                        });
                    }
                }catch (final IOException e){
                    Log.e(TAG, "IO Exception while getting residences " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.dismissNotificationFailed(e.getMessage());
                        }
                    });

                }catch (final JSONException e){
                    Log.e(TAG, "Could not unwrap json (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.dismissNotificationFailed(e.getMessage());
                        }
                    });

                }
            }
        }).start();
    }


    public interface NotificationInterface{
        void notificationSuccess(List<Notification> notificationList);

        void notificationFailed(String errorMessage);
    }

    public interface DismissNotificationInterface{
        void dismissNotificationFailed(String errorMessage);
    }

}
