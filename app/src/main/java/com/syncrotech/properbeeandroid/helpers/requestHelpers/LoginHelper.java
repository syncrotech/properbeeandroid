package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.TokenDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginHelper {

    public static void getAndSaveTokenFromApi(final String email, final String password, final Context context, final TokenInterface callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    RequestBody body = new FormBody.Builder()
                            .add("userName", email)
                            .add("grant_type", "password")
                            .add("Password", password)
                            .build();

                    Request request = new Request.Builder()
                            .url(baseUrl + "Account/Token")
                            .post(body)
                            .addHeader("content-type", "application/x-www-form-urlencoded")
                            .addHeader("DeviceId", PrefHelper.getDeviceId(context))
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200){
                        JSONObject jsonToken = new JSONObject(response.body().string());


                        String accessToken = jsonToken.getString("access_token");
                        String refreshToken= jsonToken.getString("refresh_token");
                        String userName= jsonToken.getString("userName");
                        String userRole= jsonToken.getString("userRole");
                        int expiresIn= jsonToken.getInt("expires_in");

                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.MILLISECOND, expiresIn);

                        TokenDetails tokenDetails = new TokenDetails(accessToken, refreshToken, userName, cal.getTime(), userRole);
                        PrefHelper.saveTokenDetails(tokenDetails, context);
                        response.close();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.tokenSuccess();
                            }
                        });

                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("LOGIN_HELPER", "Could not login (" + response.code() + ") - " + response.message());
                                    callback.tokenFailed(response.message());
                            }
                        });
                    }
                } catch (final IOException e){
                    Log.e("LOGIN_HELPER", "IO Exception on login " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.tokenFailed(e.getMessage());
                        }
                    });
                } catch(JSONException e){
                    Log.e("LOGIN_HELPER", "Could not get json object");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.tokenFailed("JSON Error");
                        }
                    });
                }
            }
        }).start();
    }


    public interface TokenInterface {
        void tokenSuccess();

        void tokenFailed(String errorMessage);
    }
}
