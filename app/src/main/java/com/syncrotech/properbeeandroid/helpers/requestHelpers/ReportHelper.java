package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Report;
import com.syncrotech.properbeeandroid.models.viewModels.ReportCreateViewModel;
import com.syncrotech.properbeeandroid.models.viewModels.ReportMessageIdViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ReportHelper {
    public static final String TAG = "ReportHelper";

    public static void getReports(final Context context, final GetReportsInterface callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean isLandlord = PrefHelper.getTokenDetails(context).getUserRole().equals("Landlord");

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();


                    Request request = new Request.Builder()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(isLandlord?context.getString(R.string.base_url) + "Reports/GetForResidence/" + PrefHelper.getDefaultResidenceId(context): context.getString(R.string.base_url) + "Reports")
                            .get()
                            .build();

                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){
                        JSONArray reports = new JSONArray(response.body().string());
                        final List<ReportMessageIdViewModel> reportList = new ArrayList<>();
                        for (int i = 0; i < reports.length(); i++) {
                            reportList.add(new ReportMessageIdViewModel(reports.getJSONObject(i)));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getReportsSuccess(reportList);
                            }
                        });
                    }else{
                        Log.e(TAG, "Could not get reports (" + response.code() + ") - " + response.message());
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getReportsFailed();
                            }
                        });
                    }

                }catch (final IOException e){
                    Log.e(TAG, "IOEception while getting notifications " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getReportsFailed();
                        }
                    });

                }catch(final JSONException e){
                    Log.e(TAG, "Could not unwrap JSON " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getReportsFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void getReportWithId(final Context context, final GetReportWithIdInterface callback, final String reportId){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();


                    Request request = new Request.Builder()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Reports/" + reportId)
                            .get()
                            .build();

                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){
                        JSONObject jsonObject = new JSONObject(response.body().string());
                            final Report report = new Report(jsonObject);
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getReportWithIdSuccess(report);
                            }
                        });
                    }else{
                        Log.e(TAG, "Could not get report (" + response.code() + ") - " + response.message());
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.getReportWithIdFailed();
                            }
                        });
                    }

                }catch (final IOException e){
                    Log.e(TAG, "IOEception while getting notifications " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getReportWithIdFailed();
                        }
                    });

                }catch(final JSONException e){
                    Log.e(TAG, "Could not unwrap JSON " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.getReportWithIdFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void postNewReport(final Context context, final PostReportInterface callback, final ReportCreateViewModel model){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody postBody = RequestBody.create(mediaType, model.toJSONObject().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Reports")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postReportSuccess();
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postReportFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not post report - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postReportFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postReportFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void postReportResolved(final Context context, final PostReportInterface callback, final String reportId){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("ReportId", reportId);

                    RequestBody postBody = RequestBody.create(mediaType, jsonObject.toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Reports/ResolveReport")
                            .post(postBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postReportSuccess();
                            }
                        });
                    }
                    else{
                        Log.e(TAG, "Server error");
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.postReportFailed();
                            }
                        });
                    }

                } catch (IOException e) {
                    Log.e(TAG, "Could not post report - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postReportFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.postReportFailed();
                        }
                    });
                }
            }
        }).start();
    }



    public interface GetReportsInterface{
        void getReportsSuccess(List<ReportMessageIdViewModel> reportList);
        void getReportsFailed();
    }

    public interface PostReportInterface{
        void postReportSuccess();
        void postReportFailed();
    }

    public interface GetReportWithIdInterface{
        void getReportWithIdSuccess(Report report);
        void getReportWithIdFailed();
    }
}
