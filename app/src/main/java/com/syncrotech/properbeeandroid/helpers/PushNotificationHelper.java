package com.syncrotech.properbeeandroid.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.FcmTokenHelper;

public class PushNotificationHelper {
    public static final String FCM_PREFERENCES = "FcmToken";
    private static final String TAG = "PushMessagingHelper";

    public static void CheckFCMToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FCM_PREFERENCES, Context.MODE_PRIVATE);

        String token = FirebaseInstanceId.getInstance().getToken();
        FcmTokenHelper.UpdateToken(token, context);
    }

}
