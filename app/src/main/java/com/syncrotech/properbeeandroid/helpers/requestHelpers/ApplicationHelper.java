package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.TenancyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ScryJ_000 on 13/10/2016.
 */
public class ApplicationHelper {
    public static final String TAG = "ApplicationHelper";

    public static void updateApplication(final TenancyApplication tenancyApplication, final Context context, final ApplicationInterface callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    RequestBody requestBody = RequestBody.create(mediaType, tenancyApplication.toJSON().toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Applications/Update")
                            .post(requestBody)
                            .addHeader("Content-Type", "x-www-form-urlencoded")
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        Log.e(TAG, "Application updated (" + response.code() + ") - " + response.message());
                        if (tenancyApplication.state == 1) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.applicationUpdateSuccess();
                                }
                            });
                        }
                    } else {
                        Log.e(TAG, "Application was not updated (" + response.code() + ") - " + response.message());
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Application Update Failed" + e.getMessage());
                }
            }
        }).start();
    }

    public static void getApplications(final ApplicationInterface callback, final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Applications")
                            .get()
                            .build();
                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        JSONArray applications = new JSONArray(response.body().string());
                        final List<TenancyApplication> applicationList = new ArrayList<TenancyApplication>();
                        for (int i = 0; i < applications.length(); i++) {
                            applicationList.add(new TenancyApplication(applications.getJSONObject(i)));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.applicationSuccess(applicationList);
                            }
                        });

                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Could not get residences (" + response.code() + ") - " + response.message());
                                callback.applicationFailed(response.message());
                            }
                        });
                    }

                } catch (final IOException e) {
                    Log.e(TAG, "IO Exception while getting residences " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.applicationFailed(e.getMessage());
                        }
                    });
                } catch (final JSONException e) {
                    Log.e(TAG, "Could not unwrap json (" + e.getMessage() + ")");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.applicationFailed(e.getMessage());
                        }
                    });
                }

            }
        }).start();

    }

    public static void respondToApllication(final int response, final String applicationId, final Context context, final ApplicationResponseInterface callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("State", response);
                    jsonObject.put("Application_Id", applicationId);
                    RequestBody requestBody = RequestBody.create(mediaType, jsonObject.toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Applications/Respond")
                            .post(requestBody)
                            .addHeader("Content-Type", "x-www-form-urlencoded")
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();
                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.applicationResponseSuccess();
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.applicationResponseFailed("Server error");
                            }
                        });
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSON Exception while making response" + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.applicationResponseFailed("Server error");
                        }
                    });
                } catch (IOException e) {
                    Log.e(TAG, "IO Exception while making response " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.applicationResponseFailed("Server error");
                        }
                    });
                }

            }
        }).start();
    }

    public static void getApplication(final String applicationId, final ApplicationInterface callback, final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();


                    Request request = new Request.Builder()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .url(context.getString(R.string.base_url) + "Applications/" + applicationId)
                            .get()
                            .build();
                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        final ArrayList<TenancyApplication> applications = new ArrayList<TenancyApplication>();
                        TenancyApplication application = new TenancyApplication(jsonObject);
                        applications.add(application);

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.applicationSuccess(applications);
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, "Could not get applications (" + response.code() + ") - " + response.message());
                                callback.applicationFailed(response.message());
                            }
                        });
                    }


                } catch (IOException e) {
                    Log.e(TAG, "JSON Exception while getting application" + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.applicationFailed("Server error");
                        }
                    });

                } catch (JSONException e){
                    Log.e(TAG, "IO Exception while getting application " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.applicationFailed("Server error");
                        }
                    });

                }
            }
        }).start();
    }


    public interface ApplicationInterface {
        void applicationFailed(String errorMessage);

        void applicationSuccess(List<TenancyApplication> applicationList);

        void applicationUpdateSuccess();
    }

    public interface ApplicationResponseInterface{
        void applicationResponseFailed(String errorMessage);

        void applicationResponseSuccess();
    }
}
