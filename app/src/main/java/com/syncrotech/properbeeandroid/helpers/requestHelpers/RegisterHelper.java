package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.models.dbModels.Owner;
import com.syncrotech.properbeeandroid.models.dbModels.Tenant;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ScryJ_000 on 18/09/2016.
 */
public class RegisterHelper {

    public static void registerOwner(final Owner owner, final String password, final String conformPassword,
                                      final Context context, final RegisterInterface callback){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = new FormBody.Builder()
                            .add("Email", owner.email)
                            .add("Password", password)
                            .add("ConfirmPassword", conformPassword)
                            .add("FirstName", owner.firstName)
                            .add("MiddleName", owner.middleName)
                            .add("LastName", owner.lastName)
                            .add("ContactNumber", owner.contactNumber)

                            .add("Owner.Address", owner.address)
                            .add("Owner.City", owner.city)
                            .add("Owner.Occupation", owner.occupation)
                            .build();

                    Request request = new Request.Builder()
                            .url("http://properbee.azurewebsites.net/api/Account/Register")
                            .post(body)
                            .addHeader("content-type", "application/x-www-form-urlencoded")
                            .build();
                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.registerSuccess();
                            }
                        });

                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("REGISTER_HELPER", "Could not register (" + response.code() + ") - " + response.message());
                                callback.registerFailed(response.message());
                            }
                        });
                    }

                }catch (final IOException e){
                    Log.e("REGISTER_HELPER", "IO Exception on register " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.registerFailed(e.getMessage());
                        }
                    });
                }
//                catch(JSONException e){
//                    Log.e("REGISTER_HELPER", "Could not get json object");
//                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                        @Override
//                        public void run() {
//                            callback.registerFailed("JSON Error");
//                        }
//                    });
//                }
            }
        }).start();
    }

    public static void registerTenant(final Tenant tenant, final String password, final String conformPassword,
                                      final Context context, final RegisterInterface callback){
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();


                    MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
                    RequestBody body = new FormBody.Builder()
                            .add("Email", tenant.email)
                            .add("Password", password)
                            .add("ConfirmPassword", conformPassword)
                            .add("FirstName", tenant.firstName)
                            .add("MiddleName", tenant.middleName)
                            .add("LastName", tenant.lastName)
                            .add("ContactNumber", tenant.contactNumber)

                            .add("Tenant.DOB", tenant.dob)
                            .add("Tenant.ResidentialStatus", String.valueOf(tenant.residentialStatus))
                            .build();

                    Request request = new Request.Builder()
                            .url("http://properbee.azurewebsites.net/api/Account/Register")
                            .post(body)
                            .addHeader("content-type", "application/x-www-form-urlencoded")
                            .build();
                    final Response response = client.newCall(request).execute();

                    if(response.code() == 200){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.registerSuccess();
                            }
                        });

                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("REGISTER_HELPER", "Could not register (" + response.code() + ") - " + response.message());
                                callback.registerFailed(response.message());
                            }
                        });
                    }

                }catch (final IOException e){
                    Log.e("REGISTER_HELPER", "IO Exception on register " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.registerFailed(e.getMessage());
                        }
                    });
                }
            }
        }).start();
    }




    public interface RegisterInterface {
        void registerSuccess();

        void registerFailed(String errorMessage);
    }
}
