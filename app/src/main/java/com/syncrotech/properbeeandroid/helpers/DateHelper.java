package com.syncrotech.properbeeandroid.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ScryJ_000 on 17/09/2016.
 */
public class DateHelper {
    public static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    public static String getStringFromDate(Date date){
        DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);
        return dateFormat.format(date);
    }

    public static String getStringFromDate(Date date, String format){
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    public static Date getDateFromString(String string){
        DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);
        try {
            return dateFormat.parse(string);
        }
        catch (Exception e){

            return Calendar.getInstance().getTime();
        }
    }


}
