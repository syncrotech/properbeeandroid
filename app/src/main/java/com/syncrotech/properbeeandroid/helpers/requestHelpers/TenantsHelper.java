package com.syncrotech.properbeeandroid.helpers.requestHelpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.models.ResidenceTenantModel;
import com.syncrotech.properbeeandroid.models.SearchTenantModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class TenantsHelper {

    public static final String TAG = "TenantsHelper";

    public static void getTenantsForResidence(final Context context, final GetTenantsResponseInterface callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);
                    String residenceId = PrefHelper.getDefaultResidenceId(context);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    Request request = new Request.Builder()
                            .url(baseUrl + "Residences/GetTenantsForResidence/" + residenceId)
                            .get()
                            .addHeader("Content-Type", "x-www-form-urlencoded")
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        final List<ResidenceTenantModel> data = new ArrayList<>();
                        JSONArray body = new JSONArray(response.body().string());
                        for (int i = 0; i < body.length(); i++) {
                            JSONObject jsonTenant = body.getJSONObject(i);
                            ResidenceTenantModel tenant = new ResidenceTenantModel();
                            if(PrefHelper.getTokenDetails(context).getUserRole().equals("Landlord")) {
                                tenant.firstName = jsonTenant.getString("FirstName");
                                if (!jsonTenant.isNull("MiddleName"))
                                    tenant.middleName = jsonTenant.getString("MiddleName");
                                tenant.lastName = jsonTenant.getString("LastName");
                                tenant.contactNumber = jsonTenant.getString("ContactNumber");
                                tenant.applicationId = jsonTenant.getString("ApplicationId");
                                tenant.state = ResidenceTenantModel.ApplicationStatus.values()[jsonTenant.getInt("State")];
                            }else{
                                tenant.firstName = jsonTenant.getString("FirstName");
                                if (!jsonTenant.isNull("MiddleName"))
                                    tenant.middleName = jsonTenant.getString("MiddleName");
                                tenant.lastName = jsonTenant.getString("LastName");
                                tenant.contactNumber = jsonTenant.getString("ContactNumber");
                            }
                            data.add(tenant);
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.tenantsSuccess(data);
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.tenantsFailed();
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Could not get tenants - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.tenantsFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.tenantsFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void addTenantToProperty(final Context context, final PostTenantsResponseInterface callback, final String tenantId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);
                    String residenceId = PrefHelper.getDefaultResidenceId(context);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    MediaType mediaType = MediaType.parse("application/json");

                    JSONObject postBody = new JSONObject();
                    postBody.put("TenantId", tenantId);
                    postBody.put("ResidenceId", residenceId);

                    RequestBody formBody = RequestBody.create(mediaType, postBody.toString());

                    Request request = new Request.Builder()
                            .url(baseUrl + "Applications")
                            .post(formBody)
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.tenantsPostedSuccess();
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.tenantsPostedFailed();
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Could not get tenants - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.tenantsPostedFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.tenantsPostedFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public static void searchForTenants(final Context context, final SearchTenantsInterface callback, final String searchTerm) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String baseUrl = context.getString(R.string.base_url);

                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(10, TimeUnit.SECONDS)
                            .writeTimeout(10, TimeUnit.SECONDS)
                            .readTimeout(30, TimeUnit.SECONDS)
                            .build();

                    String url = baseUrl + "Applications/FindUser/" + URLEncoder.encode(searchTerm.trim(), "utf-8");

                    Request request = new Request.Builder()
                            .url(url)
                            .get()
                            .addHeader("Authorization", "bearer " + PrefHelper.getTokenDetails(context).getAccessToken())
                            .build();

                    final Response response = client.newCall(request).execute();

                    if (response.code() == 200) {
                        final List<SearchTenantModel> tenants = new ArrayList<>();
                        JSONArray body = new JSONArray(response.body().string());
                        for (int i = 0; i < body.length(); i++) {
                            JSONObject obj = body.getJSONObject(i);
                            tenants.add(new SearchTenantModel(
                                    obj.getString("Name"),
                                    obj.getString("Email"),
                                    obj.getString("Id")
                            ));
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.searchTenantsSuccess(tenants);
                            }
                        });
                    } else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.searchTenantsFailed();
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Could not get tenants - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.searchTenantsFailed();
                        }
                    });
                } catch (JSONException e) {
                    Log.e(TAG, "Could not parse JSON - " + e.getMessage());
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.searchTenantsFailed();
                        }
                    });
                }
            }
        }).start();
    }

    public interface GetTenantsResponseInterface {
        void tenantsSuccess(List<ResidenceTenantModel> data);

        void tenantsFailed();
    }

    public interface PostTenantsResponseInterface {
        void tenantsPostedSuccess();

        void tenantsPostedFailed();
    }

    public interface SearchTenantsInterface {
        void searchTenantsSuccess(List<SearchTenantModel> tenants);

        void searchTenantsFailed();
    }

}

