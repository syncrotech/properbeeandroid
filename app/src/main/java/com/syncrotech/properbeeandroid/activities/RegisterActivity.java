package com.syncrotech.properbeeandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.LoginHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.RegisterHelper;
import com.syncrotech.properbeeandroid.models.TokenDetails;
import com.syncrotech.properbeeandroid.models.dbModels.Owner;
import com.syncrotech.properbeeandroid.models.dbModels.Tenant;

public class RegisterActivity extends AppCompatActivity implements RegisterHelper.RegisterInterface, LoginHelper.TokenInterface {

    private Context mContext;
    private TextView switchTenant, switchOwner, loadingState;
    private ViewFlipper viewFlipper;
    private EditText email, password, confirmPassword, firstName, middleName, lastName,
            contactNumber, ownerAddress, ownerCity, ownerOccupation;
    private DatePicker tenantDOB;
    private boolean isOwner;
    private TokenDetails tokenDetails;
    private Spinner residenceStatus;

    private TextInputLayout emailInput, passwordInput, confirmPasswordInput, firstNameInput, lastNameInput, contactNumberInput;

    private View loadingPanel, registerPanel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = this;

        isOwner = true;

        registerPanel = findViewById(R.id.register_input);
        loadingPanel = findViewById(R.id.loading_panel);

        viewFlipper = (ViewFlipper) findViewById(R.id.register_view_flipper);

        switchTenant = (TextView) findViewById(R.id.register_switch_tenant);
        switchOwner = (TextView) findViewById(R.id.register_switch_owner);
        loadingState = (TextView) findViewById(R.id.loginState);

        switchTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFlipper.setDisplayedChild(1);
                isOwner = false;
            }
        });

        switchOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewFlipper.setDisplayedChild(0);
                isOwner = true;
            }
        });


        email = (EditText) findViewById(R.id.register_email);
        password = (EditText) findViewById(R.id.register_password);
        confirmPassword = (EditText) findViewById(R.id.register_confirm_password);
        firstName = (EditText) findViewById(R.id.register_first_name);
        middleName = (EditText) findViewById(R.id.register_middle_name);
        lastName = (EditText) findViewById(R.id.register_last_name);
        contactNumber = (EditText) findViewById(R.id.register_contact_number);
        ownerAddress = (EditText) findViewById(R.id.register_owner_address);
        ownerCity = (EditText) findViewById(R.id.register_owner_city);
        ownerOccupation = (EditText) findViewById(R.id.register_owner_occupation);
        tenantDOB = (DatePicker) findViewById(R.id.register_tenant_dob);
        residenceStatus = (Spinner) findViewById(R.id.residenceStatus);

        String[] residenceStatusText = getResources().getStringArray(R.array.residence_status);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, residenceStatusText);
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        residenceStatus.setAdapter(spinnerAdapter);

        emailInput = (TextInputLayout) findViewById(R.id.register_input_email);
        passwordInput = (TextInputLayout) findViewById(R.id.register_input_password);
        confirmPasswordInput = (TextInputLayout) findViewById(R.id.register_input_confirm_password);
        firstNameInput = (TextInputLayout) findViewById(R.id.register_input_first_name);
        lastNameInput = (TextInputLayout) findViewById(R.id.register_input_last_name);
        contactNumberInput = (TextInputLayout) findViewById(R.id.register_input_contact_number);
    }

    public void registerClick(View view) {

        Boolean valid = validate();

        if (isOwner) {

            if (valid) {
                loadingState.setText("Loading");
                registerPanel.setVisibility(View.GONE);
                loadingPanel.setVisibility(View.VISIBLE);
                Owner owner = new Owner(email.getText().toString(), firstName.getText().toString(), middleName.getText().toString(),
                        lastName.getText().toString(), contactNumber.getText().toString(), ownerAddress.getText().toString(),
                        ownerCity.getText().toString(), ownerOccupation.getText().toString());

                RegisterHelper.registerOwner(owner, password.getText().toString(), confirmPassword.getText().toString(), this, this);
            }

        } else {

            if (valid) {
                registerPanel.setVisibility(View.GONE);
                loadingPanel.setVisibility(View.VISIBLE);
                loadingState.setText("Loading");
                Tenant tenant = new Tenant(email.getText().toString(), firstName.getText().toString(), middleName.getText().toString(),
                        lastName.getText().toString(), contactNumber.getText().toString(), (tenantDOB.getMonth() + 1) + "." + tenantDOB.getDayOfMonth() + "." + (tenantDOB.getYear() % 100),
                        residenceStatus.getSelectedItemPosition());

                RegisterHelper.registerTenant(tenant, password.getText().toString(), confirmPassword.getText().toString(), this, this);
            }
        }
    }

    @Override
    public void registerSuccess() {
        LoginHelper.getAndSaveTokenFromApi(email.getText().toString(), password.getText().toString(), this, this);
        Toast.makeText(this, "Registration Successful", Toast.LENGTH_LONG).show();
    }

    @Override
    public void registerFailed(String errorMessage) {
        Toast.makeText(this, "Registration Unsuccessful", Toast.LENGTH_LONG).show();
        registerPanel.setVisibility(View.VISIBLE);
        loadingPanel.setVisibility(View.GONE);
    }

    @Override
    public void tokenSuccess() {
        tokenDetails = PrefHelper.getTokenDetails(mContext);
        Intent i = null;
        if (tokenDetails != null) {
            switch (tokenDetails.getUserRole()) {
                case "Landlord":
                    i = new Intent(this, OwnerActivity.class);
                    break;
                case "Tenant":
                    i = new Intent(this, TennantActivity.class);
                    break;
                default:
                    break;
            }
        }

        if (i != null) {
            finish();
            startActivity(i);
        } else {
            Toast.makeText(this, "Unable to login", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void tokenFailed(String errorMessage) {
        registerPanel.setVisibility(View.VISIBLE);
        loadingPanel.setVisibility(View.GONE);

    }

    public Boolean validate() {
        Boolean valid = true;

        //email validation
        if (email.getText().toString().isEmpty()) {
            emailInput.setError(getString(R.string.register_error_empty));
            valid = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            emailInput.setError(getString(R.string.register_error_email));
            valid = false;
        }

        //password validation
        if (password.getText().toString().isEmpty()) {
            passwordInput.setError(getString(R.string.register_error_empty));
            valid = false;
        } else if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            confirmPasswordInput.setError(getString(R.string.register_error_match_passwords));
            valid = false;
        }

        //first name validation
        if (firstName.getText().toString().isEmpty()) {
            firstNameInput.setError(getString(R.string.register_error_empty));
            valid = false;
        }

        //last name validation
        if (lastName.getText().toString().isEmpty()) {
            lastNameInput.setError(getString(R.string.register_error_empty));
            valid = false;
        }

        //phone validation
        if (contactNumber.getText().toString().isEmpty()) {
            contactNumberInput.setError(getString(R.string.register_error_empty));
            valid = false;
        } else if (!Patterns.PHONE.matcher(contactNumber.getText().toString()).matches()) {
            contactNumberInput.setError(getString(R.string.register_error_contact_phone));
            valid = false;
        }

        if (isOwner) {
            //address validation
            if (ownerAddress.getText().toString().isEmpty()) {
                ownerAddress.setError(getString(R.string.register_error_empty));
                valid = false;
            }

            //city validation
            if (ownerCity.getText().toString().isEmpty()) {
                ownerCity.setError(getString(R.string.register_error_empty));
                valid = false;
            }

            //occupation validation
            if (ownerOccupation.getText().toString().isEmpty()) {
                ownerOccupation.setError(getString(R.string.register_error_empty));
                valid = false;
            }

        } else {
            //DOB validation

        }

        return valid;
    }
}
