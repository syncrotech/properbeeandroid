package com.syncrotech.properbeeandroid.activities;

import android.app.Application;

import com.syncrotech.properbeeandroid.models.TenancyApplication;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

public class GlobalClass extends Application {

    public static final String DATA_SYNCED_FILTER = "DataSynced";

    public String userName;
    public TenancyApplication tenancyApplication;
    public Residence selectedResidence;
    public boolean dataSynced = false;
}
