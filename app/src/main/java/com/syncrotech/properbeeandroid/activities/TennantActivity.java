package com.syncrotech.properbeeandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantAccount;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantHome;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantInspection;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantMessage;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantNotification;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantReport;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantTenant;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.ResidenceHelper;
import com.syncrotech.properbeeandroid.models.dbModels.Residence;

import java.util.List;

public class TennantActivity extends AppCompatActivity implements ResidenceHelper.ResidenceInterface {

    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private View navHeader;
    private Context mContext;
    private TextView navHeaderUser;
    private boolean navDrawerEnabled;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tennant);

        navDrawerEnabled = true;

        mContext = this;

        ResidenceHelper.getResidences(this, this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.tenant_drawer_layout);
        drawerToggle = setupDrawerToggle();

        mDrawer.addDrawerListener(drawerToggle);

        nvDrawer = (NavigationView) findViewById(R.id.tenant_nav_view);
        setupDrawerContent(nvDrawer);
        navHeader = nvDrawer.inflateHeaderView(R.layout.tenant_nav_header);

        navHeaderUser = (TextView) navHeader.findViewById(R.id.tenant_nav_header_user_name);
        navHeaderUser.setText(((GlobalClass) getApplicationContext()).userName);

        MenuItem home = nvDrawer.getMenu().getItem(0);
        selectDrawerItem(home);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }


    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (menuItem.getItemId()) {
            case R.id.tenant_nav_home:
                fragmentClass = TenantHome.class;
                break;
            case R.id.tenant_nav_messages:
                if (navDrawerEnabled)
                    fragmentClass = TenantMessage.class;
                break;
            case R.id.tenant_nav_inspections:
                if (navDrawerEnabled)
                    fragmentClass = TenantInspection.class;
                break;
            case R.id.tenant_nav_reports:
                if (navDrawerEnabled)
                    fragmentClass = TenantReport.class;
                break;
            case R.id.tenant_nav_tenants:
                if (navDrawerEnabled)
                    fragmentClass = TenantTenant.class;
                break;
            case R.id.tenant_nav_notifications:
                fragmentClass = TenantNotification.class;
                break;
            case R.id.tenant_nav_account:
                fragmentClass = TenantAccount.class;
                break;
            case R.id.tenant_nav_logout:
                PrefHelper.logUserOff(mContext);
                Intent i = new Intent(mContext, LoginActivity.class);
                finish();
                startActivity(i);
                return;
            default:
                fragmentClass = TenantHome.class;
        }

        if (fragmentClass != null) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.tenant_content, fragment).commit();

            // Highlight the selected item has been done by NavigationView
            menuItem.setChecked(true);
            // Set action bar title
            setTitle(menuItem.getTitle());
            // Close the navigation drawer
        } else {
            Toast.makeText(this, "Can not navigate to this page without a property", Toast.LENGTH_LONG).show();
        }

        mDrawer.closeDrawers();
    }

    @Override
    public void residenceFailed(String errorMessage) {
        Toast.makeText(this, "Could not get residence", Toast.LENGTH_LONG).show();
    }

    @Override
    public void residenceSuccessNoResidence() {
        enableButtons(false);
        GlobalClass gc = (GlobalClass) getApplicationContext();
        gc.dataSynced = true;

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(GlobalClass.DATA_SYNCED_FILTER));
    }

    @Override
    public void residenceSuccess(List<Residence> residences) {
        PrefHelper.saveDefaultResidence(this, residences.get(0));
        enableButtons(true);
        GlobalClass gc = (GlobalClass) getApplicationContext();
        gc.selectedResidence = residences.get(0);
        gc.dataSynced = true;

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(GlobalClass.DATA_SYNCED_FILTER));
    }

    public void enableButtons(boolean enable) {
        navDrawerEnabled = enable;
    }

}
