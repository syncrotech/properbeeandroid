package com.syncrotech.properbeeandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.PushNotificationHelper;
import com.syncrotech.properbeeandroid.models.TokenDetails;

import java.util.Calendar;

public class SplashActivity extends AppCompatActivity {

    private final static int SPLASH_TIME_OUT = 3000;
    private TokenDetails tokenDetails;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        PushNotificationHelper.CheckFCMToken(this);

        mContext = this;

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tokenDetails = PrefHelper.getTokenDetails(SplashActivity.this);

                if (tokenDetails != null && Calendar.getInstance().getTime().after(tokenDetails.getExpires())) {
                    goToLogin();
                } else if (tokenDetails != null) {
                    goToDash();
                } else {
                    goToLogin();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    public void goToDash(){
        Intent intent;
        ((GlobalClass)getApplicationContext()).userName = tokenDetails.getUserName();
        switch (tokenDetails.getUserRole()){
            case "Landlord":
                intent = new Intent(mContext, OwnerActivity.class);
                break;
            case "Tenant":
                intent = new Intent(mContext, TennantActivity.class);
                break;
            default:
                intent = new Intent(mContext, LoginActivity.class);
                break;
        }
        startActivity(intent);
        finish();
    }

    public void goToLogin(){
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
