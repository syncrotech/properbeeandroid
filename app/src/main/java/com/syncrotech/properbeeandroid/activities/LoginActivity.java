package com.syncrotech.properbeeandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.syncrotech.properbeeandroid.R;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;
import com.syncrotech.properbeeandroid.helpers.requestHelpers.LoginHelper;
import com.syncrotech.properbeeandroid.models.TokenDetails;

public class LoginActivity extends AppCompatActivity implements LoginHelper.TokenInterface {

    private Context mContext;
    private EditText email, password;
    private TextView errorMessage, loginState;
    private View loadingPanel, loginPanel;


    private TokenDetails tokenDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;

        email = (EditText) findViewById(R.id.login_email);
        password = (EditText) findViewById(R.id.login_password);
        errorMessage = (TextView) findViewById(R.id.error_message);
        loginState = (TextView) findViewById(R.id.loginState);
        loadingPanel = findViewById(R.id.loading_panel);
        loginPanel = findViewById(R.id.login_input);

        String userName = PrefHelper.getOldUserName(mContext);
        email.setText(userName);

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    signIn();
                }
                return false;
            }
        });
    }

    public void goToActivity() {
        tokenDetails = PrefHelper.getTokenDetails(mContext);
        Intent i = null;
        if (tokenDetails != null) {
            switch (tokenDetails.getUserRole()) {
                case "Landlord":
                    i = new Intent(this, OwnerActivity.class);
                    break;
                case "Tenant":
                    i = new Intent(this, TennantActivity.class);
                    break;
                default:
                    break;
            }
        }

        if (i != null) {
            finish();
            startActivity(i);
        } else {
                errorMessage.setText(getString(R.string.role_unknown));
                errorMessage.setVisibility(View.VISIBLE);
                loginPanel.setVisibility(View.VISIBLE);
                loadingPanel.setVisibility(View.GONE);
        }
    }

    public void signIn() {

        final String eEmail = email.getText().toString();
        final String ePassword = password.getText().toString();
        if (eEmail.isEmpty() || ePassword.isEmpty()) {
            errorMessage.setText(R.string.enter_email);
            errorMessage.setVisibility(View.VISIBLE);
        } else {
            loginPanel.setVisibility(View.GONE);
            loadingPanel.setVisibility(View.VISIBLE);
            loginState.setText(R.string.logging_in);

            PrefHelper.saveUserName(eEmail, mContext);

            LoginHelper.getAndSaveTokenFromApi(eEmail, ePassword, this, this);
        }
    }

    public void loginClick(View view) {
        signIn();
    }

    public void loginFailed(){
        errorMessage.setText(getString(R.string.login_failed));
        errorMessage.setVisibility(View.VISIBLE);
        loginPanel.setVisibility(View.VISIBLE);
        loadingPanel.setVisibility(View.GONE);
    }

    public void registerClick(View view){
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);
    }

    @Override
    public void tokenSuccess() {
        goToActivity();
    }

    @Override
    public void tokenFailed(String errorMessage) {
        loginFailed();
    }
}
