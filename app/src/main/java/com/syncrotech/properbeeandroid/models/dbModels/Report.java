package com.syncrotech.properbeeandroid.models.dbModels;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

public class Report {
    public String id;
    public String message;
    public Bitmap photo;
    public boolean resolved;

    public Report(JSONObject object) throws JSONException {
        this.id = object.getString("Id");
        this.message = object.getString("Message");
        this.photo = getImageFromString(object.getString("Photo"));
        this.resolved = object.getBoolean("Resolved");
    }


    private Bitmap getImageFromString(String encodedImage){
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return  decodedByte;
    }
}