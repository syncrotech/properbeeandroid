package com.syncrotech.properbeeandroid.models.viewModels;

import android.graphics.Bitmap;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class InspectionPhotoWithId {
    public String inspectionId;
    public Bitmap photo;

    public InspectionPhotoWithId(String inspectionId, Bitmap photo) {
        this.inspectionId = inspectionId;
        this.photo = photo;
    }

    public JSONObject toJSONObject() throws JSONException{
        JSONObject object = new JSONObject();
        object.put("InsepctionId", this.inspectionId);
        object.put("Photo", encodePhoto(this.photo));
        return object;
    }


    private String encodePhoto(Bitmap photo){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return  encodedImage;
    }
}
