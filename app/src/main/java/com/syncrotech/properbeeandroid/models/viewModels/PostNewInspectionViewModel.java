package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 2/11/2016.
 */

public class PostNewInspectionViewModel {
    public String residenceId;
    public String inspectionDate;

    public PostNewInspectionViewModel(String residenceId, String inspectionDate) {
        this.residenceId = residenceId;
        this.inspectionDate = inspectionDate;
    }

    public JSONObject getJSONObject() throws JSONException{
        JSONObject object = new JSONObject();

        object.put("ResidenceId", this.residenceId);
        object.put("Date", this.inspectionDate);

        return  object;
    }
}
