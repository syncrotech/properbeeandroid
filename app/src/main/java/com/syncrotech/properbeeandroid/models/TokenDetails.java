package com.syncrotech.properbeeandroid.models;

import java.util.Date;

public class TokenDetails {
    private String accessToken;
    private String refreshToken;
    private String userName;
    private Date expires;
    private String userRole;


    public TokenDetails(String access_token, String refresh_token, String userName, Date expires, String userRole) {
        this.accessToken = access_token;
        this.refreshToken = refresh_token;
        this.userName = userName;
        this.expires = expires;
        this.userRole = userRole;
    }

    public Date getExpires() {
        return expires;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserRole() {
        return userRole;
    }

}


