package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class AddParticipantViewModel {

    public String conversationId;
    public List<String> participants;

    public JSONObject getJsonObject() throws JSONException{
        JSONObject obj = new JSONObject();
        obj.put("ConversationId", conversationId);
        JSONArray participants = new JSONArray(this.participants);
        obj.put("Participants", participants);
        return obj;
    }
}
