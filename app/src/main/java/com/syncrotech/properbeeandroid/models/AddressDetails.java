package com.syncrotech.properbeeandroid.models;

/**
 * Created by djwhy on 9/10/2016.
 */
public class AddressDetails {

    public String address;
    public int livingArrangements;
    public String length;
    public String leaveReason;
    public String contactName;
    public String contactPhone;

    public AddressDetails(){}

    public AddressDetails(String address, int livingArrangements, String length, String leaveReason, String contactName, String contactPhone) {
        this.address = address;
        this.livingArrangements = livingArrangements;
        this.length = length;
        this.leaveReason = leaveReason;
        this.contactName = contactName;
        this.contactPhone = contactPhone;
    }
}
