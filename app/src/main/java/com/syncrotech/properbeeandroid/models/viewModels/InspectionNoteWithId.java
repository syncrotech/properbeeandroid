package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class InspectionNoteWithId {
    public String inspectionId;
    public String note;

    public InspectionNoteWithId(String inspectionId, String note) {
        this.inspectionId = inspectionId;
        this.note = note;
    }

    public JSONObject toJSONObject() throws JSONException{
        JSONObject object = new JSONObject();
        object.put("InsepctionId", this.inspectionId);
        object.put("Note", this.note);
        return object;
    }
}
