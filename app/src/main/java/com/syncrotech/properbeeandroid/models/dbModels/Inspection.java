package com.syncrotech.properbeeandroid.models.dbModels;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class Inspection {
    public int state;
    public ArrayList<String> notes;
    public ArrayList<Bitmap> photos;

    public Inspection(JSONObject object) throws JSONException{
        this.state = object.getInt("State");

        this.notes = new ArrayList<>();
        JSONArray notes = object.getJSONArray("Notes");
        for(int i = 0; i < notes.length(); i++)
        {
            String note = notes.get(i).toString();
            this.notes.add(note);
        }

        this.photos = new ArrayList<>();
        JSONArray photos = object.getJSONArray("Photos");
        for(int i = 0; i < photos.length(); i++){
            Bitmap photo = getImageFromString(photos.get(i).toString());
            this.photos.add(photo);
        }
    }

    private Bitmap getImageFromString(String encodedImage){
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return  decodedByte;
    }

}
