package com.syncrotech.properbeeandroid.models;

/**
 * Created by ScryJ_000 on 6/10/2016.
 */
public class Occupant {
    private String id;
    private String name;
    private int age;
    private String relationship;

    public Occupant (String id, String name, int age, String relationship){
        this.id = id;
        this.name = name;
        this.age = age;
        this.relationship = relationship;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }
}
