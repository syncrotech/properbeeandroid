package com.syncrotech.properbeeandroid.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Message {
    public String message;
    public String userName;
    public boolean seen;
    public boolean userMessage;

    public Message(JSONObject obj) throws JSONException {
        this.message = obj.getString("Message");
        this.userName = obj.getString("UserName");
        this.seen = obj.getBoolean("Seen");
        this.userMessage = obj.getBoolean("UserMessage");
    }

    public Message() {
    }
}
