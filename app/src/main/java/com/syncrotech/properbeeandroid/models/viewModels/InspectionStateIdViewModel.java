package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class InspectionStateIdViewModel {
    public String inspectionId;
    public int state;

    public InspectionStateIdViewModel(String inspectionId, int state) {
        this.inspectionId = inspectionId;
        this.state = state;
    }

    public JSONObject toJSONObject() throws JSONException{
        JSONObject object = new JSONObject();
        object.put("InsepctionId", this.inspectionId);
        object.put("State", this.state);
        return object;
    }
}
