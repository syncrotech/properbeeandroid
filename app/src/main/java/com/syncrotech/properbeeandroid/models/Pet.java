package com.syncrotech.properbeeandroid.models;

/**
 * Created by ScryJ_000 on 6/10/2016.
 */
public class Pet {
    private String id;
    private String type;
    private String breed;
    private int age;
    private String description;

    public Pet(String id, String type, String breed, int age, String description){
        this.id = id;
        this.type = type;
        this.breed = breed;
        this.age = age;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
