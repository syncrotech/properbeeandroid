package com.syncrotech.properbeeandroid.models;

public class ResidenceTenantModel {

    public String applicationId;
    public String firstName;
    public String middleName;
    public String lastName;
    public String contactNumber;
    public ApplicationStatus state;
    public boolean titleRow = false;
    public String titleText;
    public ResidenceTenantModel() {
    }

    public String getName() {
        StringBuilder name = new StringBuilder();
        name.append(firstName);
        if (middleName != null && !middleName.isEmpty()) {
            name.append(" " + middleName);
        }
        name.append(" " + lastName);
        return name.toString();
    }

    public enum ApplicationStatus {AwaitingInformation, Completed, Declined, Accpeted}
}
