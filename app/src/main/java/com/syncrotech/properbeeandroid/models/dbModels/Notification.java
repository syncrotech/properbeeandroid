package com.syncrotech.properbeeandroid.models.dbModels;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.OwnerHome;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.OwnerInspection;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.OwnerMessage;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.OwnerNotification;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.ownerFragments.OwnerTenant;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantInspection;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantMessage;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantNotification;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantReport;
import com.syncrotech.properbeeandroid.fragments.navDrawerFragments.tenantFragments.TenantTenant;
import com.syncrotech.properbeeandroid.helpers.PrefHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 31/10/2016.
 */

public class Notification {
    public String id;
    public String message;
    public int actionArea;
    public boolean firstSeen;

    public Notification(JSONObject notification) throws JSONException{
        this.id = notification.getString("Id");
        this.message = notification.getString("Message");
        this.actionArea = notification.getInt("ActionArea");
        this.firstSeen = notification.getBoolean("firstSeen");
    }

    public Fragment getActionArea(Context context){
        Class fragmentClass;
        boolean landlord = PrefHelper.getTokenDetails(context).getUserRole().equals("Landlord");

        switch (this.actionArea){
            case 0:
                fragmentClass = landlord? OwnerInspection.class: TenantInspection.class;
                break;
            case 1:
                fragmentClass = landlord? OwnerTenant.class: TenantTenant.class;
                break;
            case 2:
                fragmentClass = landlord? OwnerHome.class: TenantReport.class;
                break;
            case 3:
                fragmentClass = landlord? OwnerMessage.class: TenantMessage.class;
                break;
            default:
                fragmentClass = landlord? OwnerNotification.class: TenantNotification.class;
                break;
        }

        Fragment fragment = null;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fragment;
    }

    public String getTitle(){
        String title = "";
        switch (this.actionArea){
            case 0:
                title = "Inspections";
                break;
            case 1:
                title = "Tenants";
                break;
            case 2:
                title = "Reports";
                break;
            case 3:
                title = "Messages";
                break;
            default:
                title = "Notifications";
                break;
        }

        return title;
    }

}
