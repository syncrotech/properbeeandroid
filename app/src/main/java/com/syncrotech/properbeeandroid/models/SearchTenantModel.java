package com.syncrotech.properbeeandroid.models;

public class SearchTenantModel {

    public String name;
    public String email;
    public String id;
    public boolean selected = false;

    public SearchTenantModel(String name, String email, String id) {
        this.name = name;
        this.email = email;
        this.id = id;
    }
}
