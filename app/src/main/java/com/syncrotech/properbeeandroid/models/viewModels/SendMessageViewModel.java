package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONException;
import org.json.JSONObject;

public class SendMessageViewModel {

    public String conversationId;
    public String message;

    public JSONObject getJsonObject() throws JSONException{
        JSONObject obj = new JSONObject();
        obj.put("ConversationId", conversationId);
        obj.put("Message", message);
        return obj;
    }
}
