package com.syncrotech.properbeeandroid.models.dbModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 4/10/2016.
 */
public class Owner extends User {
    public String address;
    public String city;
    public String occupation;

    public Owner() {

    }

    public Owner(String email, String firstName, String middleName, String lastName,
                 String contactNumber, String address, String city, String occupation) {
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
        this.address = address;
        this.city = city;
        this.occupation = occupation;
    }

    public Owner(JSONObject obj) throws JSONException {
        this.id = obj.getString("User_Id");
        this.firstName = obj.getString("FirstName");
        this.middleName = obj.getString("MiddleName");
        this.lastName = obj.getString("LastName");
        this.contactNumber = obj.getString("ContactNumber");
        this.address = obj.getString("Address");
        this.city = obj.getString("City");
        this.occupation = obj.getString("Occupation");
    }

}
