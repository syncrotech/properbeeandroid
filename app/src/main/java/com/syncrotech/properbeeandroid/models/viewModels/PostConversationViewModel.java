package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class PostConversationViewModel {

    public String name;
    public List<String> participants;
    public String residenceId;

    public JSONObject getJsonObject() throws JSONException{
        JSONObject obj = new JSONObject();
        obj.put("Name", this.name);
        JSONArray participants = new JSONArray(this.participants);
        obj.put("Participants", participants);
        obj.put("ResidenceId", residenceId);
        return obj;
    }
}
