package com.syncrotech.properbeeandroid.models.dbModels;

/**
 * Created by ScryJ_000 on 4/10/2016.
 */
public class User {
    public String id;
    public String email;
    public String firstName;
    public String middleName;
    public String lastName;
    public String contactNumber;
}
