package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class ReportMessageIdViewModel {
    public String id;
    public String message;

    public ReportMessageIdViewModel(JSONObject model) throws JSONException{
        this.id = model.getString("Id");
        this.message = model.getString("Message");
    }
}
