package com.syncrotech.properbeeandroid.models.dbModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 4/10/2016.
 */
public class Tenant extends User {
    public String dob;
    public int residentialStatus;

    public Tenant() {

    }

    public Tenant(String email, String firstName, String middleName, String lastName,
                  String contactNumber, String dob, int residentialStatus) {
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
        this.dob = dob;
        this.residentialStatus = residentialStatus;
    }

    public Tenant(JSONObject obj) throws JSONException {
        this.id = obj.getString("User_Id");
        this.firstName = obj.getString("FirstName");
        this.middleName = obj.getString("MiddleName");
        this.lastName = obj.getString("LastName");
        this.contactNumber = obj.getString("ContactNumber");
        this.dob = obj.getString("Dob");
        this.residentialStatus = obj.getInt("ResidentialStatus");
    }
}
