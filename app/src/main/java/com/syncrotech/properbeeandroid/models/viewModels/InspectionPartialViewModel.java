package com.syncrotech.properbeeandroid.models.viewModels;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ScryJ_000 on 3/11/2016.
 */

public class InspectionPartialViewModel {
    public String id;
    public String date;
    public int state;


    public InspectionPartialViewModel(JSONObject obj) throws JSONException {
        this.id = obj.getString("Id");
        this.date = obj.getString("Date");
        this.state = obj.getInt("State");
    }

}
