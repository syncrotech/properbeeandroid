package com.syncrotech.properbeeandroid.models.dbModels;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Residence {
    public String id;
    public List<Owner> owners;
    public List<Tenant> tenants;
    public String address;
    public int residenceType;
    public double rentalPrice;
    public int bedrooms;
    public int maxTenants;
    public int bathrooms;
    public int parking;
    public int pets;
    public String details;

    public Residence(String address, int residenceType, double rentalPrice, int bedrooms, int maxTenants, int bathrooms, int parking, int pets, String details) {
        this.address = address;
        this.residenceType = residenceType;
        this.rentalPrice = rentalPrice;
        this.bedrooms = bedrooms;
        this.maxTenants = maxTenants;
        this.bathrooms = bathrooms;
        this.parking = parking;
        this.pets = pets;
        this.details = details;
    }

    public Residence(JSONObject obj) throws JSONException {
        this.id = obj.getString("Id");
        JSONArray owners = obj.getJSONArray("Owners");
        this.owners = new ArrayList<>();
        for (int i = 0; i < owners.length(); i++) {
            this.owners.add(new Owner(owners.getJSONObject(i)));
        }
        JSONArray tenants = obj.getJSONArray("Tenants");
        this.tenants = new ArrayList<>();
        for (int i = 0; i < tenants.length(); i++) {
            this.tenants.add(new Tenant(tenants.getJSONObject(i)));
        }
        this.address = obj.getString("Address");
        this.residenceType = obj.getInt("ResidenceType");
        this.rentalPrice = obj.getDouble("RentalPrice");
        this.bathrooms = obj.getInt("Bathrooms");
        this.bedrooms = obj.getInt("Bedrooms");
        this.maxTenants = obj.getInt("MaxTenants");
        this.parking = obj.getInt("Parking");
        this.pets = obj.getInt("Pets");
        this.details = obj.getString("PropertyDetails");
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("Address", this.address);
        obj.put("ResidenceType", this.residenceType);
        obj.put("RentalPrice", this.rentalPrice);
        obj.put("Bedrooms", this.bedrooms);
        obj.put("MaxTenants", this.maxTenants);
        obj.put("Bathrooms", this.bathrooms);
        obj.put("Parking", this.parking);
        obj.put("PropertyDetails", this.details);
        obj.put("Pets", this.pets);

        return obj;
    }
}
