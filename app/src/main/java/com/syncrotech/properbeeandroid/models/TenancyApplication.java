package com.syncrotech.properbeeandroid.models;

import com.syncrotech.properbeeandroid.helpers.DateHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ScryJ_000 on 6/10/2016.
 */
public class TenancyApplication {
    public String id;
    public String address;
    //Rental History and Information
        //Current Address
    public AddressDetails currentAddress;
        //Previous Address
    public AddressDetails previousAddress;
        //New Address Info
    public ArrayList<Occupant> otherOccupants;
    public String intentRentalPeriod;
    public Boolean intentBorders = false;
    public int intentBordersAmount = 0;
    public Boolean intentCommercial = false;
    public String intentCommercialDescription;
    public ArrayList<Pet> pets;
        //Tenancy Tribunal
    public Boolean tenancyTribunal = false;
    public String tenancyTribunalReason;
    //Personal Income and References
        //Criminal
    public Boolean convicted = false;
    public String convictedReason;
    public Boolean bailParole = false;
    public Boolean bailParoleBorder = false;
    public Boolean monitoringDevice = false;
    public Boolean monitoringDeviceBorder = false;
    public Boolean residenceRestrictions = false;
    public Boolean residenceRestrictionsBorder = false;
    public Boolean bankrupt = false;
        //Income
            //Employment
    public Boolean employed = false;
    public Boolean selfEmployed = false;
    public String occupation;
    public int occupationHours = 0;
    public String occupationTime;
    public String employerName;
    public String employerAddress;
    public String employerContactName;
    public String employerContactPhone;
            //Studying
    public Boolean student = false;
    public String studentInstitution;
    public String studentCourse;
    public Date studentCourseEnd;
    public String studentCourseAdminName;
    public String studentCourseAdminContact;
    public String studentIncome;
    public Double studentIncomeAmount = 0.0;
            //WINZ
    public Boolean winz = false;
    public Double winzAmount = 0.0;
    public String winzCategory;
    public Date winzEnd;
    public Boolean winzRedirect = false;
    public int winzDay;
    public  Double winzAccSupp = 0.0;
            //Advance and Bond
    public Double advanceSavingsLoan = 0.0;
    public Double advanceWINZ = 0.0;
    public Double advanceBondTransfer = 0.0;
        //Assets
            //House
    public Boolean houseOwner = false;
    public String houseAddress;
            //Vehicle
    public Boolean vehicleOwner = false;
    public String vehicleMake;
    public String vehicleModel;
    public String vehicleColour;
    public String vehicleRego;
        //References
            //Reference
    public ReferenceDetails reference;
            //Relative
    public ReferenceDetails relative;

    public int state = 0;

    public TenancyApplication(){
        currentAddress = new AddressDetails();
        previousAddress = new AddressDetails();
        this.otherOccupants = new ArrayList<>();
        this.pets = new ArrayList<>();
        reference = new ReferenceDetails();
        relative = new ReferenceDetails();
    }

    public TenancyApplication(JSONObject application){
        try {
            unwrap(application);
        }catch (JSONException e){
            try{
            unwrapPartial(application);
            }catch (JSONException ex){

            }
        }
    }

    public JSONObject toJSON(){
        try {
            JSONObject currentAddress = new JSONObject();
            currentAddress.put("Address", this.currentAddress.address);
            currentAddress.put("LivingArrangments", this.currentAddress.livingArrangements);
            currentAddress.put("LeavingReason", this.currentAddress.leaveReason);
            currentAddress.put("LengthOfStay", this.currentAddress.length);
            currentAddress.put("ContactName", this.currentAddress.contactName);
            currentAddress.put("ContactNumber", this.currentAddress.contactPhone);

            JSONObject previousAddress = new JSONObject();
            previousAddress.put("Address", this.previousAddress.address);
            previousAddress.put("LivingArrangments", this.previousAddress.livingArrangements);
            previousAddress.put("LeavingReason", this.previousAddress.leaveReason);
            previousAddress.put("LengthOfStay", this.previousAddress.length);
            previousAddress.put("ContactName", this.previousAddress.contactName);
            previousAddress.put("ContactNumber", this.previousAddress.contactPhone);

            JSONArray otherOccupants = new JSONArray();
            for (Occupant occupant: this.otherOccupants) {
                JSONObject jsonOccupant = new JSONObject();
                jsonOccupant.put("Id", occupant.getId());
                jsonOccupant.put("Name", occupant.getName());
                jsonOccupant.put("Age", occupant.getAge());
                jsonOccupant.put("Relationship", occupant.getRelationship());
                otherOccupants.put(jsonOccupant);
            }

            JSONArray pets = new JSONArray();
            for (Pet pet: this.pets) {
                JSONObject jsonOccupant = new JSONObject();
                jsonOccupant.put("Id", pet.getId());
                jsonOccupant.put("Breed", pet.getBreed());
                jsonOccupant.put("Type", pet.getType());
                jsonOccupant.put("Age", pet.getAge());
                jsonOccupant.put("Description", pet.getDescription());
                pets.put(jsonOccupant);
            }

            JSONObject reference = new JSONObject();
            reference.put("RefernceName", this.reference.name);
            reference.put("RefernceAddress", this.reference.address);
            reference.put("ReferncePhoneA", this.reference.phoneA);
            reference.put("ReferncePhoneB", this.reference.phoneB);

            JSONObject relative = new JSONObject();
            relative.put("RefernceName", this.relative.name);
            relative.put("RefernceAddress", this.relative.address);
            relative.put("ReferncePhoneA", this.relative.phoneA);
            relative.put("ReferncePhoneB", this.relative.phoneB);


            JSONObject application = new JSONObject();
            application.put("Id", this.id);
            //Current Address
            application.put("CurrentAddress", currentAddress);
            //Previous Address
            application.put("PreviousAddress", previousAddress);
            //New Address
            application.put("OtherOccupants", otherOccupants);
            application.put("RentalPeriod", this.intentRentalPeriod);
            application.put("Borders", this.intentBorders);
            application.put("BordersAmount", this.intentBordersAmount);
            application.put("Commercial", this.intentCommercial);
            application.put("CommercialDescription", this.intentCommercialDescription);
            application.put("Pets", pets);
            //Tenancy Tribunal
            application.put("TenancyTribunal", this.tenancyTribunal);
            application.put("TenancyTribunalReason", this.tenancyTribunalReason);
            //Criminal
            application.put("Convicted", this.convicted);
            application.put("ConvictionDetails", this.convictedReason);
            application.put("BailParole", this.bailParole);
            application.put("BoarderOnBailParole", this.bailParoleBorder);
            application.put("MonitoringDevice", this.monitoringDevice);
            application.put("BoarderMonitoringDevice", this.monitoringDeviceBorder);
            application.put("ResidenceRestrictions", this.residenceRestrictions);
            application.put("BoarderResidenceRestrictions", this.residenceRestrictionsBorder);
            application.put("Bankrupt", this.bankrupt);
            //Employment
            application.put("Employed", this.employed);
            application.put("SelfEmployed", this.selfEmployed);
            application.put("Occupation", this.occupation);
            application.put("OccupationHours", this.occupationHours);
            application.put("OccupationTime", this.occupationTime);
            application.put("EmployerName", this.employerName);
            application.put("EmployerAddress", this.employerAddress);
            application.put("EmployerContactName", this.employerContactName);
            application.put("EmployerContactPhone", this.employerContactPhone);
            //Studying
            application.put("Student", this.student);
            application.put("Institution", this.studentInstitution);
            application.put("Course", this.studentCourse);
            application.put("CourseEnd", this.studentCourseEnd);
            application.put("CourseContactName", this.studentCourseAdminName);
            application.put("CourseContactPhone", this.studentCourseAdminContact);
            application.put("StudentSourceIncome", this.studentIncome);
            application.put("StudentIncomeAmount", this.studentIncomeAmount);
            //WINZ
            application.put("Winz", this.winz);
            application.put("WinzAmount", this.winzAmount);
            application.put("WinzCategory", this.winzCategory);
            application.put("WinzEnd", DateHelper.getStringFromDate(this.winzEnd));
            application.put("WinzRedirect", this.winzRedirect);
            application.put("WinzDay", this.winzDay);
            application.put("WinzAccSupp", this.winzAccSupp);
            //Advance and Bond
            application.put("AdvanceSavingsLoan", this.advanceSavingsLoan);
            application.put("AdvanceWINZ", this.advanceWINZ);
            application.put("AdvanceBondTransfer", this.advanceBondTransfer);
            //House
            application.put("HouseOwner", this.houseOwner);
            application.put("HouseAddress", this.houseAddress);
            //Vehicle
            application.put("VehicleOwner", this.vehicleOwner);
            application.put("VehicleMake", this.vehicleMake);
            application.put("VehicleModel", this.vehicleModel);
            application.put("VehicleColour", this.vehicleColour);
            application.put("VehicleRego", this.vehicleRego);
            //Reference
            application.put("Reference", reference);
            application.put("Relative", relative);
            application.put("State", this.state);
            return application;
        }
        catch (JSONException e){
            return null;
        }
    }

    private void unwrapPartial(JSONObject application) throws JSONException{
        this.id = application.getString("Id");
        this.address = application.getString("Address");
        this.state = application.getInt("State");
        currentAddress = new AddressDetails();
        previousAddress = new AddressDetails();
        this.otherOccupants = new ArrayList<>();
        this.pets = new ArrayList<>();
        reference = new ReferenceDetails();
        relative = new ReferenceDetails();

    }

    private void unwrap(JSONObject application) throws JSONException{
        this.id = application.getString("Id");
//        this.address = application.getString("Address");
        this.state = application.getInt("State");

        JSONObject jsonCurrentAddress = application.getJSONObject("CurrentAddress");
        this.currentAddress = new AddressDetails(   jsonCurrentAddress.getString("Address"),
                jsonCurrentAddress.getInt("LivingArrangments"),
                jsonCurrentAddress.getString("LengthOfStay"),
                jsonCurrentAddress.getString("LeavingReason"),
                jsonCurrentAddress.getString("ContactName"),
                jsonCurrentAddress.getString("ContactNumber")
        );

        JSONObject jsonPreviousAddress = application.getJSONObject("PreviousAddress");
        this.previousAddress = new AddressDetails(   jsonPreviousAddress.getString("Address"),
                -1,
                jsonPreviousAddress.getString("LengthOfStay"),
                jsonPreviousAddress.getString("LeavingReason"),
                jsonPreviousAddress.getString("ContactName"),
                jsonPreviousAddress.getString("ContactNumber")
        );

        this.otherOccupants = new ArrayList<>();
        JSONArray jOtherOccupants = application.getJSONArray("OtherOccupants");
        for(int i = 0; i <jOtherOccupants.length(); i++){
            JSONObject obj = jOtherOccupants.getJSONObject(i);
            Occupant occupant = new Occupant(obj.getString("Id"),
                    obj.getString("Name"),
                    obj.getInt("Age"),
                    obj.getString("Relationship"));
            this.otherOccupants.add(occupant);
        }

        this.intentRentalPeriod = application.getString("RentalPeriod");
        this.intentBorders = application.getBoolean("Borders");
        this.intentBordersAmount = application.getInt("BordersAmount");
        this.intentCommercial = application.getBoolean("Commercial");
        this.intentCommercialDescription = application.getString("CommercialDescription");

        this.pets = new ArrayList<>();
        JSONArray jPets = application.getJSONArray("Pets");
        for(int i = 0; i <jPets.length(); i++){
            JSONObject obj = jPets.getJSONObject(i);
            Pet pet = new Pet(obj.getString("Id"),
                    obj.getString("Type"),
                    obj.getString("Breed"),
                    obj.getInt("Age"),
                    obj.getString("Description"));
            this.pets.add(pet);
        }

        this.tenancyTribunal = application.getBoolean("TenancyTribunal");
        this.tenancyTribunalReason = application.getString("TenancyTribunalReason");

        this.convicted = application.getBoolean("Convicted");
        this.convictedReason = application.getString("ConvictionDetails");
        this.bailParole = application.getBoolean("BailParole");
        this.bailParoleBorder = application.getBoolean("BoarderOnBailParole");
        this.monitoringDevice = application.getBoolean("MonitoringDevice");
        this.monitoringDeviceBorder = application.getBoolean("BoarderMonitoringDevice");
        this.residenceRestrictions = application.getBoolean("ResidenceRestrictions");
        this.residenceRestrictionsBorder = application.getBoolean("BoarderResidenceRestrictions");
        this.bankrupt = application.getBoolean("Bankrupt");

        this.employed = application.getBoolean("Employed");
        this.selfEmployed = application.getBoolean("SelfEmployed");
        this.occupation = application.getString("Occupation");
        this.occupationHours = application.getInt("OccupationHours");
        this.occupationTime = application.getString("OccupationTime");
        this.employerName = application.getString("EmployerName");
        this.employerAddress = application.getString("EmployerAddress");
        this.employerContactName = application.getString("EmployerContactName");
        this.employerContactPhone = application.getString("EmployerContactPhone");

        this.student = application.getBoolean("Student");
        this.studentInstitution = application.getString("Institution");
        this.studentCourse = application.getString("Course");
        this.studentCourseEnd = DateHelper.getDateFromString(application.getString("CourseEnd"));
        this.studentCourseAdminName = application.getString("CourseContactName");
        this.studentCourseAdminContact = application.getString("CourseContactPhone");
        this.studentIncome = application.getString("StudentSourceIncome");
        this.studentIncomeAmount = application.getDouble("StudentIncomeAmount");

        this.winz = application.getBoolean("Winz");
        this.winzAmount = application.getDouble("WinzAmount");
        this.winzCategory = application.getString("WinzCategory");
        this.winzEnd = DateHelper.getDateFromString(application.getString("WinzEnd"));
        this.winzRedirect = application.getBoolean("WinzRedirect");
        this.winzDay = application.getInt("WinzDay");
        this.winzAccSupp = application.getDouble("WinzAccSupp");

        this.advanceSavingsLoan = application.getDouble("AdvanceSavingsLoan");
        this.advanceWINZ = application.getDouble("AdvanceWINZ");
        this.advanceBondTransfer = application.getDouble("AdvanceBondTransfer");

        this.houseOwner = application.getBoolean("HouseOwner");
        this.houseAddress = application.getString("HouseAddress");

        this.vehicleOwner = application.getBoolean("VehicleOwner");
        this.vehicleMake = application.getString("VehicleMake");
        this.vehicleModel = application.getString("VehicleModel");
        this.vehicleColour = application.getString("VehicleColour");
        this.vehicleRego = application.getString("VehicleRego");

        JSONObject jReference = application.getJSONObject("Reference");
        this.reference = new ReferenceDetails(jReference.getString("RefernceName"),
                jReference.getString("RefernceAddress"),
                jReference.getString("ReferncePhoneA"),
                jReference.getString("ReferncePhoneB"));

        JSONObject jRelative = application.getJSONObject("Relative");
        this.relative = new ReferenceDetails(jRelative.getString("RefernceName"),
                jRelative.getString("RefernceAddress"),
                jRelative.getString("ReferncePhoneA"),
                jRelative.getString("ReferncePhoneB"));
    }
}
