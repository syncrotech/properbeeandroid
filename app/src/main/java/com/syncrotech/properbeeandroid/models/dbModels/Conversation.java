package com.syncrotech.properbeeandroid.models.dbModels;

import org.json.JSONException;
import org.json.JSONObject;

public class Conversation {

    public String id;
    public String name;
    public int unreadMessages;

    public Conversation(JSONObject obj) throws JSONException {
        this.id = obj.getString("Id");
        this.unreadMessages = obj.getInt("UnreadMessages");
        this.name = obj.getString("Name");
    }
}
