package com.syncrotech.properbeeandroid.models;

/**
 * Created by ScryJ_000 on 13/10/2016.
 */
public class ReferenceDetails {
    public String name;
    public String address;
    public String phoneA;
    public String phoneB;

    public ReferenceDetails() {
    }

    public ReferenceDetails(String name, String address, String phoneA, String phoneB) {
        this.name = name;
        this.address = address;
        this.phoneA = phoneA;
        this.phoneB = phoneB;
    }
}
